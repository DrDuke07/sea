﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColorSliderImage
struct ColorSliderImage_t376502149;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void ColorSliderImage::.ctor()
extern "C"  void ColorSliderImage__ctor_m1463450338 (ColorSliderImage_t376502149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform ColorSliderImage::get_rectTransform()
extern "C"  RectTransform_t3349966182 * ColorSliderImage_get_rectTransform_m3308471489 (ColorSliderImage_t376502149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorSliderImage::Awake()
extern "C"  void ColorSliderImage_Awake_m834120629 (ColorSliderImage_t376502149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorSliderImage::OnEnable()
extern "C"  void ColorSliderImage_OnEnable_m1531418098 (ColorSliderImage_t376502149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorSliderImage::OnDisable()
extern "C"  void ColorSliderImage_OnDisable_m2944825581 (ColorSliderImage_t376502149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorSliderImage::OnDestroy()
extern "C"  void ColorSliderImage_OnDestroy_m2738933995 (ColorSliderImage_t376502149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorSliderImage::ColorChanged(UnityEngine.Color)
extern "C"  void ColorSliderImage_ColorChanged_m3679980033 (ColorSliderImage_t376502149 * __this, Color_t2020392075  ___newColor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorSliderImage::HSVChanged(System.Single,System.Single,System.Single)
extern "C"  void ColorSliderImage_HSVChanged_m1936863288 (ColorSliderImage_t376502149 * __this, float ___hue0, float ___saturation1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorSliderImage::RegenerateTexture()
extern "C"  void ColorSliderImage_RegenerateTexture_m1461909699 (ColorSliderImage_t376502149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
