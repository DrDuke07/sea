﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HexColorField
struct HexColorField_t4192118964;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"

// System.Void HexColorField::.ctor()
extern "C"  void HexColorField__ctor_m2851903553 (HexColorField_t4192118964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HexColorField::Awake()
extern "C"  void HexColorField_Awake_m306535424 (HexColorField_t4192118964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HexColorField::OnDestroy()
extern "C"  void HexColorField_OnDestroy_m4113204650 (HexColorField_t4192118964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HexColorField::UpdateHex(UnityEngine.Color)
extern "C"  void HexColorField_UpdateHex_m1796725051 (HexColorField_t4192118964 * __this, Color_t2020392075  ___newColor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HexColorField::UpdateColor(System.String)
extern "C"  void HexColorField_UpdateColor_m4189307031 (HexColorField_t4192118964 * __this, String_t* ___newHex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HexColorField::ColorToHex(UnityEngine.Color32)
extern "C"  String_t* HexColorField_ColorToHex_m410116926 (HexColorField_t4192118964 * __this, Color32_t874517518  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HexColorField::HexToColor(System.String,UnityEngine.Color32&)
extern "C"  bool HexColorField_HexToColor_m3970770477 (Il2CppObject * __this /* static, unused */, String_t* ___hex0, Color32_t874517518 * ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
