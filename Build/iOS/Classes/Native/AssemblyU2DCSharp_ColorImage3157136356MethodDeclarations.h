﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColorImage
struct ColorImage_t3157136356;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void ColorImage::.ctor()
extern "C"  void ColorImage__ctor_m797732221 (ColorImage_t3157136356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorImage::Awake()
extern "C"  void ColorImage_Awake_m1432153760 (ColorImage_t3157136356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorImage::OnDestroy()
extern "C"  void ColorImage_OnDestroy_m537617202 (ColorImage_t3157136356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorImage::ColorChanged(UnityEngine.Color)
extern "C"  void ColorImage_ColorChanged_m2780022934 (ColorImage_t3157136356 * __this, Color_t2020392075  ___newColor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
