﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HsvColor1057062332.h"

// System.Void HsvColor::.ctor(System.Double,System.Double,System.Double)
extern "C"  void HsvColor__ctor_m2096855601 (HsvColor_t1057062332 * __this, double ___h0, double ___s1, double ___v2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HsvColor::get_normalizedH()
extern "C"  float HsvColor_get_normalizedH_m3607787917 (HsvColor_t1057062332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HsvColor::set_normalizedH(System.Single)
extern "C"  void HsvColor_set_normalizedH_m2139074108 (HsvColor_t1057062332 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HsvColor::get_normalizedS()
extern "C"  float HsvColor_get_normalizedS_m3607786980 (HsvColor_t1057062332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HsvColor::set_normalizedS(System.Single)
extern "C"  void HsvColor_set_normalizedS_m413849441 (HsvColor_t1057062332 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HsvColor::get_normalizedV()
extern "C"  float HsvColor_get_normalizedV_m3607786943 (HsvColor_t1057062332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HsvColor::set_normalizedV(System.Single)
extern "C"  void HsvColor_set_normalizedV_m460463238 (HsvColor_t1057062332 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HsvColor::ToString()
extern "C"  String_t* HsvColor_ToString_m1590809902 (HsvColor_t1057062332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
