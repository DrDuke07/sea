﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// _PickerSeaWaterColorControl
struct _PickerSeaWaterColorControl_t1489839923;

#include "codegen/il2cpp-codegen.h"

// System.Void _PickerSeaWaterColorControl::.ctor()
extern "C"  void _PickerSeaWaterColorControl__ctor_m2200172044 (_PickerSeaWaterColorControl_t1489839923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _PickerSeaWaterColorControl::Start()
extern "C"  void _PickerSeaWaterColorControl_Start_m3388375480 (_PickerSeaWaterColorControl_t1489839923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _PickerSeaWaterColorControl::Update()
extern "C"  void _PickerSeaWaterColorControl_Update_m1653883871 (_PickerSeaWaterColorControl_t1489839923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
