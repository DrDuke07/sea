﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColorPickerTester
struct ColorPickerTester_t1006114474;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void ColorPickerTester::.ctor()
extern "C"  void ColorPickerTester__ctor_m185493325 (ColorPickerTester_t1006114474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPickerTester::Start()
extern "C"  void ColorPickerTester_Start_m2707422277 (ColorPickerTester_t1006114474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPickerTester::Update()
extern "C"  void ColorPickerTester_Update_m85289384 (ColorPickerTester_t1006114474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPickerTester::<Start>m__0(UnityEngine.Color)
extern "C"  void ColorPickerTester_U3CStartU3Em__0_m540941732 (ColorPickerTester_t1006114474 * __this, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
