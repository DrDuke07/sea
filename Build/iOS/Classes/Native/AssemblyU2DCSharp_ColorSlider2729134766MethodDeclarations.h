﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColorSlider
struct ColorSlider_t2729134766;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void ColorSlider::.ctor()
extern "C"  void ColorSlider__ctor_m3582912827 (ColorSlider_t2729134766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorSlider::Awake()
extern "C"  void ColorSlider_Awake_m2035409562 (ColorSlider_t2729134766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorSlider::OnDestroy()
extern "C"  void ColorSlider_OnDestroy_m1119776176 (ColorSlider_t2729134766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorSlider::ColorChanged(UnityEngine.Color)
extern "C"  void ColorSlider_ColorChanged_m742523788 (ColorSlider_t2729134766 * __this, Color_t2020392075  ___newColor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorSlider::HSVChanged(System.Single,System.Single,System.Single)
extern "C"  void ColorSlider_HSVChanged_m3304896453 (ColorSlider_t2729134766 * __this, float ___hue0, float ___saturation1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorSlider::SliderChanged(System.Single)
extern "C"  void ColorSlider_SliderChanged_m1758874829 (ColorSlider_t2729134766 * __this, float ___newValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
