﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// _PickerSkyColor
struct _PickerSkyColor_t979671171;

#include "codegen/il2cpp-codegen.h"

// System.Void _PickerSkyColor::.ctor()
extern "C"  void _PickerSkyColor__ctor_m1569764186 (_PickerSkyColor_t979671171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _PickerSkyColor::Start()
extern "C"  void _PickerSkyColor_Start_m4067587422 (_PickerSkyColor_t979671171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _PickerSkyColor::Update()
extern "C"  void _PickerSkyColor_Update_m683868167 (_PickerSkyColor_t979671171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
