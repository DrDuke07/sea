﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColorPicker
struct ColorPicker_t3035206225;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_ColorValues3063098635.h"

// System.Void ColorPicker::.ctor()
extern "C"  void ColorPicker__ctor_m1239957560 (ColorPicker_t3035206225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color ColorPicker::get_CurrentColor()
extern "C"  Color_t2020392075  ColorPicker_get_CurrentColor_m3166096170 (ColorPicker_t3035206225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::set_CurrentColor(UnityEngine.Color)
extern "C"  void ColorPicker_set_CurrentColor_m2125228471 (ColorPicker_t3035206225 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::Start()
extern "C"  void ColorPicker_Start_m1519077976 (ColorPicker_t3035206225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ColorPicker::get_H()
extern "C"  float ColorPicker_get_H_m3860865411 (ColorPicker_t3035206225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::set_H(System.Single)
extern "C"  void ColorPicker_set_H_m2725867100 (ColorPicker_t3035206225 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ColorPicker::get_S()
extern "C"  float ColorPicker_get_S_m2449498732 (ColorPicker_t3035206225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::set_S(System.Single)
extern "C"  void ColorPicker_set_S_m985865155 (ColorPicker_t3035206225 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ColorPicker::get_V()
extern "C"  float ColorPicker_get_V_m265062405 (ColorPicker_t3035206225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::set_V(System.Single)
extern "C"  void ColorPicker_set_V_m1877583698 (ColorPicker_t3035206225 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ColorPicker::get_R()
extern "C"  float ColorPicker_get_R_m3995379697 (ColorPicker_t3035206225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::set_R(System.Single)
extern "C"  void ColorPicker_set_R_m350597694 (ColorPicker_t3035206225 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ColorPicker::get_G()
extern "C"  float ColorPicker_get_G_m755548720 (ColorPicker_t3035206225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::set_G(System.Single)
extern "C"  void ColorPicker_set_G_m2188343591 (ColorPicker_t3035206225 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ColorPicker::get_B()
extern "C"  float ColorPicker_get_B_m1736779681 (ColorPicker_t3035206225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::set_B(System.Single)
extern "C"  void ColorPicker_set_B_m26090126 (ColorPicker_t3035206225 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ColorPicker::get_A()
extern "C"  float ColorPicker_get_A_m473223718 (ColorPicker_t3035206225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::set_A(System.Single)
extern "C"  void ColorPicker_set_A_m1839123465 (ColorPicker_t3035206225 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::RGBChanged()
extern "C"  void ColorPicker_RGBChanged_m2442023581 (ColorPicker_t3035206225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::HSVChanged()
extern "C"  void ColorPicker_HSVChanged_m3054464207 (ColorPicker_t3035206225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::SendChangedEvent()
extern "C"  void ColorPicker_SendChangedEvent_m2880248376 (ColorPicker_t3035206225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::AssignColor(ColorValues,System.Single)
extern "C"  void ColorPicker_AssignColor_m579716850 (ColorPicker_t3035206225 * __this, int32_t ___type0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ColorPicker::GetValue(ColorValues)
extern "C"  float ColorPicker_GetValue_m1991475278 (ColorPicker_t3035206225 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
