﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HsvColor1057062332.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// HsvColor HSVUtil::ConvertRgbToHsv(UnityEngine.Color)
extern "C"  HsvColor_t1057062332  HSVUtil_ConvertRgbToHsv_m4088715697 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HsvColor HSVUtil::ConvertRgbToHsv(System.Double,System.Double,System.Double)
extern "C"  HsvColor_t1057062332  HSVUtil_ConvertRgbToHsv_m2238362913 (Il2CppObject * __this /* static, unused */, double ___r0, double ___b1, double ___g2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HSVUtil::ConvertHsvToRgb(System.Double,System.Double,System.Double,System.Single)
extern "C"  Color_t2020392075  HSVUtil_ConvertHsvToRgb_m2339284600 (Il2CppObject * __this /* static, unused */, double ___h0, double ___s1, double ___v2, float ___alpha3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
