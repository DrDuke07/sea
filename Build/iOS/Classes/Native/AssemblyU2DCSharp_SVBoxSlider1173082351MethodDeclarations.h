﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SVBoxSlider
struct SVBoxSlider_t1173082351;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;

#include "codegen/il2cpp-codegen.h"

// System.Void SVBoxSlider::.ctor()
extern "C"  void SVBoxSlider__ctor_m2623596066 (SVBoxSlider_t1173082351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform SVBoxSlider::get_rectTransform()
extern "C"  RectTransform_t3349966182 * SVBoxSlider_get_rectTransform_m3427717911 (SVBoxSlider_t1173082351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SVBoxSlider::Awake()
extern "C"  void SVBoxSlider_Awake_m1174811035 (SVBoxSlider_t1173082351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SVBoxSlider::OnEnable()
extern "C"  void SVBoxSlider_OnEnable_m542656758 (SVBoxSlider_t1173082351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SVBoxSlider::OnDisable()
extern "C"  void SVBoxSlider_OnDisable_m2237433047 (SVBoxSlider_t1173082351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SVBoxSlider::OnDestroy()
extern "C"  void SVBoxSlider_OnDestroy_m1087675145 (SVBoxSlider_t1173082351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SVBoxSlider::SliderChanged(System.Single,System.Single)
extern "C"  void SVBoxSlider_SliderChanged_m3739635521 (SVBoxSlider_t1173082351 * __this, float ___saturation0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SVBoxSlider::HSVChanged(System.Single,System.Single,System.Single)
extern "C"  void SVBoxSlider_HSVChanged_m3533720340 (SVBoxSlider_t1173082351 * __this, float ___h0, float ___s1, float ___v2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SVBoxSlider::RegenerateSVTexture()
extern "C"  void SVBoxSlider_RegenerateSVTexture_m4094171080 (SVBoxSlider_t1173082351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
