﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ColorPicker
struct ColorPicker_t3035206225;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// _PickerSkyColor
struct  _PickerSkyColor_t979671171  : public MonoBehaviour_t1158329972
{
public:
	// ColorPicker _PickerSkyColor::picker_sky_color
	ColorPicker_t3035206225 * ___picker_sky_color_2;
	// UnityEngine.Color _PickerSkyColor::sky_color
	Color_t2020392075  ___sky_color_3;

public:
	inline static int32_t get_offset_of_picker_sky_color_2() { return static_cast<int32_t>(offsetof(_PickerSkyColor_t979671171, ___picker_sky_color_2)); }
	inline ColorPicker_t3035206225 * get_picker_sky_color_2() const { return ___picker_sky_color_2; }
	inline ColorPicker_t3035206225 ** get_address_of_picker_sky_color_2() { return &___picker_sky_color_2; }
	inline void set_picker_sky_color_2(ColorPicker_t3035206225 * value)
	{
		___picker_sky_color_2 = value;
		Il2CppCodeGenWriteBarrier(&___picker_sky_color_2, value);
	}

	inline static int32_t get_offset_of_sky_color_3() { return static_cast<int32_t>(offsetof(_PickerSkyColor_t979671171, ___sky_color_3)); }
	inline Color_t2020392075  get_sky_color_3() const { return ___sky_color_3; }
	inline Color_t2020392075 * get_address_of_sky_color_3() { return &___sky_color_3; }
	inline void set_sky_color_3(Color_t2020392075  value)
	{
		___sky_color_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
