﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// _SeaControl
struct  _SeaControl_t4060510597  : public MonoBehaviour_t1158329972
{
public:
	// System.Single _SeaControl::sea_frequency
	float ___sea_frequency_2;
	// System.Single _SeaControl::sea_height
	float ___sea_height_3;
	// System.Single _SeaControl::sea_choppy
	float ___sea_choppy_4;
	// System.Single _SeaControl::sea_speed
	float ___sea_speed_5;
	// System.Single _SeaControl::sun_pos_u
	float ___sun_pos_u_6;
	// System.Single _SeaControl::sun_pos_v
	float ___sun_pos_v_7;
	// System.Single _SeaControl::camera_height
	float ___camera_height_8;
	// System.Single _SeaControl::camera_rot_angle
	float ___camera_rot_angle_9;
	// System.Single _SeaControl::sea_movement_speed
	float ___sea_movement_speed_10;
	// UnityEngine.Color _SeaControl::sea_water_color
	Color_t2020392075  ___sea_water_color_11;
	// UnityEngine.Color _SeaControl::sea_base_color
	Color_t2020392075  ___sea_base_color_12;
	// UnityEngine.Color _SeaControl::sun_color
	Color_t2020392075  ___sun_color_13;
	// UnityEngine.GameObject _SeaControl::Gui_layer
	GameObject_t1756533147 * ___Gui_layer_14;
	// System.Boolean _SeaControl::show_gui
	bool ___show_gui_15;
	// UnityEngine.Camera _SeaControl::main_camera
	Camera_t189460977 * ___main_camera_16;
	// UnityEngine.UI.Text _SeaControl::sun_pos_u_text
	Text_t356221433 * ___sun_pos_u_text_17;
	// UnityEngine.UI.Text _SeaControl::sun_pos_v_text
	Text_t356221433 * ___sun_pos_v_text_18;
	// UnityEngine.UI.Text _SeaControl::sea_speed_text
	Text_t356221433 * ___sea_speed_text_19;
	// UnityEngine.UI.Text _SeaControl::sea_choppy_text
	Text_t356221433 * ___sea_choppy_text_20;
	// UnityEngine.UI.Text _SeaControl::sea_height_text
	Text_t356221433 * ___sea_height_text_21;
	// UnityEngine.UI.Text _SeaControl::sea_frequency_text
	Text_t356221433 * ___sea_frequency_text_22;
	// UnityEngine.UI.Text _SeaControl::camera_height_text
	Text_t356221433 * ___camera_height_text_23;
	// UnityEngine.UI.Text _SeaControl::camera_rot_angle_text
	Text_t356221433 * ___camera_rot_angle_text_24;
	// UnityEngine.UI.Text _SeaControl::sea_movement_speed_text
	Text_t356221433 * ___sea_movement_speed_text_25;
	// UnityEngine.Color _SeaControl::sky_color
	Color_t2020392075  ___sky_color_26;

public:
	inline static int32_t get_offset_of_sea_frequency_2() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___sea_frequency_2)); }
	inline float get_sea_frequency_2() const { return ___sea_frequency_2; }
	inline float* get_address_of_sea_frequency_2() { return &___sea_frequency_2; }
	inline void set_sea_frequency_2(float value)
	{
		___sea_frequency_2 = value;
	}

	inline static int32_t get_offset_of_sea_height_3() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___sea_height_3)); }
	inline float get_sea_height_3() const { return ___sea_height_3; }
	inline float* get_address_of_sea_height_3() { return &___sea_height_3; }
	inline void set_sea_height_3(float value)
	{
		___sea_height_3 = value;
	}

	inline static int32_t get_offset_of_sea_choppy_4() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___sea_choppy_4)); }
	inline float get_sea_choppy_4() const { return ___sea_choppy_4; }
	inline float* get_address_of_sea_choppy_4() { return &___sea_choppy_4; }
	inline void set_sea_choppy_4(float value)
	{
		___sea_choppy_4 = value;
	}

	inline static int32_t get_offset_of_sea_speed_5() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___sea_speed_5)); }
	inline float get_sea_speed_5() const { return ___sea_speed_5; }
	inline float* get_address_of_sea_speed_5() { return &___sea_speed_5; }
	inline void set_sea_speed_5(float value)
	{
		___sea_speed_5 = value;
	}

	inline static int32_t get_offset_of_sun_pos_u_6() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___sun_pos_u_6)); }
	inline float get_sun_pos_u_6() const { return ___sun_pos_u_6; }
	inline float* get_address_of_sun_pos_u_6() { return &___sun_pos_u_6; }
	inline void set_sun_pos_u_6(float value)
	{
		___sun_pos_u_6 = value;
	}

	inline static int32_t get_offset_of_sun_pos_v_7() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___sun_pos_v_7)); }
	inline float get_sun_pos_v_7() const { return ___sun_pos_v_7; }
	inline float* get_address_of_sun_pos_v_7() { return &___sun_pos_v_7; }
	inline void set_sun_pos_v_7(float value)
	{
		___sun_pos_v_7 = value;
	}

	inline static int32_t get_offset_of_camera_height_8() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___camera_height_8)); }
	inline float get_camera_height_8() const { return ___camera_height_8; }
	inline float* get_address_of_camera_height_8() { return &___camera_height_8; }
	inline void set_camera_height_8(float value)
	{
		___camera_height_8 = value;
	}

	inline static int32_t get_offset_of_camera_rot_angle_9() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___camera_rot_angle_9)); }
	inline float get_camera_rot_angle_9() const { return ___camera_rot_angle_9; }
	inline float* get_address_of_camera_rot_angle_9() { return &___camera_rot_angle_9; }
	inline void set_camera_rot_angle_9(float value)
	{
		___camera_rot_angle_9 = value;
	}

	inline static int32_t get_offset_of_sea_movement_speed_10() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___sea_movement_speed_10)); }
	inline float get_sea_movement_speed_10() const { return ___sea_movement_speed_10; }
	inline float* get_address_of_sea_movement_speed_10() { return &___sea_movement_speed_10; }
	inline void set_sea_movement_speed_10(float value)
	{
		___sea_movement_speed_10 = value;
	}

	inline static int32_t get_offset_of_sea_water_color_11() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___sea_water_color_11)); }
	inline Color_t2020392075  get_sea_water_color_11() const { return ___sea_water_color_11; }
	inline Color_t2020392075 * get_address_of_sea_water_color_11() { return &___sea_water_color_11; }
	inline void set_sea_water_color_11(Color_t2020392075  value)
	{
		___sea_water_color_11 = value;
	}

	inline static int32_t get_offset_of_sea_base_color_12() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___sea_base_color_12)); }
	inline Color_t2020392075  get_sea_base_color_12() const { return ___sea_base_color_12; }
	inline Color_t2020392075 * get_address_of_sea_base_color_12() { return &___sea_base_color_12; }
	inline void set_sea_base_color_12(Color_t2020392075  value)
	{
		___sea_base_color_12 = value;
	}

	inline static int32_t get_offset_of_sun_color_13() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___sun_color_13)); }
	inline Color_t2020392075  get_sun_color_13() const { return ___sun_color_13; }
	inline Color_t2020392075 * get_address_of_sun_color_13() { return &___sun_color_13; }
	inline void set_sun_color_13(Color_t2020392075  value)
	{
		___sun_color_13 = value;
	}

	inline static int32_t get_offset_of_Gui_layer_14() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___Gui_layer_14)); }
	inline GameObject_t1756533147 * get_Gui_layer_14() const { return ___Gui_layer_14; }
	inline GameObject_t1756533147 ** get_address_of_Gui_layer_14() { return &___Gui_layer_14; }
	inline void set_Gui_layer_14(GameObject_t1756533147 * value)
	{
		___Gui_layer_14 = value;
		Il2CppCodeGenWriteBarrier(&___Gui_layer_14, value);
	}

	inline static int32_t get_offset_of_show_gui_15() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___show_gui_15)); }
	inline bool get_show_gui_15() const { return ___show_gui_15; }
	inline bool* get_address_of_show_gui_15() { return &___show_gui_15; }
	inline void set_show_gui_15(bool value)
	{
		___show_gui_15 = value;
	}

	inline static int32_t get_offset_of_main_camera_16() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___main_camera_16)); }
	inline Camera_t189460977 * get_main_camera_16() const { return ___main_camera_16; }
	inline Camera_t189460977 ** get_address_of_main_camera_16() { return &___main_camera_16; }
	inline void set_main_camera_16(Camera_t189460977 * value)
	{
		___main_camera_16 = value;
		Il2CppCodeGenWriteBarrier(&___main_camera_16, value);
	}

	inline static int32_t get_offset_of_sun_pos_u_text_17() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___sun_pos_u_text_17)); }
	inline Text_t356221433 * get_sun_pos_u_text_17() const { return ___sun_pos_u_text_17; }
	inline Text_t356221433 ** get_address_of_sun_pos_u_text_17() { return &___sun_pos_u_text_17; }
	inline void set_sun_pos_u_text_17(Text_t356221433 * value)
	{
		___sun_pos_u_text_17 = value;
		Il2CppCodeGenWriteBarrier(&___sun_pos_u_text_17, value);
	}

	inline static int32_t get_offset_of_sun_pos_v_text_18() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___sun_pos_v_text_18)); }
	inline Text_t356221433 * get_sun_pos_v_text_18() const { return ___sun_pos_v_text_18; }
	inline Text_t356221433 ** get_address_of_sun_pos_v_text_18() { return &___sun_pos_v_text_18; }
	inline void set_sun_pos_v_text_18(Text_t356221433 * value)
	{
		___sun_pos_v_text_18 = value;
		Il2CppCodeGenWriteBarrier(&___sun_pos_v_text_18, value);
	}

	inline static int32_t get_offset_of_sea_speed_text_19() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___sea_speed_text_19)); }
	inline Text_t356221433 * get_sea_speed_text_19() const { return ___sea_speed_text_19; }
	inline Text_t356221433 ** get_address_of_sea_speed_text_19() { return &___sea_speed_text_19; }
	inline void set_sea_speed_text_19(Text_t356221433 * value)
	{
		___sea_speed_text_19 = value;
		Il2CppCodeGenWriteBarrier(&___sea_speed_text_19, value);
	}

	inline static int32_t get_offset_of_sea_choppy_text_20() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___sea_choppy_text_20)); }
	inline Text_t356221433 * get_sea_choppy_text_20() const { return ___sea_choppy_text_20; }
	inline Text_t356221433 ** get_address_of_sea_choppy_text_20() { return &___sea_choppy_text_20; }
	inline void set_sea_choppy_text_20(Text_t356221433 * value)
	{
		___sea_choppy_text_20 = value;
		Il2CppCodeGenWriteBarrier(&___sea_choppy_text_20, value);
	}

	inline static int32_t get_offset_of_sea_height_text_21() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___sea_height_text_21)); }
	inline Text_t356221433 * get_sea_height_text_21() const { return ___sea_height_text_21; }
	inline Text_t356221433 ** get_address_of_sea_height_text_21() { return &___sea_height_text_21; }
	inline void set_sea_height_text_21(Text_t356221433 * value)
	{
		___sea_height_text_21 = value;
		Il2CppCodeGenWriteBarrier(&___sea_height_text_21, value);
	}

	inline static int32_t get_offset_of_sea_frequency_text_22() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___sea_frequency_text_22)); }
	inline Text_t356221433 * get_sea_frequency_text_22() const { return ___sea_frequency_text_22; }
	inline Text_t356221433 ** get_address_of_sea_frequency_text_22() { return &___sea_frequency_text_22; }
	inline void set_sea_frequency_text_22(Text_t356221433 * value)
	{
		___sea_frequency_text_22 = value;
		Il2CppCodeGenWriteBarrier(&___sea_frequency_text_22, value);
	}

	inline static int32_t get_offset_of_camera_height_text_23() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___camera_height_text_23)); }
	inline Text_t356221433 * get_camera_height_text_23() const { return ___camera_height_text_23; }
	inline Text_t356221433 ** get_address_of_camera_height_text_23() { return &___camera_height_text_23; }
	inline void set_camera_height_text_23(Text_t356221433 * value)
	{
		___camera_height_text_23 = value;
		Il2CppCodeGenWriteBarrier(&___camera_height_text_23, value);
	}

	inline static int32_t get_offset_of_camera_rot_angle_text_24() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___camera_rot_angle_text_24)); }
	inline Text_t356221433 * get_camera_rot_angle_text_24() const { return ___camera_rot_angle_text_24; }
	inline Text_t356221433 ** get_address_of_camera_rot_angle_text_24() { return &___camera_rot_angle_text_24; }
	inline void set_camera_rot_angle_text_24(Text_t356221433 * value)
	{
		___camera_rot_angle_text_24 = value;
		Il2CppCodeGenWriteBarrier(&___camera_rot_angle_text_24, value);
	}

	inline static int32_t get_offset_of_sea_movement_speed_text_25() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___sea_movement_speed_text_25)); }
	inline Text_t356221433 * get_sea_movement_speed_text_25() const { return ___sea_movement_speed_text_25; }
	inline Text_t356221433 ** get_address_of_sea_movement_speed_text_25() { return &___sea_movement_speed_text_25; }
	inline void set_sea_movement_speed_text_25(Text_t356221433 * value)
	{
		___sea_movement_speed_text_25 = value;
		Il2CppCodeGenWriteBarrier(&___sea_movement_speed_text_25, value);
	}

	inline static int32_t get_offset_of_sky_color_26() { return static_cast<int32_t>(offsetof(_SeaControl_t4060510597, ___sky_color_26)); }
	inline Color_t2020392075  get_sky_color_26() const { return ___sky_color_26; }
	inline Color_t2020392075 * get_address_of_sky_color_26() { return &___sky_color_26; }
	inline void set_sky_color_26(Color_t2020392075  value)
	{
		___sky_color_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
