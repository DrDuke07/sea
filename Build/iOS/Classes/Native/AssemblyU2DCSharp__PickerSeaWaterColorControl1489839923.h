﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ColorPicker
struct ColorPicker_t3035206225;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// _PickerSeaWaterColorControl
struct  _PickerSeaWaterColorControl_t1489839923  : public MonoBehaviour_t1158329972
{
public:
	// ColorPicker _PickerSeaWaterColorControl::sea_water_color_picker
	ColorPicker_t3035206225 * ___sea_water_color_picker_2;
	// UnityEngine.Color _PickerSeaWaterColorControl::sea_water_color
	Color_t2020392075  ___sea_water_color_3;

public:
	inline static int32_t get_offset_of_sea_water_color_picker_2() { return static_cast<int32_t>(offsetof(_PickerSeaWaterColorControl_t1489839923, ___sea_water_color_picker_2)); }
	inline ColorPicker_t3035206225 * get_sea_water_color_picker_2() const { return ___sea_water_color_picker_2; }
	inline ColorPicker_t3035206225 ** get_address_of_sea_water_color_picker_2() { return &___sea_water_color_picker_2; }
	inline void set_sea_water_color_picker_2(ColorPicker_t3035206225 * value)
	{
		___sea_water_color_picker_2 = value;
		Il2CppCodeGenWriteBarrier(&___sea_water_color_picker_2, value);
	}

	inline static int32_t get_offset_of_sea_water_color_3() { return static_cast<int32_t>(offsetof(_PickerSeaWaterColorControl_t1489839923, ___sea_water_color_3)); }
	inline Color_t2020392075  get_sea_water_color_3() const { return ___sea_water_color_3; }
	inline Color_t2020392075 * get_address_of_sea_water_color_3() { return &___sea_water_color_3; }
	inline void set_sea_water_color_3(Color_t2020392075  value)
	{
		___sea_water_color_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
