﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _PickerSunColorControl : MonoBehaviour {


    //public new Renderer renderer;
    public ColorPicker picker_sun_color;
    public Color sun_color_color = new Color(1.0f, 1.0f, 1.0f);

	// Use this for initialization
	void Start () {
        picker_sun_color.CurrentColor = sun_color_color;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
