﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// _PickerSunColorControl
struct _PickerSunColorControl_t1983561777;

#include "codegen/il2cpp-codegen.h"

// System.Void _PickerSunColorControl::.ctor()
extern "C"  void _PickerSunColorControl__ctor_m1452739074 (_PickerSunColorControl_t1983561777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _PickerSunColorControl::Start()
extern "C"  void _PickerSunColorControl_Start_m3187310546 (_PickerSunColorControl_t1983561777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _PickerSunColorControl::Update()
extern "C"  void _PickerSunColorControl_Update_m3201213873 (_PickerSunColorControl_t1983561777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
