﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColorLabel
struct ColorLabel_t1884607337;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void ColorLabel::.ctor()
extern "C"  void ColorLabel__ctor_m3361268158 (ColorLabel_t1884607337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorLabel::Awake()
extern "C"  void ColorLabel_Awake_m1477595641 (ColorLabel_t1884607337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorLabel::OnEnable()
extern "C"  void ColorLabel_OnEnable_m3391994902 (ColorLabel_t1884607337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorLabel::OnDestroy()
extern "C"  void ColorLabel_OnDestroy_m648294215 (ColorLabel_t1884607337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorLabel::ColorChanged(UnityEngine.Color)
extern "C"  void ColorLabel_ColorChanged_m35916549 (ColorLabel_t1884607337 * __this, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorLabel::HSVChanged(System.Single,System.Single,System.Single)
extern "C"  void ColorLabel_HSVChanged_m3658710332 (ColorLabel_t1884607337 * __this, float ___hue0, float ___sateration1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorLabel::UpdateValue()
extern "C"  void ColorLabel_UpdateValue_m3108932048 (ColorLabel_t1884607337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ColorLabel::ConvertToDisplayString(System.Single)
extern "C"  String_t* ColorLabel_ConvertToDisplayString_m1654160419 (ColorLabel_t1884607337 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
