﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// _PickerControl
struct _PickerControl_t2624631720;
// _PickerSeaWaterColorControl
struct _PickerSeaWaterColorControl_t1489839923;
// _PickerSkyColor
struct _PickerSkyColor_t979671171;
// _PickerSunColorControl
struct _PickerSunColorControl_t1983561777;
// _SeaControl
struct _SeaControl_t4060510597;
// ColorChangedEvent
struct ColorChangedEvent_t2990895397;
// ColorImage
struct ColorImage_t3157136356;
// UnityEngine.UI.Image
struct Image_t2042527209;
// System.Object
struct Il2CppObject;
// ColorLabel
struct ColorLabel_t1884607337;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;
// ColorPicker
struct ColorPicker_t3035206225;
// ColorPickerTester
struct ColorPickerTester_t1006114474;
// ColorPresets
struct ColorPresets_t4120623669;
// ColorSlider
struct ColorSlider_t2729134766;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// ColorSliderImage
struct ColorSliderImage_t376502149;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// HexColorField
struct HexColorField_t4192118964;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// HSVChangedEvent
struct HSVChangedEvent_t1170297569;
// SVBoxSlider
struct SVBoxSlider_t1173082351;
// UnityEngine.UI.BoxSlider
struct BoxSlider_t1871650694;
// TiltWindow
struct TiltWindow_t1839185375;
// UnityEngine.UI.BoxSlider/BoxSliderEvent
struct BoxSliderEvent_t1774115848;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp__PickerControl2624631720.h"
#include "AssemblyU2DCSharp__PickerControl2624631720MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "AssemblyU2DCSharp_ColorPicker3035206225MethodDeclarations.h"
#include "AssemblyU2DCSharp_ColorPicker3035206225.h"
#include "AssemblyU2DCSharp__PickerSeaWaterColorControl1489839923.h"
#include "AssemblyU2DCSharp__PickerSeaWaterColorControl1489839923MethodDeclarations.h"
#include "AssemblyU2DCSharp__PickerSkyColor979671171.h"
#include "AssemblyU2DCSharp__PickerSkyColor979671171MethodDeclarations.h"
#include "AssemblyU2DCSharp__PickerSunColorControl1983561777.h"
#include "AssemblyU2DCSharp__PickerSunColorControl1983561777MethodDeclarations.h"
#include "AssemblyU2DCSharp__SeaControl4060510597.h"
#include "AssemblyU2DCSharp__SeaControl4060510597MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderSettings1057812535MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material193706927MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_ColorChangedEvent2990895397.h"
#include "AssemblyU2DCSharp_ColorChangedEvent2990895397MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2058742090MethodDeclarations.h"
#include "AssemblyU2DCSharp_ColorImage3157136356.h"
#include "AssemblyU2DCSharp_ColorImage3157136356MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3386977826MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3386977826.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "AssemblyU2DCSharp_ColorLabel1884607337.h"
#include "AssemblyU2DCSharp_ColorLabel1884607337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_3_gen235051313MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen4197061729MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "AssemblyU2DCSharp_HSVChangedEvent1170297569.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_3_gen235051313.h"
#include "AssemblyU2DCSharp_ColorValues3063098635.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "AssemblyU2DCSharp_HSVChangedEvent1170297569MethodDeclarations.h"
#include "AssemblyU2DCSharp_HSVUtil3885028383MethodDeclarations.h"
#include "AssemblyU2DCSharp_HsvColor1057062332.h"
#include "AssemblyU2DCSharp_HsvColor1057062332MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_NotImplementedException2785117854MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "AssemblyU2DCSharp_ColorPickerTester1006114474.h"
#include "AssemblyU2DCSharp_ColorPickerTester1006114474MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer257310565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "AssemblyU2DCSharp_ColorPresets4120623669.h"
#include "AssemblyU2DCSharp_ColorPresets4120623669MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "AssemblyU2DCSharp_ColorSlider2729134766.h"
#include "AssemblyU2DCSharp_ColorSlider2729134766MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider297367283MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3443095683MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2114859947MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider297367283.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent2111116400.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3443095683.h"
#include "AssemblyU2DCSharp_ColorSliderImage376502149.h"
#include "AssemblyU2DCSharp_ColorSliderImage376502149MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage2749640213.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage2749640213MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Color32874517518MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "mscorlib_System_Byte3683104436.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction1525323322.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "AssemblyU2DCSharp_ColorValues3063098635MethodDeclarations.h"
#include "AssemblyU2DCSharp_HexColorField4192118964.h"
#include "AssemblyU2DCSharp_HexColorField4192118964MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField1631627530MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3395805984MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2067570248MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField1631627530.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_SubmitEven907918422.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3395805984.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_OnChangeE2863344003.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "System_System_Text_RegularExpressions_Regex1803876613MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436MethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberStyles3408984435.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Double4078015681MethodDeclarations.h"
#include "AssemblyU2DCSharp_HSVUtil3885028383.h"
#include "mscorlib_System_Math2022911894MethodDeclarations.h"
#include "AssemblyU2DCSharp_SVBoxSlider1173082351.h"
#include "AssemblyU2DCSharp_SVBoxSlider1173082351MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_BoxSlider1871650694.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_BoxSlider1871650694MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen134459182MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen2016657100MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_BoxSlider_BoxSlid1774115848.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen134459182.h"
#include "AssemblyU2DCSharp_TiltWindow1839185375.h"
#include "AssemblyU2DCSharp_TiltWindow1839185375MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_BoxSlider_BoxSlid1774115848MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate1528800019.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker154385424.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker154385424MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3960014691MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3960014691.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DrivenTransformProperties2488747555.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransformUtility2941082270MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve2981963041.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_BoxSlider_Axis3966514019.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_BoxSlider_Axis3966514019MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_BoxSlider_Directi1632189177.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_BoxSlider_Directi1632189177MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t2042527209_m2189462422(__this, method) ((  Image_t2042527209 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t356221433_m1342661039(__this, method) ((  Text_t356221433 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t2042527209_m4162535761(__this, method) ((  Image_t2042527209 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Slider>()
#define Component_GetComponent_TisSlider_t297367283_m1462559763(__this, method) ((  Slider_t297367283 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.RawImage>()
#define Component_GetComponent_TisRawImage_t2749640213_m1817787565(__this, method) ((  RawImage_t2749640213 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.InputField>()
#define Component_GetComponent_TisInputField_t1631627530_m1177654614(__this, method) ((  InputField_t1631627530 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.BoxSlider>()
#define Component_GetComponent_TisBoxSlider_t1871650694_m3168588160(__this, method) ((  BoxSlider_t1871650694 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Boolean UnityEngine.UI.BoxSlider::SetClass<System.Object>(T&,T)
extern "C"  bool BoxSlider_SetClass_TisIl2CppObject_m1811500378_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject ** ___currentValue0, Il2CppObject * ___newValue1, const MethodInfo* method);
#define BoxSlider_SetClass_TisIl2CppObject_m1811500378(__this /* static, unused */, ___currentValue0, ___newValue1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, const MethodInfo*))BoxSlider_SetClass_TisIl2CppObject_m1811500378_gshared)(__this /* static, unused */, ___currentValue0, ___newValue1, method)
// System.Boolean UnityEngine.UI.BoxSlider::SetClass<UnityEngine.RectTransform>(T&,T)
#define BoxSlider_SetClass_TisRectTransform_t3349966182_m272479997(__this /* static, unused */, ___currentValue0, ___newValue1, method) ((  bool (*) (Il2CppObject * /* static, unused */, RectTransform_t3349966182 **, RectTransform_t3349966182 *, const MethodInfo*))BoxSlider_SetClass_TisIl2CppObject_m1811500378_gshared)(__this /* static, unused */, ___currentValue0, ___newValue1, method)
// System.Boolean UnityEngine.UI.BoxSlider::SetStruct<System.Single>(T&,T)
extern "C"  bool BoxSlider_SetStruct_TisSingle_t2076509932_m2710312218_gshared (Il2CppObject * __this /* static, unused */, float* ___currentValue0, float ___newValue1, const MethodInfo* method);
#define BoxSlider_SetStruct_TisSingle_t2076509932_m2710312218(__this /* static, unused */, ___currentValue0, ___newValue1, method) ((  bool (*) (Il2CppObject * /* static, unused */, float*, float, const MethodInfo*))BoxSlider_SetStruct_TisSingle_t2076509932_m2710312218_gshared)(__this /* static, unused */, ___currentValue0, ___newValue1, method)
// System.Boolean UnityEngine.UI.BoxSlider::SetStruct<System.Boolean>(T&,T)
extern "C"  bool BoxSlider_SetStruct_TisBoolean_t3825574718_m1967921404_gshared (Il2CppObject * __this /* static, unused */, bool* ___currentValue0, bool ___newValue1, const MethodInfo* method);
#define BoxSlider_SetStruct_TisBoolean_t3825574718_m1967921404(__this /* static, unused */, ___currentValue0, ___newValue1, method) ((  bool (*) (Il2CppObject * /* static, unused */, bool*, bool, const MethodInfo*))BoxSlider_SetStruct_TisBoolean_t3825574718_m1967921404_gshared)(__this /* static, unused */, ___currentValue0, ___newValue1, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t3349966182_m1310250299(__this, method) ((  RectTransform_t3349966182 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void _PickerControl::.ctor()
extern "C"  void _PickerControl__ctor_m82678137 (_PickerControl_t2624631720 * __this, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3811852957(&L_0, (0.1f), (0.19f), (0.22f), /*hidden argument*/NULL);
		__this->set_sea_base_color_3(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void _PickerControl::Start()
extern "C"  void _PickerControl_Start_m2315169225 (_PickerControl_t2624631720 * __this, const MethodInfo* method)
{
	{
		ColorPicker_t3035206225 * L_0 = __this->get_picker_2();
		Color_t2020392075  L_1 = __this->get_sea_base_color_3();
		NullCheck(L_0);
		ColorPicker_set_CurrentColor_m2125228471(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void _PickerControl::Update()
extern "C"  void _PickerControl_Update_m2391504050 (_PickerControl_t2624631720 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void _PickerSeaWaterColorControl::.ctor()
extern "C"  void _PickerSeaWaterColorControl__ctor_m2200172044 (_PickerSeaWaterColorControl_t1489839923 * __this, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3811852957(&L_0, (0.8f), (0.9f), (0.6f), /*hidden argument*/NULL);
		__this->set_sea_water_color_3(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void _PickerSeaWaterColorControl::Start()
extern "C"  void _PickerSeaWaterColorControl_Start_m3388375480 (_PickerSeaWaterColorControl_t1489839923 * __this, const MethodInfo* method)
{
	{
		ColorPicker_t3035206225 * L_0 = __this->get_sea_water_color_picker_2();
		Color_t2020392075  L_1 = __this->get_sea_water_color_3();
		NullCheck(L_0);
		ColorPicker_set_CurrentColor_m2125228471(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void _PickerSeaWaterColorControl::Update()
extern "C"  void _PickerSeaWaterColorControl_Update_m1653883871 (_PickerSeaWaterColorControl_t1489839923 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void _PickerSkyColor::.ctor()
extern "C"  void _PickerSkyColor__ctor_m1569764186 (_PickerSkyColor_t979671171 * __this, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3811852957(&L_0, (0.35294f), (0.58039f), (0.79608f), /*hidden argument*/NULL);
		__this->set_sky_color_3(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void _PickerSkyColor::Start()
extern "C"  void _PickerSkyColor_Start_m4067587422 (_PickerSkyColor_t979671171 * __this, const MethodInfo* method)
{
	{
		ColorPicker_t3035206225 * L_0 = __this->get_picker_sky_color_2();
		Color_t2020392075  L_1 = __this->get_sky_color_3();
		NullCheck(L_0);
		ColorPicker_set_CurrentColor_m2125228471(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void _PickerSkyColor::Update()
extern "C"  void _PickerSkyColor_Update_m683868167 (_PickerSkyColor_t979671171 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void _PickerSunColorControl::.ctor()
extern "C"  void _PickerSunColorControl__ctor_m1452739074 (_PickerSunColorControl_t1983561777 * __this, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3811852957(&L_0, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_sun_color_color_3(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void _PickerSunColorControl::Start()
extern "C"  void _PickerSunColorControl_Start_m3187310546 (_PickerSunColorControl_t1983561777 * __this, const MethodInfo* method)
{
	{
		ColorPicker_t3035206225 * L_0 = __this->get_picker_sun_color_2();
		Color_t2020392075  L_1 = __this->get_sun_color_color_3();
		NullCheck(L_0);
		ColorPicker_set_CurrentColor_m2125228471(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void _PickerSunColorControl::Update()
extern "C"  void _PickerSunColorControl_Update_m3201213873 (_PickerSunColorControl_t1983561777 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void _SeaControl::.ctor()
extern "C"  void _SeaControl__ctor_m3015587372 (_SeaControl_t4060510597 * __this, const MethodInfo* method)
{
	{
		__this->set_sea_frequency_2((0.3f));
		__this->set_sea_height_3((0.6f));
		__this->set_sea_choppy_4((4.0f));
		__this->set_sea_speed_5((0.8f));
		__this->set_sun_pos_u_6((1.0f));
		__this->set_camera_height_8((20.0f));
		__this->set_camera_rot_angle_9((45.0f));
		Color_t2020392075  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3811852957(&L_0, (0.8f), (0.9f), (0.6f), /*hidden argument*/NULL);
		__this->set_sea_water_color_11(L_0);
		Color_t2020392075  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Color__ctor_m3811852957(&L_1, (0.1f), (0.19f), (0.22f), /*hidden argument*/NULL);
		__this->set_sea_base_color_12(L_1);
		Color_t2020392075  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Color__ctor_m3811852957(&L_2, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_sun_color_13(L_2);
		Color_t2020392075  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Color__ctor_m3811852957(&L_3, (0.35294f), (0.58039f), (0.79608f), /*hidden argument*/NULL);
		__this->set_sky_color_26(L_3);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void _SeaControl::Start()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3545670481;
extern Il2CppCodeGenString* _stringLiteral3133503666;
extern Il2CppCodeGenString* _stringLiteral1738062422;
extern Il2CppCodeGenString* _stringLiteral148561136;
extern Il2CppCodeGenString* _stringLiteral799262727;
extern Il2CppCodeGenString* _stringLiteral799262758;
extern Il2CppCodeGenString* _stringLiteral50949294;
extern Il2CppCodeGenString* _stringLiteral2349478286;
extern Il2CppCodeGenString* _stringLiteral2140366224;
extern const uint32_t _SeaControl_Start_m3738024332_MetadataUsageId;
extern "C"  void _SeaControl_Start_m3738024332 (_SeaControl_t4060510597 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SeaControl_Start_m3738024332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_1 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)(((float)((float)L_0)))*(float)(0.8f))), /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)(((float)((float)L_2)))*(float)(0.8f))), /*hidden argument*/NULL);
		Screen_SetResolution_m55027544(NULL /*static, unused*/, L_1, L_3, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_Gui_layer_14();
		bool L_5 = __this->get_show_gui_15();
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, L_5, /*hidden argument*/NULL);
		Camera_t189460977 * L_6 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_main_camera_16(L_6);
		float L_7 = __this->get_camera_height_8();
		__this->set_camera_height_8(((-L_7)));
		Camera_t189460977 * L_8 = __this->get_main_camera_16();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(L_8, /*hidden argument*/NULL);
		float L_10 = __this->get_camera_height_8();
		Vector3_t2243707580  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector3__ctor_m2638739322(&L_11, (0.0f), L_10, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_position_m2469242620(L_9, L_11, /*hidden argument*/NULL);
		Text_t356221433 * L_12 = __this->get_sea_frequency_text_22();
		float L_13 = __this->get_sea_frequency_2();
		float L_14 = bankers_roundf(((float)((float)L_13*(float)(100.0f))));
		float L_15 = ((float)((float)L_14/(float)(100.0f)));
		Il2CppObject * L_16 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3545670481, L_16, /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_17);
		Text_t356221433 * L_18 = __this->get_sea_height_text_21();
		float L_19 = __this->get_sea_height_3();
		float L_20 = bankers_roundf(((float)((float)L_19*(float)(100.0f))));
		float L_21 = ((float)((float)L_20/(float)(100.0f)));
		Il2CppObject * L_22 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_21);
		String_t* L_23 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3133503666, L_22, /*hidden argument*/NULL);
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, L_23);
		Text_t356221433 * L_24 = __this->get_sea_choppy_text_20();
		float L_25 = __this->get_sea_choppy_4();
		float L_26 = bankers_roundf(((float)((float)L_25*(float)(100.0f))));
		float L_27 = ((float)((float)L_26/(float)(100.0f)));
		Il2CppObject * L_28 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_27);
		String_t* L_29 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1738062422, L_28, /*hidden argument*/NULL);
		NullCheck(L_24);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_24, L_29);
		Text_t356221433 * L_30 = __this->get_sea_speed_text_19();
		float L_31 = __this->get_sea_speed_5();
		float L_32 = bankers_roundf(((float)((float)L_31*(float)(100.0f))));
		float L_33 = ((float)((float)L_32/(float)(100.0f)));
		Il2CppObject * L_34 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_33);
		String_t* L_35 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral148561136, L_34, /*hidden argument*/NULL);
		NullCheck(L_30);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_30, L_35);
		Text_t356221433 * L_36 = __this->get_sun_pos_u_text_17();
		float L_37 = __this->get_sun_pos_u_6();
		float L_38 = bankers_roundf(((float)((float)L_37*(float)(100.0f))));
		float L_39 = ((float)((float)L_38/(float)(100.0f)));
		Il2CppObject * L_40 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_39);
		String_t* L_41 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral799262727, L_40, /*hidden argument*/NULL);
		NullCheck(L_36);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_36, L_41);
		Text_t356221433 * L_42 = __this->get_sun_pos_v_text_18();
		float L_43 = __this->get_sun_pos_v_7();
		float L_44 = bankers_roundf(((float)((float)L_43*(float)(100.0f))));
		float L_45 = ((float)((float)L_44/(float)(100.0f)));
		Il2CppObject * L_46 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_45);
		String_t* L_47 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral799262758, L_46, /*hidden argument*/NULL);
		NullCheck(L_42);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_42, L_47);
		Text_t356221433 * L_48 = __this->get_sea_movement_speed_text_25();
		float L_49 = __this->get_sea_movement_speed_10();
		float L_50 = bankers_roundf(((float)((float)L_49*(float)(100.0f))));
		float L_51 = ((float)((float)L_50/(float)(100.0f)));
		Il2CppObject * L_52 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_51);
		String_t* L_53 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral50949294, L_52, /*hidden argument*/NULL);
		NullCheck(L_48);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_48, L_53);
		Text_t356221433 * L_54 = __this->get_camera_height_text_23();
		float L_55 = __this->get_camera_height_8();
		float L_56 = bankers_roundf(((float)((float)L_55*(float)(100.0f))));
		float L_57 = ((float)((float)L_56/(float)(100.0f)));
		Il2CppObject * L_58 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_57);
		String_t* L_59 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2349478286, L_58, /*hidden argument*/NULL);
		NullCheck(L_54);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_54, L_59);
		Text_t356221433 * L_60 = __this->get_camera_rot_angle_text_24();
		float L_61 = __this->get_camera_rot_angle_9();
		float L_62 = bankers_roundf(((float)((float)L_61*(float)(100.0f))));
		float L_63 = ((float)((float)L_62/(float)(100.0f)));
		Il2CppObject * L_64 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_63);
		String_t* L_65 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2140366224, L_64, /*hidden argument*/NULL);
		NullCheck(L_60);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_60, L_65);
		return;
	}
}
// System.Void _SeaControl::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral392676615;
extern Il2CppCodeGenString* _stringLiteral776705964;
extern Il2CppCodeGenString* _stringLiteral3885833208;
extern Il2CppCodeGenString* _stringLiteral316756142;
extern Il2CppCodeGenString* _stringLiteral2182586178;
extern Il2CppCodeGenString* _stringLiteral1592637782;
extern Il2CppCodeGenString* _stringLiteral1483095625;
extern Il2CppCodeGenString* _stringLiteral2871124738;
extern Il2CppCodeGenString* _stringLiteral2765494502;
extern Il2CppCodeGenString* _stringLiteral1199410561;
extern Il2CppCodeGenString* _stringLiteral3863559880;
extern Il2CppCodeGenString* _stringLiteral1110997421;
extern const uint32_t _SeaControl_Update_m2073980969_MetadataUsageId;
extern "C"  void _SeaControl_Update_m2073980969 (_SeaControl_t4060510597 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SeaControl_Update_m2073980969_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = RenderSettings_get_skybox_m654921356(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_sea_frequency_2();
		NullCheck(L_0);
		Material_SetFloat_m1926275467(L_0, _stringLiteral392676615, L_1, /*hidden argument*/NULL);
		Material_t193706927 * L_2 = RenderSettings_get_skybox_m654921356(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = __this->get_sea_height_3();
		NullCheck(L_2);
		Material_SetFloat_m1926275467(L_2, _stringLiteral776705964, L_3, /*hidden argument*/NULL);
		Material_t193706927 * L_4 = RenderSettings_get_skybox_m654921356(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_sea_choppy_4();
		NullCheck(L_4);
		Material_SetFloat_m1926275467(L_4, _stringLiteral3885833208, L_5, /*hidden argument*/NULL);
		Material_t193706927 * L_6 = RenderSettings_get_skybox_m654921356(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = __this->get_sea_speed_5();
		NullCheck(L_6);
		Material_SetFloat_m1926275467(L_6, _stringLiteral316756142, L_7, /*hidden argument*/NULL);
		Material_t193706927 * L_8 = RenderSettings_get_skybox_m654921356(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t2020392075  L_9 = __this->get_sea_water_color_11();
		NullCheck(L_8);
		Material_SetColor_m650857509(L_8, _stringLiteral2182586178, L_9, /*hidden argument*/NULL);
		Material_t193706927 * L_10 = RenderSettings_get_skybox_m654921356(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t2020392075  L_11 = __this->get_sea_base_color_12();
		NullCheck(L_10);
		Material_SetColor_m650857509(L_10, _stringLiteral1592637782, L_11, /*hidden argument*/NULL);
		Material_t193706927 * L_12 = RenderSettings_get_skybox_m654921356(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t2020392075  L_13 = __this->get_sun_color_13();
		NullCheck(L_12);
		Material_SetColor_m650857509(L_12, _stringLiteral1483095625, L_13, /*hidden argument*/NULL);
		Material_t193706927 * L_14 = RenderSettings_get_skybox_m654921356(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_15 = __this->get_sea_movement_speed_10();
		NullCheck(L_14);
		Material_SetFloat_m1926275467(L_14, _stringLiteral2871124738, L_15, /*hidden argument*/NULL);
		Material_t193706927 * L_16 = RenderSettings_get_skybox_m654921356(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_17 = __this->get_sun_pos_u_6();
		NullCheck(L_16);
		Material_SetFloat_m1926275467(L_16, _stringLiteral2765494502, L_17, /*hidden argument*/NULL);
		Material_t193706927 * L_18 = RenderSettings_get_skybox_m654921356(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_19 = __this->get_sun_pos_v_7();
		NullCheck(L_18);
		Material_SetFloat_m1926275467(L_18, _stringLiteral1199410561, L_19, /*hidden argument*/NULL);
		Material_t193706927 * L_20 = RenderSettings_get_skybox_m654921356(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t2020392075  L_21 = __this->get_sky_color_26();
		NullCheck(L_20);
		Material_SetColor_m650857509(L_20, _stringLiteral3863559880, L_21, /*hidden argument*/NULL);
		Camera_t189460977 * L_22 = __this->get_main_camera_16();
		NullCheck(L_22);
		Transform_t3275118058 * L_23 = Component_get_transform_m2697483695(L_22, /*hidden argument*/NULL);
		float L_24 = __this->get_camera_height_8();
		Vector3_t2243707580  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector3__ctor_m2638739322(&L_25, (0.0f), L_24, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_set_position_m2469242620(L_23, L_25, /*hidden argument*/NULL);
		Camera_t189460977 * L_26 = __this->get_main_camera_16();
		NullCheck(L_26);
		Transform_t3275118058 * L_27 = Component_get_transform_m2697483695(L_26, /*hidden argument*/NULL);
		float L_28 = __this->get_camera_rot_angle_9();
		Vector3_t2243707580  L_29;
		memset(&L_29, 0, sizeof(L_29));
		Vector3__ctor_m2638739322(&L_29, L_28, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_eulerAngles_m2881310872(L_27, L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_30 = Input_GetKeyUp_m1952023453(NULL /*static, unused*/, _stringLiteral1110997421, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0145;
		}
	}
	{
		Application_Quit_m3885595876(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0145:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_31 = Input_GetKeyUp_m1008512962(NULL /*static, unused*/, ((int32_t)282), /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_0160;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_32 = Input_GetKeyUp_m1008512962(NULL /*static, unused*/, ((int32_t)105), /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0180;
		}
	}

IL_0160:
	{
		bool L_33 = __this->get_show_gui_15();
		__this->set_show_gui_15((bool)((((int32_t)L_33) == ((int32_t)0))? 1 : 0));
		GameObject_t1756533147 * L_34 = __this->get_Gui_layer_14();
		bool L_35 = __this->get_show_gui_15();
		NullCheck(L_34);
		GameObject_SetActive_m2887581199(L_34, L_35, /*hidden argument*/NULL);
	}

IL_0180:
	{
		return;
	}
}
// System.Void _SeaControl::ChangeSeaFreq(System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3545670481;
extern const uint32_t _SeaControl_ChangeSeaFreq_m2565305150_MetadataUsageId;
extern "C"  void _SeaControl_ChangeSeaFreq_m2565305150 (_SeaControl_t4060510597 * __this, float ___newSeaFreq0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SeaControl_ChangeSeaFreq_m2565305150_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___newSeaFreq0;
		__this->set_sea_frequency_2(L_0);
		Text_t356221433 * L_1 = __this->get_sea_frequency_text_22();
		float L_2 = __this->get_sea_frequency_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = bankers_roundf(((float)((float)L_2*(float)(100.0f))));
		float L_4 = ((float)((float)L_3/(float)(100.0f)));
		Il2CppObject * L_5 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3545670481, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_6);
		return;
	}
}
// System.Void _SeaControl::ChangeSeaHeight(System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3133503666;
extern const uint32_t _SeaControl_ChangeSeaHeight_m4220722243_MetadataUsageId;
extern "C"  void _SeaControl_ChangeSeaHeight_m4220722243 (_SeaControl_t4060510597 * __this, float ___newSeaHeight0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SeaControl_ChangeSeaHeight_m4220722243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___newSeaHeight0;
		__this->set_sea_height_3(L_0);
		Text_t356221433 * L_1 = __this->get_sea_height_text_21();
		float L_2 = __this->get_sea_height_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = bankers_roundf(((float)((float)L_2*(float)(100.0f))));
		float L_4 = ((float)((float)L_3/(float)(100.0f)));
		Il2CppObject * L_5 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3133503666, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_6);
		return;
	}
}
// System.Void _SeaControl::ChangeSeaChoppy(System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1738062422;
extern const uint32_t _SeaControl_ChangeSeaChoppy_m1268845189_MetadataUsageId;
extern "C"  void _SeaControl_ChangeSeaChoppy_m1268845189 (_SeaControl_t4060510597 * __this, float ___newSeaChoppy0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SeaControl_ChangeSeaChoppy_m1268845189_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___newSeaChoppy0;
		__this->set_sea_choppy_4(L_0);
		Text_t356221433 * L_1 = __this->get_sea_choppy_text_20();
		float L_2 = __this->get_sea_choppy_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = bankers_roundf(((float)((float)L_2*(float)(100.0f))));
		float L_4 = ((float)((float)L_3/(float)(100.0f)));
		Il2CppObject * L_5 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1738062422, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_6);
		return;
	}
}
// System.Void _SeaControl::ChangeSeaSpeed(System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral148561136;
extern const uint32_t _SeaControl_ChangeSeaSpeed_m1480111079_MetadataUsageId;
extern "C"  void _SeaControl_ChangeSeaSpeed_m1480111079 (_SeaControl_t4060510597 * __this, float ___newSeaSpeed0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SeaControl_ChangeSeaSpeed_m1480111079_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___newSeaSpeed0;
		__this->set_sea_speed_5(L_0);
		Text_t356221433 * L_1 = __this->get_sea_speed_text_19();
		float L_2 = __this->get_sea_speed_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = bankers_roundf(((float)((float)L_2*(float)(100.0f))));
		float L_4 = ((float)((float)L_3/(float)(100.0f)));
		Il2CppObject * L_5 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral148561136, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_6);
		return;
	}
}
// System.Void _SeaControl::ChangeSeaWaterColor(UnityEngine.Color)
extern "C"  void _SeaControl_ChangeSeaWaterColor_m3629580051 (_SeaControl_t4060510597 * __this, Color_t2020392075  ___newSeaWaterColor0, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = ___newSeaWaterColor0;
		__this->set_sea_water_color_11(L_0);
		return;
	}
}
// System.Void _SeaControl::ChangeSeaBaseColor(UnityEngine.Color)
extern "C"  void _SeaControl_ChangeSeaBaseColor_m1177439755 (_SeaControl_t4060510597 * __this, Color_t2020392075  ___newSeaBaseColor0, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = ___newSeaBaseColor0;
		__this->set_sea_base_color_12(L_0);
		return;
	}
}
// System.Void _SeaControl::ChangeSunColor(UnityEngine.Color)
extern "C"  void _SeaControl_ChangeSunColor_m3190758425 (_SeaControl_t4060510597 * __this, Color_t2020392075  ___newSunColor0, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = ___newSunColor0;
		__this->set_sun_color_13(L_0);
		return;
	}
}
// System.Void _SeaControl::ChangeSunPosU(System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral799262727;
extern const uint32_t _SeaControl_ChangeSunPosU_m2897449384_MetadataUsageId;
extern "C"  void _SeaControl_ChangeSunPosU_m2897449384 (_SeaControl_t4060510597 * __this, float ___newSunPosU0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SeaControl_ChangeSunPosU_m2897449384_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___newSunPosU0;
		__this->set_sun_pos_u_6(L_0);
		Text_t356221433 * L_1 = __this->get_sun_pos_u_text_17();
		float L_2 = __this->get_sun_pos_u_6();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = bankers_roundf(((float)((float)L_2*(float)(100.0f))));
		float L_4 = ((float)((float)L_3/(float)(100.0f)));
		Il2CppObject * L_5 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral799262727, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_6);
		return;
	}
}
// System.Void _SeaControl::ChangeSunPosV(System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral799262758;
extern const uint32_t _SeaControl_ChangeSunPosV_m893575213_MetadataUsageId;
extern "C"  void _SeaControl_ChangeSunPosV_m893575213 (_SeaControl_t4060510597 * __this, float ___newSunPosV0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SeaControl_ChangeSunPosV_m893575213_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___newSunPosV0;
		__this->set_sun_pos_v_7(L_0);
		Text_t356221433 * L_1 = __this->get_sun_pos_v_text_18();
		float L_2 = __this->get_sun_pos_v_7();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = bankers_roundf(((float)((float)L_2*(float)(100.0f))));
		float L_4 = ((float)((float)L_3/(float)(100.0f)));
		Il2CppObject * L_5 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral799262758, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_6);
		return;
	}
}
// System.Void _SeaControl::ChangeSeaMovementSpeed(System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral50949294;
extern const uint32_t _SeaControl_ChangeSeaMovementSpeed_m3088995436_MetadataUsageId;
extern "C"  void _SeaControl_ChangeSeaMovementSpeed_m3088995436 (_SeaControl_t4060510597 * __this, float ___newSeaMovementSpeed0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SeaControl_ChangeSeaMovementSpeed_m3088995436_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___newSeaMovementSpeed0;
		__this->set_sea_movement_speed_10(L_0);
		Text_t356221433 * L_1 = __this->get_sea_movement_speed_text_25();
		float L_2 = ___newSeaMovementSpeed0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = bankers_roundf(((float)((float)L_2*(float)(100.0f))));
		float L_4 = ((float)((float)L_3/(float)(100.0f)));
		Il2CppObject * L_5 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral50949294, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_6);
		return;
	}
}
// System.Void _SeaControl::ChangeCameraHeight(System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2349478286;
extern const uint32_t _SeaControl_ChangeCameraHeight_m1236121629_MetadataUsageId;
extern "C"  void _SeaControl_ChangeCameraHeight_m1236121629 (_SeaControl_t4060510597 * __this, float ___newCameraHeight0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SeaControl_ChangeCameraHeight_m1236121629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___newCameraHeight0;
		__this->set_camera_height_8(L_0);
		Text_t356221433 * L_1 = __this->get_camera_height_text_23();
		float L_2 = __this->get_camera_height_8();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = bankers_roundf(((float)((float)L_2*(float)(100.0f))));
		float L_4 = ((float)((float)L_3/(float)(100.0f)));
		Il2CppObject * L_5 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2349478286, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_6);
		return;
	}
}
// System.Void _SeaControl::ChangeCameraRotAngle(System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2140366224;
extern const uint32_t _SeaControl_ChangeCameraRotAngle_m4012972818_MetadataUsageId;
extern "C"  void _SeaControl_ChangeCameraRotAngle_m4012972818 (_SeaControl_t4060510597 * __this, float ___newCameraRotAngle0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_SeaControl_ChangeCameraRotAngle_m4012972818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___newCameraRotAngle0;
		__this->set_camera_rot_angle_9(L_0);
		Text_t356221433 * L_1 = __this->get_camera_rot_angle_text_24();
		float L_2 = __this->get_camera_rot_angle_9();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = bankers_roundf(((float)((float)L_2*(float)(100.0f))));
		float L_4 = ((float)((float)L_3/(float)(100.0f)));
		Il2CppObject * L_5 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2140366224, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_6);
		return;
	}
}
// System.Void _SeaControl::ChangeSkyColor(UnityEngine.Color)
extern "C"  void _SeaControl_ChangeSkyColor_m1581686058 (_SeaControl_t4060510597 * __this, Color_t2020392075  ___newSkyColor0, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = ___newSkyColor0;
		__this->set_sky_color_26(L_0);
		return;
	}
}
// System.Void ColorChangedEvent::.ctor()
extern const MethodInfo* UnityEvent_1__ctor_m117795578_MethodInfo_var;
extern const uint32_t ColorChangedEvent__ctor_m3710136698_MetadataUsageId;
extern "C"  void ColorChangedEvent__ctor_m3710136698 (ColorChangedEvent_t2990895397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorChangedEvent__ctor_m3710136698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m117795578(__this, /*hidden argument*/UnityEvent_1__ctor_m117795578_MethodInfo_var);
		return;
	}
}
// System.Void ColorImage::.ctor()
extern "C"  void ColorImage__ctor_m797732221 (ColorImage_t3157136356 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorImage::Awake()
extern Il2CppClass* UnityAction_1_t3386977826_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var;
extern const MethodInfo* ColorImage_ColorChanged_m2780022934_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3329809356_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m903508446_MethodInfo_var;
extern const uint32_t ColorImage_Awake_m1432153760_MetadataUsageId;
extern "C"  void ColorImage_Awake_m1432153760 (ColorImage_t3157136356 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorImage_Awake_m1432153760_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Image_t2042527209 * L_0 = Component_GetComponent_TisImage_t2042527209_m2189462422(__this, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		__this->set_image_3(L_0);
		ColorPicker_t3035206225 * L_1 = __this->get_picker_2();
		NullCheck(L_1);
		ColorChangedEvent_t2990895397 * L_2 = L_1->get_onValueChanged_9();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)ColorImage_ColorChanged_m2780022934_MethodInfo_var);
		UnityAction_1_t3386977826 * L_4 = (UnityAction_1_t3386977826 *)il2cpp_codegen_object_new(UnityAction_1_t3386977826_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3329809356(L_4, __this, L_3, /*hidden argument*/UnityAction_1__ctor_m3329809356_MethodInfo_var);
		NullCheck(L_2);
		UnityEvent_1_AddListener_m903508446(L_2, L_4, /*hidden argument*/UnityEvent_1_AddListener_m903508446_MethodInfo_var);
		return;
	}
}
// System.Void ColorImage::OnDestroy()
extern Il2CppClass* UnityAction_1_t3386977826_il2cpp_TypeInfo_var;
extern const MethodInfo* ColorImage_ColorChanged_m2780022934_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3329809356_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_RemoveListener_m1138414664_MethodInfo_var;
extern const uint32_t ColorImage_OnDestroy_m537617202_MetadataUsageId;
extern "C"  void ColorImage_OnDestroy_m537617202 (ColorImage_t3157136356 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorImage_OnDestroy_m537617202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorPicker_t3035206225 * L_0 = __this->get_picker_2();
		NullCheck(L_0);
		ColorChangedEvent_t2990895397 * L_1 = L_0->get_onValueChanged_9();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ColorImage_ColorChanged_m2780022934_MethodInfo_var);
		UnityAction_1_t3386977826 * L_3 = (UnityAction_1_t3386977826 *)il2cpp_codegen_object_new(UnityAction_1_t3386977826_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3329809356(L_3, __this, L_2, /*hidden argument*/UnityAction_1__ctor_m3329809356_MethodInfo_var);
		NullCheck(L_1);
		UnityEvent_1_RemoveListener_m1138414664(L_1, L_3, /*hidden argument*/UnityEvent_1_RemoveListener_m1138414664_MethodInfo_var);
		return;
	}
}
// System.Void ColorImage::ColorChanged(UnityEngine.Color)
extern "C"  void ColorImage_ColorChanged_m2780022934 (ColorImage_t3157136356 * __this, Color_t2020392075  ___newColor0, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_image_3();
		Color_t2020392075  L_1 = ___newColor0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
// System.Void ColorLabel::.ctor()
extern Il2CppCodeGenString* _stringLiteral2880467378;
extern const uint32_t ColorLabel__ctor_m3361268158_MetadataUsageId;
extern "C"  void ColorLabel__ctor_m3361268158 (ColorLabel_t1884607337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorLabel__ctor_m3361268158_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_prefix_4(_stringLiteral2880467378);
		__this->set_maxValue_6((255.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorLabel::Awake()
extern const MethodInfo* Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var;
extern const uint32_t ColorLabel_Awake_m1477595641_MetadataUsageId;
extern "C"  void ColorLabel_Awake_m1477595641 (ColorLabel_t1884607337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorLabel_Awake_m1477595641_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = Component_GetComponent_TisText_t356221433_m1342661039(__this, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		__this->set_label_8(L_0);
		return;
	}
}
// System.Void ColorLabel::OnEnable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_1_t3386977826_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_3_t235051313_il2cpp_TypeInfo_var;
extern const MethodInfo* ColorLabel_ColorChanged_m35916549_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3329809356_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m903508446_MethodInfo_var;
extern const MethodInfo* ColorLabel_HSVChanged_m3658710332_MethodInfo_var;
extern const MethodInfo* UnityAction_3__ctor_m3626891334_MethodInfo_var;
extern const MethodInfo* UnityEvent_3_AddListener_m3608966849_MethodInfo_var;
extern const uint32_t ColorLabel_OnEnable_m3391994902_MetadataUsageId;
extern "C"  void ColorLabel_OnEnable_m3391994902 (ColorLabel_t1884607337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorLabel_OnEnable_m3391994902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0053;
		}
	}
	{
		ColorPicker_t3035206225 * L_1 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0053;
		}
	}
	{
		ColorPicker_t3035206225 * L_3 = __this->get_picker_2();
		NullCheck(L_3);
		ColorChangedEvent_t2990895397 * L_4 = L_3->get_onValueChanged_9();
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)ColorLabel_ColorChanged_m35916549_MethodInfo_var);
		UnityAction_1_t3386977826 * L_6 = (UnityAction_1_t3386977826 *)il2cpp_codegen_object_new(UnityAction_1_t3386977826_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3329809356(L_6, __this, L_5, /*hidden argument*/UnityAction_1__ctor_m3329809356_MethodInfo_var);
		NullCheck(L_4);
		UnityEvent_1_AddListener_m903508446(L_4, L_6, /*hidden argument*/UnityEvent_1_AddListener_m903508446_MethodInfo_var);
		ColorPicker_t3035206225 * L_7 = __this->get_picker_2();
		NullCheck(L_7);
		HSVChangedEvent_t1170297569 * L_8 = L_7->get_onHSVChanged_10();
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)ColorLabel_HSVChanged_m3658710332_MethodInfo_var);
		UnityAction_3_t235051313 * L_10 = (UnityAction_3_t235051313 *)il2cpp_codegen_object_new(UnityAction_3_t235051313_il2cpp_TypeInfo_var);
		UnityAction_3__ctor_m3626891334(L_10, __this, L_9, /*hidden argument*/UnityAction_3__ctor_m3626891334_MethodInfo_var);
		NullCheck(L_8);
		UnityEvent_3_AddListener_m3608966849(L_8, L_10, /*hidden argument*/UnityEvent_3_AddListener_m3608966849_MethodInfo_var);
	}

IL_0053:
	{
		return;
	}
}
// System.Void ColorLabel::OnDestroy()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_1_t3386977826_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_3_t235051313_il2cpp_TypeInfo_var;
extern const MethodInfo* ColorLabel_ColorChanged_m35916549_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3329809356_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_RemoveListener_m1138414664_MethodInfo_var;
extern const MethodInfo* ColorLabel_HSVChanged_m3658710332_MethodInfo_var;
extern const MethodInfo* UnityAction_3__ctor_m3626891334_MethodInfo_var;
extern const MethodInfo* UnityEvent_3_RemoveListener_m2407167912_MethodInfo_var;
extern const uint32_t ColorLabel_OnDestroy_m648294215_MetadataUsageId;
extern "C"  void ColorLabel_OnDestroy_m648294215 (ColorLabel_t1884607337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorLabel_OnDestroy_m648294215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorPicker_t3035206225 * L_0 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0049;
		}
	}
	{
		ColorPicker_t3035206225 * L_2 = __this->get_picker_2();
		NullCheck(L_2);
		ColorChangedEvent_t2990895397 * L_3 = L_2->get_onValueChanged_9();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)ColorLabel_ColorChanged_m35916549_MethodInfo_var);
		UnityAction_1_t3386977826 * L_5 = (UnityAction_1_t3386977826 *)il2cpp_codegen_object_new(UnityAction_1_t3386977826_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3329809356(L_5, __this, L_4, /*hidden argument*/UnityAction_1__ctor_m3329809356_MethodInfo_var);
		NullCheck(L_3);
		UnityEvent_1_RemoveListener_m1138414664(L_3, L_5, /*hidden argument*/UnityEvent_1_RemoveListener_m1138414664_MethodInfo_var);
		ColorPicker_t3035206225 * L_6 = __this->get_picker_2();
		NullCheck(L_6);
		HSVChangedEvent_t1170297569 * L_7 = L_6->get_onHSVChanged_10();
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)ColorLabel_HSVChanged_m3658710332_MethodInfo_var);
		UnityAction_3_t235051313 * L_9 = (UnityAction_3_t235051313 *)il2cpp_codegen_object_new(UnityAction_3_t235051313_il2cpp_TypeInfo_var);
		UnityAction_3__ctor_m3626891334(L_9, __this, L_8, /*hidden argument*/UnityAction_3__ctor_m3626891334_MethodInfo_var);
		NullCheck(L_7);
		UnityEvent_3_RemoveListener_m2407167912(L_7, L_9, /*hidden argument*/UnityEvent_3_RemoveListener_m2407167912_MethodInfo_var);
	}

IL_0049:
	{
		return;
	}
}
// System.Void ColorLabel::ColorChanged(UnityEngine.Color)
extern "C"  void ColorLabel_ColorChanged_m35916549 (ColorLabel_t1884607337 * __this, Color_t2020392075  ___color0, const MethodInfo* method)
{
	{
		ColorLabel_UpdateValue_m3108932048(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorLabel::HSVChanged(System.Single,System.Single,System.Single)
extern "C"  void ColorLabel_HSVChanged_m3658710332 (ColorLabel_t1884607337 * __this, float ___hue0, float ___sateration1, float ___value2, const MethodInfo* method)
{
	{
		ColorLabel_UpdateValue_m3108932048(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorLabel::UpdateValue()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029313;
extern const uint32_t ColorLabel_UpdateValue_m3108932048_MetadataUsageId;
extern "C"  void ColorLabel_UpdateValue_m3108932048 (ColorLabel_t1884607337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorLabel_UpdateValue_m3108932048_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		ColorPicker_t3035206225 * L_0 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		Text_t356221433 * L_2 = __this->get_label_8();
		String_t* L_3 = __this->get_prefix_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2596409543(NULL /*static, unused*/, L_3, _stringLiteral372029313, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_4);
		goto IL_0075;
	}

IL_0031:
	{
		float L_5 = __this->get_minValue_5();
		ColorPicker_t3035206225 * L_6 = __this->get_picker_2();
		int32_t L_7 = __this->get_type_3();
		NullCheck(L_6);
		float L_8 = ColorPicker_GetValue_m1991475278(L_6, L_7, /*hidden argument*/NULL);
		float L_9 = __this->get_maxValue_6();
		float L_10 = __this->get_minValue_5();
		V_0 = ((float)((float)L_5+(float)((float)((float)L_8*(float)((float)((float)L_9-(float)L_10))))));
		Text_t356221433 * L_11 = __this->get_label_8();
		String_t* L_12 = __this->get_prefix_4();
		float L_13 = V_0;
		String_t* L_14 = ColorLabel_ConvertToDisplayString_m1654160419(__this, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m2596409543(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, L_15);
	}

IL_0075:
	{
		return;
	}
}
// System.String ColorLabel::ConvertToDisplayString(System.Single)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral811305532;
extern const uint32_t ColorLabel_ConvertToDisplayString_m1654160419_MetadataUsageId;
extern "C"  String_t* ColorLabel_ConvertToDisplayString_m1654160419 (ColorLabel_t1884607337 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorLabel_ConvertToDisplayString_m1654160419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_precision_7();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_1 = __this->get_precision_7();
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral811305532, L_3, /*hidden argument*/NULL);
		String_t* L_5 = Single_ToString_m2359963436((&___value0), L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0029:
	{
		float L_6 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_FloorToInt_m4005035722(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		String_t* L_8 = Int32_ToString_m2960866144((&V_0), /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void ColorPicker::.ctor()
extern Il2CppClass* ColorChangedEvent_t2990895397_il2cpp_TypeInfo_var;
extern Il2CppClass* HSVChangedEvent_t1170297569_il2cpp_TypeInfo_var;
extern const uint32_t ColorPicker__ctor_m1239957560_MetadataUsageId;
extern "C"  void ColorPicker__ctor_m1239957560 (ColorPicker_t3035206225 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorPicker__ctor_m1239957560_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set__alpha_8((1.0f));
		ColorChangedEvent_t2990895397 * L_0 = (ColorChangedEvent_t2990895397 *)il2cpp_codegen_object_new(ColorChangedEvent_t2990895397_il2cpp_TypeInfo_var);
		ColorChangedEvent__ctor_m3710136698(L_0, /*hidden argument*/NULL);
		__this->set_onValueChanged_9(L_0);
		HSVChangedEvent_t1170297569 * L_1 = (HSVChangedEvent_t1170297569 *)il2cpp_codegen_object_new(HSVChangedEvent_t1170297569_il2cpp_TypeInfo_var);
		HSVChangedEvent__ctor_m3043072144(L_1, /*hidden argument*/NULL);
		__this->set_onHSVChanged_10(L_1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color ColorPicker::get_CurrentColor()
extern "C"  Color_t2020392075  ColorPicker_get_CurrentColor_m3166096170 (ColorPicker_t3035206225 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__red_5();
		float L_1 = __this->get__green_6();
		float L_2 = __this->get__blue_7();
		float L_3 = __this->get__alpha_8();
		Color_t2020392075  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color__ctor_m1909920690(&L_4, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void ColorPicker::set_CurrentColor(UnityEngine.Color)
extern "C"  void ColorPicker_set_CurrentColor_m2125228471 (ColorPicker_t3035206225 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = ColorPicker_get_CurrentColor_m3166096170(__this, /*hidden argument*/NULL);
		Color_t2020392075  L_1 = ___value0;
		bool L_2 = Color_op_Equality_m3156451394(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		float L_3 = (&___value0)->get_r_0();
		__this->set__red_5(L_3);
		float L_4 = (&___value0)->get_g_1();
		__this->set__green_6(L_4);
		float L_5 = (&___value0)->get_b_2();
		__this->set__blue_7(L_5);
		float L_6 = (&___value0)->get_a_3();
		__this->set__alpha_8(L_6);
		ColorPicker_RGBChanged_m2442023581(__this, /*hidden argument*/NULL);
		ColorPicker_SendChangedEvent_m2880248376(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorPicker::Start()
extern "C"  void ColorPicker_Start_m1519077976 (ColorPicker_t3035206225 * __this, const MethodInfo* method)
{
	{
		ColorPicker_SendChangedEvent_m2880248376(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ColorPicker::get_H()
extern "C"  float ColorPicker_get_H_m3860865411 (ColorPicker_t3035206225 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__hue_2();
		return L_0;
	}
}
// System.Void ColorPicker::set_H(System.Single)
extern "C"  void ColorPicker_set_H_m2725867100 (ColorPicker_t3035206225 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = __this->get__hue_2();
		float L_1 = ___value0;
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		float L_2 = ___value0;
		__this->set__hue_2(L_2);
		ColorPicker_HSVChanged_m3054464207(__this, /*hidden argument*/NULL);
		ColorPicker_SendChangedEvent_m2880248376(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ColorPicker::get_S()
extern "C"  float ColorPicker_get_S_m2449498732 (ColorPicker_t3035206225 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__saturation_3();
		return L_0;
	}
}
// System.Void ColorPicker::set_S(System.Single)
extern "C"  void ColorPicker_set_S_m985865155 (ColorPicker_t3035206225 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = __this->get__saturation_3();
		float L_1 = ___value0;
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		float L_2 = ___value0;
		__this->set__saturation_3(L_2);
		ColorPicker_HSVChanged_m3054464207(__this, /*hidden argument*/NULL);
		ColorPicker_SendChangedEvent_m2880248376(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ColorPicker::get_V()
extern "C"  float ColorPicker_get_V_m265062405 (ColorPicker_t3035206225 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__brightness_4();
		return L_0;
	}
}
// System.Void ColorPicker::set_V(System.Single)
extern "C"  void ColorPicker_set_V_m1877583698 (ColorPicker_t3035206225 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = __this->get__brightness_4();
		float L_1 = ___value0;
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		float L_2 = ___value0;
		__this->set__brightness_4(L_2);
		ColorPicker_HSVChanged_m3054464207(__this, /*hidden argument*/NULL);
		ColorPicker_SendChangedEvent_m2880248376(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ColorPicker::get_R()
extern "C"  float ColorPicker_get_R_m3995379697 (ColorPicker_t3035206225 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__red_5();
		return L_0;
	}
}
// System.Void ColorPicker::set_R(System.Single)
extern "C"  void ColorPicker_set_R_m350597694 (ColorPicker_t3035206225 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = __this->get__red_5();
		float L_1 = ___value0;
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		float L_2 = ___value0;
		__this->set__red_5(L_2);
		ColorPicker_RGBChanged_m2442023581(__this, /*hidden argument*/NULL);
		ColorPicker_SendChangedEvent_m2880248376(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ColorPicker::get_G()
extern "C"  float ColorPicker_get_G_m755548720 (ColorPicker_t3035206225 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__green_6();
		return L_0;
	}
}
// System.Void ColorPicker::set_G(System.Single)
extern "C"  void ColorPicker_set_G_m2188343591 (ColorPicker_t3035206225 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = __this->get__green_6();
		float L_1 = ___value0;
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		float L_2 = ___value0;
		__this->set__green_6(L_2);
		ColorPicker_RGBChanged_m2442023581(__this, /*hidden argument*/NULL);
		ColorPicker_SendChangedEvent_m2880248376(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ColorPicker::get_B()
extern "C"  float ColorPicker_get_B_m1736779681 (ColorPicker_t3035206225 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__blue_7();
		return L_0;
	}
}
// System.Void ColorPicker::set_B(System.Single)
extern "C"  void ColorPicker_set_B_m26090126 (ColorPicker_t3035206225 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = __this->get__blue_7();
		float L_1 = ___value0;
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		float L_2 = ___value0;
		__this->set__blue_7(L_2);
		ColorPicker_RGBChanged_m2442023581(__this, /*hidden argument*/NULL);
		ColorPicker_SendChangedEvent_m2880248376(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ColorPicker::get_A()
extern "C"  float ColorPicker_get_A_m473223718 (ColorPicker_t3035206225 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__alpha_8();
		return L_0;
	}
}
// System.Void ColorPicker::set_A(System.Single)
extern "C"  void ColorPicker_set_A_m1839123465 (ColorPicker_t3035206225 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = __this->get__alpha_8();
		float L_1 = ___value0;
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		float L_2 = ___value0;
		__this->set__alpha_8(L_2);
		ColorPicker_SendChangedEvent_m2880248376(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorPicker::RGBChanged()
extern "C"  void ColorPicker_RGBChanged_m2442023581 (ColorPicker_t3035206225 * __this, const MethodInfo* method)
{
	HsvColor_t1057062332  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2020392075  L_0 = ColorPicker_get_CurrentColor_m3166096170(__this, /*hidden argument*/NULL);
		HsvColor_t1057062332  L_1 = HSVUtil_ConvertRgbToHsv_m4088715697(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = HsvColor_get_normalizedH_m3607787917((&V_0), /*hidden argument*/NULL);
		__this->set__hue_2(L_2);
		float L_3 = HsvColor_get_normalizedS_m3607786980((&V_0), /*hidden argument*/NULL);
		__this->set__saturation_3(L_3);
		float L_4 = HsvColor_get_normalizedV_m3607786943((&V_0), /*hidden argument*/NULL);
		__this->set__brightness_4(L_4);
		return;
	}
}
// System.Void ColorPicker::HSVChanged()
extern "C"  void ColorPicker_HSVChanged_m3054464207 (ColorPicker_t3035206225 * __this, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get__hue_2();
		float L_1 = __this->get__saturation_3();
		float L_2 = __this->get__brightness_4();
		float L_3 = __this->get__alpha_8();
		Color_t2020392075  L_4 = HSVUtil_ConvertHsvToRgb_m2339284600(NULL /*static, unused*/, (((double)((double)((float)((float)L_0*(float)(360.0f)))))), (((double)((double)L_1))), (((double)((double)L_2))), L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_r_0();
		__this->set__red_5(L_5);
		float L_6 = (&V_0)->get_g_1();
		__this->set__green_6(L_6);
		float L_7 = (&V_0)->get_b_2();
		__this->set__blue_7(L_7);
		return;
	}
}
// System.Void ColorPicker::SendChangedEvent()
extern const MethodInfo* UnityEvent_1_Invoke_m2213115825_MethodInfo_var;
extern const MethodInfo* UnityEvent_3_Invoke_m2734200716_MethodInfo_var;
extern const uint32_t ColorPicker_SendChangedEvent_m2880248376_MetadataUsageId;
extern "C"  void ColorPicker_SendChangedEvent_m2880248376 (ColorPicker_t3035206225 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorPicker_SendChangedEvent_m2880248376_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorChangedEvent_t2990895397 * L_0 = __this->get_onValueChanged_9();
		Color_t2020392075  L_1 = ColorPicker_get_CurrentColor_m3166096170(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		UnityEvent_1_Invoke_m2213115825(L_0, L_1, /*hidden argument*/UnityEvent_1_Invoke_m2213115825_MethodInfo_var);
		HSVChangedEvent_t1170297569 * L_2 = __this->get_onHSVChanged_10();
		float L_3 = __this->get__hue_2();
		float L_4 = __this->get__saturation_3();
		float L_5 = __this->get__brightness_4();
		NullCheck(L_2);
		UnityEvent_3_Invoke_m2734200716(L_2, L_3, L_4, L_5, /*hidden argument*/UnityEvent_3_Invoke_m2734200716_MethodInfo_var);
		return;
	}
}
// System.Void ColorPicker::AssignColor(ColorValues,System.Single)
extern "C"  void ColorPicker_AssignColor_m579716850 (ColorPicker_t3035206225 * __this, int32_t ___type0, float ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___type0;
		if (L_0 == 0)
		{
			goto IL_0027;
		}
		if (L_0 == 1)
		{
			goto IL_0033;
		}
		if (L_0 == 2)
		{
			goto IL_003f;
		}
		if (L_0 == 3)
		{
			goto IL_004b;
		}
		if (L_0 == 4)
		{
			goto IL_0057;
		}
		if (L_0 == 5)
		{
			goto IL_0063;
		}
		if (L_0 == 6)
		{
			goto IL_006f;
		}
	}
	{
		goto IL_007b;
	}

IL_0027:
	{
		float L_1 = ___value1;
		ColorPicker_set_R_m350597694(__this, L_1, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_0033:
	{
		float L_2 = ___value1;
		ColorPicker_set_G_m2188343591(__this, L_2, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_003f:
	{
		float L_3 = ___value1;
		ColorPicker_set_B_m26090126(__this, L_3, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_004b:
	{
		float L_4 = ___value1;
		ColorPicker_set_A_m1839123465(__this, L_4, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_0057:
	{
		float L_5 = ___value1;
		ColorPicker_set_H_m2725867100(__this, L_5, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_0063:
	{
		float L_6 = ___value1;
		ColorPicker_set_S_m985865155(__this, L_6, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_006f:
	{
		float L_7 = ___value1;
		ColorPicker_set_V_m1877583698(__this, L_7, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_007b:
	{
		goto IL_0080;
	}

IL_0080:
	{
		return;
	}
}
// System.Single ColorPicker::GetValue(ColorValues)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ColorPicker_GetValue_m1991475278_MetadataUsageId;
extern "C"  float ColorPicker_GetValue_m1991475278 (ColorPicker_t3035206225 * __this, int32_t ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorPicker_GetValue_m1991475278_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___type0;
		if (L_0 == 0)
		{
			goto IL_0027;
		}
		if (L_0 == 1)
		{
			goto IL_002e;
		}
		if (L_0 == 2)
		{
			goto IL_0035;
		}
		if (L_0 == 3)
		{
			goto IL_003c;
		}
		if (L_0 == 4)
		{
			goto IL_0043;
		}
		if (L_0 == 5)
		{
			goto IL_004a;
		}
		if (L_0 == 6)
		{
			goto IL_0051;
		}
	}
	{
		goto IL_0058;
	}

IL_0027:
	{
		float L_1 = ColorPicker_get_R_m3995379697(__this, /*hidden argument*/NULL);
		return L_1;
	}

IL_002e:
	{
		float L_2 = ColorPicker_get_G_m755548720(__this, /*hidden argument*/NULL);
		return L_2;
	}

IL_0035:
	{
		float L_3 = ColorPicker_get_B_m1736779681(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_003c:
	{
		float L_4 = ColorPicker_get_A_m473223718(__this, /*hidden argument*/NULL);
		return L_4;
	}

IL_0043:
	{
		float L_5 = ColorPicker_get_H_m3860865411(__this, /*hidden argument*/NULL);
		return L_5;
	}

IL_004a:
	{
		float L_6 = ColorPicker_get_S_m2449498732(__this, /*hidden argument*/NULL);
		return L_6;
	}

IL_0051:
	{
		float L_7 = ColorPicker_get_V_m265062405(__this, /*hidden argument*/NULL);
		return L_7;
	}

IL_0058:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NotImplementedException_t2785117854 * L_9 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_9, L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}
}
// System.Void ColorPickerTester::.ctor()
extern "C"  void ColorPickerTester__ctor_m185493325 (ColorPickerTester_t1006114474 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorPickerTester::Start()
extern Il2CppClass* UnityAction_1_t3386977826_il2cpp_TypeInfo_var;
extern const MethodInfo* ColorPickerTester_U3CStartU3Em__0_m540941732_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3329809356_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m903508446_MethodInfo_var;
extern const uint32_t ColorPickerTester_Start_m2707422277_MetadataUsageId;
extern "C"  void ColorPickerTester_Start_m2707422277 (ColorPickerTester_t1006114474 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorPickerTester_Start_m2707422277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorPicker_t3035206225 * L_0 = __this->get_picker_3();
		NullCheck(L_0);
		ColorChangedEvent_t2990895397 * L_1 = L_0->get_onValueChanged_9();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ColorPickerTester_U3CStartU3Em__0_m540941732_MethodInfo_var);
		UnityAction_1_t3386977826 * L_3 = (UnityAction_1_t3386977826 *)il2cpp_codegen_object_new(UnityAction_1_t3386977826_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3329809356(L_3, __this, L_2, /*hidden argument*/UnityAction_1__ctor_m3329809356_MethodInfo_var);
		NullCheck(L_1);
		UnityEvent_1_AddListener_m903508446(L_1, L_3, /*hidden argument*/UnityEvent_1_AddListener_m903508446_MethodInfo_var);
		Renderer_t257310565 * L_4 = __this->get_renderer_2();
		NullCheck(L_4);
		Material_t193706927 * L_5 = Renderer_get_material_m2553789785(L_4, /*hidden argument*/NULL);
		ColorPicker_t3035206225 * L_6 = __this->get_picker_3();
		NullCheck(L_6);
		Color_t2020392075  L_7 = ColorPicker_get_CurrentColor_m3166096170(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_set_color_m577844242(L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorPickerTester::Update()
extern "C"  void ColorPickerTester_Update_m85289384 (ColorPickerTester_t1006114474 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ColorPickerTester::<Start>m__0(UnityEngine.Color)
extern "C"  void ColorPickerTester_U3CStartU3Em__0_m540941732 (ColorPickerTester_t1006114474 * __this, Color_t2020392075  ___color0, const MethodInfo* method)
{
	{
		Renderer_t257310565 * L_0 = __this->get_renderer_2();
		NullCheck(L_0);
		Material_t193706927 * L_1 = Renderer_get_material_m2553789785(L_0, /*hidden argument*/NULL);
		Color_t2020392075  L_2 = ___color0;
		NullCheck(L_1);
		Material_set_color_m577844242(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorPresets::.ctor()
extern "C"  void ColorPresets__ctor_m508027560 (ColorPresets_t4120623669 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorPresets::Awake()
extern Il2CppClass* UnityAction_1_t3386977826_il2cpp_TypeInfo_var;
extern const MethodInfo* ColorPresets_ColorChanged_m578093369_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3329809356_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m903508446_MethodInfo_var;
extern const uint32_t ColorPresets_Awake_m2912318885_MetadataUsageId;
extern "C"  void ColorPresets_Awake_m2912318885 (ColorPresets_t4120623669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorPresets_Awake_m2912318885_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorPicker_t3035206225 * L_0 = __this->get_picker_2();
		NullCheck(L_0);
		ColorChangedEvent_t2990895397 * L_1 = L_0->get_onValueChanged_9();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ColorPresets_ColorChanged_m578093369_MethodInfo_var);
		UnityAction_1_t3386977826 * L_3 = (UnityAction_1_t3386977826 *)il2cpp_codegen_object_new(UnityAction_1_t3386977826_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3329809356(L_3, __this, L_2, /*hidden argument*/UnityAction_1__ctor_m3329809356_MethodInfo_var);
		NullCheck(L_1);
		UnityEvent_1_AddListener_m903508446(L_1, L_3, /*hidden argument*/UnityEvent_1_AddListener_m903508446_MethodInfo_var);
		return;
	}
}
// System.Void ColorPresets::CreatePresetButton()
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern const uint32_t ColorPresets_CreatePresetButton_m19834235_MetadataUsageId;
extern "C"  void ColorPresets_CreatePresetButton_m19834235 (ColorPresets_t4120623669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorPresets_CreatePresetButton_m19834235_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_004d;
	}

IL_0007:
	{
		GameObjectU5BU5D_t3057952154* L_0 = __this->get_presets_3();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_t1756533147 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		bool L_4 = GameObject_get_activeSelf_m313590879(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0049;
		}
	}
	{
		GameObjectU5BU5D_t3057952154* L_5 = __this->get_presets_3();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		GameObject_t1756533147 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		GameObject_SetActive_m2887581199(L_8, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_9 = __this->get_presets_3();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		GameObject_t1756533147 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		Image_t2042527209 * L_13 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_12, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		ColorPicker_t3035206225 * L_14 = __this->get_picker_2();
		NullCheck(L_14);
		Color_t2020392075  L_15 = ColorPicker_get_CurrentColor_m3166096170(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_13, L_15);
		goto IL_005b;
	}

IL_0049:
	{
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_17 = V_0;
		GameObjectU5BU5D_t3057952154* L_18 = __this->get_presets_3();
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_0007;
		}
	}

IL_005b:
	{
		return;
	}
}
// System.Void ColorPresets::PresetSelect(UnityEngine.UI.Image)
extern "C"  void ColorPresets_PresetSelect_m1727594879 (ColorPresets_t4120623669 * __this, Image_t2042527209 * ___sender0, const MethodInfo* method)
{
	{
		ColorPicker_t3035206225 * L_0 = __this->get_picker_2();
		Image_t2042527209 * L_1 = ___sender0;
		NullCheck(L_1);
		Color_t2020392075  L_2 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_1);
		NullCheck(L_0);
		ColorPicker_set_CurrentColor_m2125228471(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorPresets::ColorChanged(UnityEngine.Color)
extern "C"  void ColorPresets_ColorChanged_m578093369 (ColorPresets_t4120623669 * __this, Color_t2020392075  ___color0, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_createPresetImage_4();
		Color_t2020392075  L_1 = ___color0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
// System.Void ColorSlider::.ctor()
extern "C"  void ColorSlider__ctor_m3582912827 (ColorSlider_t2729134766 * __this, const MethodInfo* method)
{
	{
		__this->set_listen_5((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorSlider::Awake()
extern Il2CppClass* UnityAction_1_t3386977826_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_3_t235051313_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_1_t3443095683_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisSlider_t297367283_m1462559763_MethodInfo_var;
extern const MethodInfo* ColorSlider_ColorChanged_m742523788_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3329809356_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m903508446_MethodInfo_var;
extern const MethodInfo* ColorSlider_HSVChanged_m3304896453_MethodInfo_var;
extern const MethodInfo* UnityAction_3__ctor_m3626891334_MethodInfo_var;
extern const MethodInfo* UnityEvent_3_AddListener_m3608966849_MethodInfo_var;
extern const MethodInfo* ColorSlider_SliderChanged_m1758874829_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m2172708761_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m2377847221_MethodInfo_var;
extern const uint32_t ColorSlider_Awake_m2035409562_MetadataUsageId;
extern "C"  void ColorSlider_Awake_m2035409562 (ColorSlider_t2729134766 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorSlider_Awake_m2035409562_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Slider_t297367283 * L_0 = Component_GetComponent_TisSlider_t297367283_m1462559763(__this, /*hidden argument*/Component_GetComponent_TisSlider_t297367283_m1462559763_MethodInfo_var);
		__this->set_slider_4(L_0);
		ColorPicker_t3035206225 * L_1 = __this->get_hsvpicker_2();
		NullCheck(L_1);
		ColorChangedEvent_t2990895397 * L_2 = L_1->get_onValueChanged_9();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)ColorSlider_ColorChanged_m742523788_MethodInfo_var);
		UnityAction_1_t3386977826 * L_4 = (UnityAction_1_t3386977826 *)il2cpp_codegen_object_new(UnityAction_1_t3386977826_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3329809356(L_4, __this, L_3, /*hidden argument*/UnityAction_1__ctor_m3329809356_MethodInfo_var);
		NullCheck(L_2);
		UnityEvent_1_AddListener_m903508446(L_2, L_4, /*hidden argument*/UnityEvent_1_AddListener_m903508446_MethodInfo_var);
		ColorPicker_t3035206225 * L_5 = __this->get_hsvpicker_2();
		NullCheck(L_5);
		HSVChangedEvent_t1170297569 * L_6 = L_5->get_onHSVChanged_10();
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)ColorSlider_HSVChanged_m3304896453_MethodInfo_var);
		UnityAction_3_t235051313 * L_8 = (UnityAction_3_t235051313 *)il2cpp_codegen_object_new(UnityAction_3_t235051313_il2cpp_TypeInfo_var);
		UnityAction_3__ctor_m3626891334(L_8, __this, L_7, /*hidden argument*/UnityAction_3__ctor_m3626891334_MethodInfo_var);
		NullCheck(L_6);
		UnityEvent_3_AddListener_m3608966849(L_6, L_8, /*hidden argument*/UnityEvent_3_AddListener_m3608966849_MethodInfo_var);
		Slider_t297367283 * L_9 = __this->get_slider_4();
		NullCheck(L_9);
		SliderEvent_t2111116400 * L_10 = Slider_get_onValueChanged_m4261003214(L_9, /*hidden argument*/NULL);
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)ColorSlider_SliderChanged_m1758874829_MethodInfo_var);
		UnityAction_1_t3443095683 * L_12 = (UnityAction_1_t3443095683 *)il2cpp_codegen_object_new(UnityAction_1_t3443095683_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m2172708761(L_12, __this, L_11, /*hidden argument*/UnityAction_1__ctor_m2172708761_MethodInfo_var);
		NullCheck(L_10);
		UnityEvent_1_AddListener_m2377847221(L_10, L_12, /*hidden argument*/UnityEvent_1_AddListener_m2377847221_MethodInfo_var);
		return;
	}
}
// System.Void ColorSlider::OnDestroy()
extern Il2CppClass* UnityAction_1_t3386977826_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_3_t235051313_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_1_t3443095683_il2cpp_TypeInfo_var;
extern const MethodInfo* ColorSlider_ColorChanged_m742523788_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3329809356_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_RemoveListener_m1138414664_MethodInfo_var;
extern const MethodInfo* ColorSlider_HSVChanged_m3304896453_MethodInfo_var;
extern const MethodInfo* UnityAction_3__ctor_m3626891334_MethodInfo_var;
extern const MethodInfo* UnityEvent_3_RemoveListener_m2407167912_MethodInfo_var;
extern const MethodInfo* ColorSlider_SliderChanged_m1758874829_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m2172708761_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_RemoveListener_m2564825698_MethodInfo_var;
extern const uint32_t ColorSlider_OnDestroy_m1119776176_MetadataUsageId;
extern "C"  void ColorSlider_OnDestroy_m1119776176 (ColorSlider_t2729134766 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorSlider_OnDestroy_m1119776176_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorPicker_t3035206225 * L_0 = __this->get_hsvpicker_2();
		NullCheck(L_0);
		ColorChangedEvent_t2990895397 * L_1 = L_0->get_onValueChanged_9();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ColorSlider_ColorChanged_m742523788_MethodInfo_var);
		UnityAction_1_t3386977826 * L_3 = (UnityAction_1_t3386977826 *)il2cpp_codegen_object_new(UnityAction_1_t3386977826_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3329809356(L_3, __this, L_2, /*hidden argument*/UnityAction_1__ctor_m3329809356_MethodInfo_var);
		NullCheck(L_1);
		UnityEvent_1_RemoveListener_m1138414664(L_1, L_3, /*hidden argument*/UnityEvent_1_RemoveListener_m1138414664_MethodInfo_var);
		ColorPicker_t3035206225 * L_4 = __this->get_hsvpicker_2();
		NullCheck(L_4);
		HSVChangedEvent_t1170297569 * L_5 = L_4->get_onHSVChanged_10();
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)ColorSlider_HSVChanged_m3304896453_MethodInfo_var);
		UnityAction_3_t235051313 * L_7 = (UnityAction_3_t235051313 *)il2cpp_codegen_object_new(UnityAction_3_t235051313_il2cpp_TypeInfo_var);
		UnityAction_3__ctor_m3626891334(L_7, __this, L_6, /*hidden argument*/UnityAction_3__ctor_m3626891334_MethodInfo_var);
		NullCheck(L_5);
		UnityEvent_3_RemoveListener_m2407167912(L_5, L_7, /*hidden argument*/UnityEvent_3_RemoveListener_m2407167912_MethodInfo_var);
		Slider_t297367283 * L_8 = __this->get_slider_4();
		NullCheck(L_8);
		SliderEvent_t2111116400 * L_9 = Slider_get_onValueChanged_m4261003214(L_8, /*hidden argument*/NULL);
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)ColorSlider_SliderChanged_m1758874829_MethodInfo_var);
		UnityAction_1_t3443095683 * L_11 = (UnityAction_1_t3443095683 *)il2cpp_codegen_object_new(UnityAction_1_t3443095683_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m2172708761(L_11, __this, L_10, /*hidden argument*/UnityAction_1__ctor_m2172708761_MethodInfo_var);
		NullCheck(L_9);
		UnityEvent_1_RemoveListener_m2564825698(L_9, L_11, /*hidden argument*/UnityEvent_1_RemoveListener_m2564825698_MethodInfo_var);
		return;
	}
}
// System.Void ColorSlider::ColorChanged(UnityEngine.Color)
extern "C"  void ColorSlider_ColorChanged_m742523788 (ColorSlider_t2729134766 * __this, Color_t2020392075  ___newColor0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		__this->set_listen_5((bool)0);
		int32_t L_0 = __this->get_type_3();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0029;
		}
		if (L_1 == 1)
		{
			goto IL_0040;
		}
		if (L_1 == 2)
		{
			goto IL_0057;
		}
		if (L_1 == 3)
		{
			goto IL_006e;
		}
	}
	{
		goto IL_0085;
	}

IL_0029:
	{
		Slider_t297367283 * L_2 = __this->get_slider_4();
		float L_3 = (&___newColor0)->get_r_0();
		NullCheck(L_2);
		Slider_set_normalizedValue_m3093868078(L_2, L_3, /*hidden argument*/NULL);
		goto IL_008a;
	}

IL_0040:
	{
		Slider_t297367283 * L_4 = __this->get_slider_4();
		float L_5 = (&___newColor0)->get_g_1();
		NullCheck(L_4);
		Slider_set_normalizedValue_m3093868078(L_4, L_5, /*hidden argument*/NULL);
		goto IL_008a;
	}

IL_0057:
	{
		Slider_t297367283 * L_6 = __this->get_slider_4();
		float L_7 = (&___newColor0)->get_b_2();
		NullCheck(L_6);
		Slider_set_normalizedValue_m3093868078(L_6, L_7, /*hidden argument*/NULL);
		goto IL_008a;
	}

IL_006e:
	{
		Slider_t297367283 * L_8 = __this->get_slider_4();
		float L_9 = (&___newColor0)->get_a_3();
		NullCheck(L_8);
		Slider_set_normalizedValue_m3093868078(L_8, L_9, /*hidden argument*/NULL);
		goto IL_008a;
	}

IL_0085:
	{
		goto IL_008a;
	}

IL_008a:
	{
		return;
	}
}
// System.Void ColorSlider::HSVChanged(System.Single,System.Single,System.Single)
extern "C"  void ColorSlider_HSVChanged_m3304896453 (ColorSlider_t2729134766 * __this, float ___hue0, float ___saturation1, float ___value2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		__this->set_listen_5((bool)0);
		int32_t L_0 = __this->get_type_3();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)4)) == 0)
		{
			goto IL_0027;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)4)) == 1)
		{
			goto IL_0038;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)4)) == 2)
		{
			goto IL_0049;
		}
	}
	{
		goto IL_005a;
	}

IL_0027:
	{
		Slider_t297367283 * L_2 = __this->get_slider_4();
		float L_3 = ___hue0;
		NullCheck(L_2);
		Slider_set_normalizedValue_m3093868078(L_2, L_3, /*hidden argument*/NULL);
		goto IL_005f;
	}

IL_0038:
	{
		Slider_t297367283 * L_4 = __this->get_slider_4();
		float L_5 = ___saturation1;
		NullCheck(L_4);
		Slider_set_normalizedValue_m3093868078(L_4, L_5, /*hidden argument*/NULL);
		goto IL_005f;
	}

IL_0049:
	{
		Slider_t297367283 * L_6 = __this->get_slider_4();
		float L_7 = ___value2;
		NullCheck(L_6);
		Slider_set_normalizedValue_m3093868078(L_6, L_7, /*hidden argument*/NULL);
		goto IL_005f;
	}

IL_005a:
	{
		goto IL_005f;
	}

IL_005f:
	{
		return;
	}
}
// System.Void ColorSlider::SliderChanged(System.Single)
extern "C"  void ColorSlider_SliderChanged_m1758874829 (ColorSlider_t2729134766 * __this, float ___newValue0, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_listen_5();
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		Slider_t297367283 * L_1 = __this->get_slider_4();
		NullCheck(L_1);
		float L_2 = Slider_get_normalizedValue_m4164062921(L_1, /*hidden argument*/NULL);
		___newValue0 = L_2;
		ColorPicker_t3035206225 * L_3 = __this->get_hsvpicker_2();
		int32_t L_4 = __this->get_type_3();
		float L_5 = ___newValue0;
		NullCheck(L_3);
		ColorPicker_AssignColor_m579716850(L_3, L_4, L_5, /*hidden argument*/NULL);
	}

IL_002a:
	{
		__this->set_listen_5((bool)1);
		return;
	}
}
// System.Void ColorSliderImage::.ctor()
extern "C"  void ColorSliderImage__ctor_m1463450338 (ColorSliderImage_t376502149 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RectTransform ColorSliderImage::get_rectTransform()
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern const uint32_t ColorSliderImage_get_rectTransform_m3308471489_MetadataUsageId;
extern "C"  RectTransform_t3349966182 * ColorSliderImage_get_rectTransform_m3308471489 (ColorSliderImage_t376502149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorSliderImage_get_rectTransform_m3308471489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		return ((RectTransform_t3349966182 *)IsInstSealed(L_0, RectTransform_t3349966182_il2cpp_TypeInfo_var));
	}
}
// System.Void ColorSliderImage::Awake()
extern const MethodInfo* Component_GetComponent_TisRawImage_t2749640213_m1817787565_MethodInfo_var;
extern const uint32_t ColorSliderImage_Awake_m834120629_MetadataUsageId;
extern "C"  void ColorSliderImage_Awake_m834120629 (ColorSliderImage_t376502149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorSliderImage_Awake_m834120629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RawImage_t2749640213 * L_0 = Component_GetComponent_TisRawImage_t2749640213_m1817787565(__this, /*hidden argument*/Component_GetComponent_TisRawImage_t2749640213_m1817787565_MethodInfo_var);
		__this->set_image_5(L_0);
		ColorSliderImage_RegenerateTexture_m1461909699(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorSliderImage::OnEnable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_1_t3386977826_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_3_t235051313_il2cpp_TypeInfo_var;
extern const MethodInfo* ColorSliderImage_ColorChanged_m3679980033_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3329809356_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m903508446_MethodInfo_var;
extern const MethodInfo* ColorSliderImage_HSVChanged_m1936863288_MethodInfo_var;
extern const MethodInfo* UnityAction_3__ctor_m3626891334_MethodInfo_var;
extern const MethodInfo* UnityEvent_3_AddListener_m3608966849_MethodInfo_var;
extern const uint32_t ColorSliderImage_OnEnable_m1531418098_MetadataUsageId;
extern "C"  void ColorSliderImage_OnEnable_m1531418098 (ColorSliderImage_t376502149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorSliderImage_OnEnable_m1531418098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorPicker_t3035206225 * L_0 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0053;
		}
	}
	{
		bool L_2 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0053;
		}
	}
	{
		ColorPicker_t3035206225 * L_3 = __this->get_picker_2();
		NullCheck(L_3);
		ColorChangedEvent_t2990895397 * L_4 = L_3->get_onValueChanged_9();
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)ColorSliderImage_ColorChanged_m3679980033_MethodInfo_var);
		UnityAction_1_t3386977826 * L_6 = (UnityAction_1_t3386977826 *)il2cpp_codegen_object_new(UnityAction_1_t3386977826_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3329809356(L_6, __this, L_5, /*hidden argument*/UnityAction_1__ctor_m3329809356_MethodInfo_var);
		NullCheck(L_4);
		UnityEvent_1_AddListener_m903508446(L_4, L_6, /*hidden argument*/UnityEvent_1_AddListener_m903508446_MethodInfo_var);
		ColorPicker_t3035206225 * L_7 = __this->get_picker_2();
		NullCheck(L_7);
		HSVChangedEvent_t1170297569 * L_8 = L_7->get_onHSVChanged_10();
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)ColorSliderImage_HSVChanged_m1936863288_MethodInfo_var);
		UnityAction_3_t235051313 * L_10 = (UnityAction_3_t235051313 *)il2cpp_codegen_object_new(UnityAction_3_t235051313_il2cpp_TypeInfo_var);
		UnityAction_3__ctor_m3626891334(L_10, __this, L_9, /*hidden argument*/UnityAction_3__ctor_m3626891334_MethodInfo_var);
		NullCheck(L_8);
		UnityEvent_3_AddListener_m3608966849(L_8, L_10, /*hidden argument*/UnityEvent_3_AddListener_m3608966849_MethodInfo_var);
	}

IL_0053:
	{
		return;
	}
}
// System.Void ColorSliderImage::OnDisable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_1_t3386977826_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_3_t235051313_il2cpp_TypeInfo_var;
extern const MethodInfo* ColorSliderImage_ColorChanged_m3679980033_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3329809356_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_RemoveListener_m1138414664_MethodInfo_var;
extern const MethodInfo* ColorSliderImage_HSVChanged_m1936863288_MethodInfo_var;
extern const MethodInfo* UnityAction_3__ctor_m3626891334_MethodInfo_var;
extern const MethodInfo* UnityEvent_3_RemoveListener_m2407167912_MethodInfo_var;
extern const uint32_t ColorSliderImage_OnDisable_m2944825581_MetadataUsageId;
extern "C"  void ColorSliderImage_OnDisable_m2944825581 (ColorSliderImage_t376502149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorSliderImage_OnDisable_m2944825581_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorPicker_t3035206225 * L_0 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0049;
		}
	}
	{
		ColorPicker_t3035206225 * L_2 = __this->get_picker_2();
		NullCheck(L_2);
		ColorChangedEvent_t2990895397 * L_3 = L_2->get_onValueChanged_9();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)ColorSliderImage_ColorChanged_m3679980033_MethodInfo_var);
		UnityAction_1_t3386977826 * L_5 = (UnityAction_1_t3386977826 *)il2cpp_codegen_object_new(UnityAction_1_t3386977826_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3329809356(L_5, __this, L_4, /*hidden argument*/UnityAction_1__ctor_m3329809356_MethodInfo_var);
		NullCheck(L_3);
		UnityEvent_1_RemoveListener_m1138414664(L_3, L_5, /*hidden argument*/UnityEvent_1_RemoveListener_m1138414664_MethodInfo_var);
		ColorPicker_t3035206225 * L_6 = __this->get_picker_2();
		NullCheck(L_6);
		HSVChangedEvent_t1170297569 * L_7 = L_6->get_onHSVChanged_10();
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)ColorSliderImage_HSVChanged_m1936863288_MethodInfo_var);
		UnityAction_3_t235051313 * L_9 = (UnityAction_3_t235051313 *)il2cpp_codegen_object_new(UnityAction_3_t235051313_il2cpp_TypeInfo_var);
		UnityAction_3__ctor_m3626891334(L_9, __this, L_8, /*hidden argument*/UnityAction_3__ctor_m3626891334_MethodInfo_var);
		NullCheck(L_7);
		UnityEvent_3_RemoveListener_m2407167912(L_7, L_9, /*hidden argument*/UnityEvent_3_RemoveListener_m2407167912_MethodInfo_var);
	}

IL_0049:
	{
		return;
	}
}
// System.Void ColorSliderImage::OnDestroy()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t ColorSliderImage_OnDestroy_m2738933995_MetadataUsageId;
extern "C"  void ColorSliderImage_OnDestroy_m2738933995 (ColorSliderImage_t376502149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorSliderImage_OnDestroy_m2738933995_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RawImage_t2749640213 * L_0 = __this->get_image_5();
		NullCheck(L_0);
		Texture_t2243626319 * L_1 = RawImage_get_texture_m2258734143(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		RawImage_t2749640213 * L_3 = __this->get_image_5();
		NullCheck(L_3);
		Texture_t2243626319 * L_4 = RawImage_get_texture_m2258734143(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void ColorSliderImage::ColorChanged(UnityEngine.Color)
extern "C"  void ColorSliderImage_ColorChanged_m3679980033 (ColorSliderImage_t376502149 * __this, Color_t2020392075  ___newColor0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_type_3();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002e;
		}
		if (L_1 == 1)
		{
			goto IL_002e;
		}
		if (L_1 == 2)
		{
			goto IL_002e;
		}
		if (L_1 == 3)
		{
			goto IL_0039;
		}
		if (L_1 == 4)
		{
			goto IL_0039;
		}
		if (L_1 == 5)
		{
			goto IL_002e;
		}
		if (L_1 == 6)
		{
			goto IL_002e;
		}
	}
	{
		goto IL_0039;
	}

IL_002e:
	{
		ColorSliderImage_RegenerateTexture_m1461909699(__this, /*hidden argument*/NULL);
		goto IL_003e;
	}

IL_0039:
	{
		goto IL_003e;
	}

IL_003e:
	{
		return;
	}
}
// System.Void ColorSliderImage::HSVChanged(System.Single,System.Single,System.Single)
extern "C"  void ColorSliderImage_HSVChanged_m1936863288 (ColorSliderImage_t376502149 * __this, float ___hue0, float ___saturation1, float ___value2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_type_3();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002e;
		}
		if (L_1 == 1)
		{
			goto IL_002e;
		}
		if (L_1 == 2)
		{
			goto IL_002e;
		}
		if (L_1 == 3)
		{
			goto IL_0039;
		}
		if (L_1 == 4)
		{
			goto IL_0039;
		}
		if (L_1 == 5)
		{
			goto IL_002e;
		}
		if (L_1 == 6)
		{
			goto IL_002e;
		}
	}
	{
		goto IL_0039;
	}

IL_002e:
	{
		ColorSliderImage_RegenerateTexture_m1461909699(__this, /*hidden argument*/NULL);
		goto IL_003e;
	}

IL_0039:
	{
		goto IL_003e;
	}

IL_003e:
	{
		return;
	}
}
// System.Void ColorSliderImage::RegenerateTexture()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppClass* Color32U5BU5D_t30278651_il2cpp_TypeInfo_var;
extern const uint32_t ColorSliderImage_RegenerateTexture_m1461909699_MetadataUsageId;
extern "C"  void ColorSliderImage_RegenerateTexture_m1461909699 (ColorSliderImage_t376502149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorSliderImage_RegenerateTexture_m1461909699_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color32_t874517518  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Texture2D_t3542995729 * V_4 = NULL;
	Color32U5BU5D_t30278651* V_5 = NULL;
	bool V_6 = false;
	bool V_7 = false;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	uint8_t V_11 = 0x0;
	uint8_t V_12 = 0x0;
	uint8_t V_13 = 0x0;
	uint8_t V_14 = 0x0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	Color_t2020392075  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	float G_B6_0 = 0.0f;
	float G_B9_0 = 0.0f;
	float G_B12_0 = 0.0f;
	int32_t G_B15_0 = 0;
	int32_t G_B18_0 = 0;
	Color32U5BU5D_t30278651* G_B32_0 = NULL;
	Color32U5BU5D_t30278651* G_B31_0 = NULL;
	int32_t G_B33_0 = 0;
	Color32U5BU5D_t30278651* G_B33_1 = NULL;
	Color32U5BU5D_t30278651* G_B39_0 = NULL;
	Color32U5BU5D_t30278651* G_B38_0 = NULL;
	int32_t G_B40_0 = 0;
	Color32U5BU5D_t30278651* G_B40_1 = NULL;
	Color32U5BU5D_t30278651* G_B46_0 = NULL;
	Color32U5BU5D_t30278651* G_B45_0 = NULL;
	int32_t G_B47_0 = 0;
	Color32U5BU5D_t30278651* G_B47_1 = NULL;
	Color32U5BU5D_t30278651* G_B53_0 = NULL;
	Color32U5BU5D_t30278651* G_B52_0 = NULL;
	int32_t G_B54_0 = 0;
	Color32U5BU5D_t30278651* G_B54_1 = NULL;
	Color32U5BU5D_t30278651* G_B60_0 = NULL;
	Color32U5BU5D_t30278651* G_B59_0 = NULL;
	int32_t G_B61_0 = 0;
	Color32U5BU5D_t30278651* G_B61_1 = NULL;
	Color32U5BU5D_t30278651* G_B67_0 = NULL;
	Color32U5BU5D_t30278651* G_B66_0 = NULL;
	int32_t G_B68_0 = 0;
	Color32U5BU5D_t30278651* G_B68_1 = NULL;
	Color32U5BU5D_t30278651* G_B74_0 = NULL;
	Color32U5BU5D_t30278651* G_B73_0 = NULL;
	int32_t G_B75_0 = 0;
	Color32U5BU5D_t30278651* G_B75_1 = NULL;
	{
		ColorPicker_t3035206225 * L_0 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		ColorPicker_t3035206225 * L_2 = __this->get_picker_2();
		NullCheck(L_2);
		Color_t2020392075  L_3 = ColorPicker_get_CurrentColor_m3166096170(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0026;
	}

IL_0021:
	{
		Color_t2020392075  L_4 = Color_get_black_m2650940523(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0026:
	{
		Color32_t874517518  L_5 = Color32_op_Implicit_m624191464(NULL /*static, unused*/, G_B3_0, /*hidden argument*/NULL);
		V_0 = L_5;
		ColorPicker_t3035206225 * L_6 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		ColorPicker_t3035206225 * L_8 = __this->get_picker_2();
		NullCheck(L_8);
		float L_9 = ColorPicker_get_H_m3860865411(L_8, /*hidden argument*/NULL);
		G_B6_0 = L_9;
		goto IL_0052;
	}

IL_004d:
	{
		G_B6_0 = (0.0f);
	}

IL_0052:
	{
		V_1 = G_B6_0;
		ColorPicker_t3035206225 * L_10 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_10, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0074;
		}
	}
	{
		ColorPicker_t3035206225 * L_12 = __this->get_picker_2();
		NullCheck(L_12);
		float L_13 = ColorPicker_get_S_m2449498732(L_12, /*hidden argument*/NULL);
		G_B9_0 = L_13;
		goto IL_0079;
	}

IL_0074:
	{
		G_B9_0 = (0.0f);
	}

IL_0079:
	{
		V_2 = G_B9_0;
		ColorPicker_t3035206225 * L_14 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_14, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_009b;
		}
	}
	{
		ColorPicker_t3035206225 * L_16 = __this->get_picker_2();
		NullCheck(L_16);
		float L_17 = ColorPicker_get_V_m265062405(L_16, /*hidden argument*/NULL);
		G_B12_0 = L_17;
		goto IL_00a0;
	}

IL_009b:
	{
		G_B12_0 = (0.0f);
	}

IL_00a0:
	{
		V_3 = G_B12_0;
		int32_t L_18 = __this->get_direction_4();
		if ((((int32_t)L_18) == ((int32_t)2)))
		{
			goto IL_00b8;
		}
	}
	{
		int32_t L_19 = __this->get_direction_4();
		G_B15_0 = ((((int32_t)L_19) == ((int32_t)3))? 1 : 0);
		goto IL_00b9;
	}

IL_00b8:
	{
		G_B15_0 = 1;
	}

IL_00b9:
	{
		V_6 = (bool)G_B15_0;
		int32_t L_20 = __this->get_direction_4();
		if ((((int32_t)L_20) == ((int32_t)3)))
		{
			goto IL_00d2;
		}
	}
	{
		int32_t L_21 = __this->get_direction_4();
		G_B18_0 = ((((int32_t)L_21) == ((int32_t)1))? 1 : 0);
		goto IL_00d3;
	}

IL_00d2:
	{
		G_B18_0 = 1;
	}

IL_00d3:
	{
		V_7 = (bool)G_B18_0;
		int32_t L_22 = __this->get_type_3();
		V_9 = L_22;
		int32_t L_23 = V_9;
		if (L_23 == 0)
		{
			goto IL_0105;
		}
		if (L_23 == 1)
		{
			goto IL_0105;
		}
		if (L_23 == 2)
		{
			goto IL_0105;
		}
		if (L_23 == 3)
		{
			goto IL_0105;
		}
		if (L_23 == 4)
		{
			goto IL_0111;
		}
		if (L_23 == 5)
		{
			goto IL_011d;
		}
		if (L_23 == 6)
		{
			goto IL_011d;
		}
	}
	{
		goto IL_0126;
	}

IL_0105:
	{
		V_8 = ((int32_t)255);
		goto IL_0131;
	}

IL_0111:
	{
		V_8 = ((int32_t)360);
		goto IL_0131;
	}

IL_011d:
	{
		V_8 = ((int32_t)100);
		goto IL_0131;
	}

IL_0126:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NotImplementedException_t2785117854 * L_25 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_25, L_24, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_25);
	}

IL_0131:
	{
		bool L_26 = V_6;
		if (!L_26)
		{
			goto IL_0147;
		}
	}
	{
		int32_t L_27 = V_8;
		Texture2D_t3542995729 * L_28 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m3598323350(L_28, 1, L_27, /*hidden argument*/NULL);
		V_4 = L_28;
		goto IL_0151;
	}

IL_0147:
	{
		int32_t L_29 = V_8;
		Texture2D_t3542995729 * L_30 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m3598323350(L_30, L_29, 1, /*hidden argument*/NULL);
		V_4 = L_30;
	}

IL_0151:
	{
		Texture2D_t3542995729 * L_31 = V_4;
		NullCheck(L_31);
		Object_set_hideFlags_m2204253440(L_31, ((int32_t)52), /*hidden argument*/NULL);
		int32_t L_32 = V_8;
		V_5 = ((Color32U5BU5D_t30278651*)SZArrayNew(Color32U5BU5D_t30278651_il2cpp_TypeInfo_var, (uint32_t)L_32));
		int32_t L_33 = __this->get_type_3();
		V_10 = L_33;
		int32_t L_34 = V_10;
		if (L_34 == 0)
		{
			goto IL_0193;
		}
		if (L_34 == 1)
		{
			goto IL_01eb;
		}
		if (L_34 == 2)
		{
			goto IL_0243;
		}
		if (L_34 == 3)
		{
			goto IL_029b;
		}
		if (L_34 == 4)
		{
			goto IL_02e9;
		}
		if (L_34 == 5)
		{
			goto IL_034a;
		}
		if (L_34 == 6)
		{
			goto IL_03a8;
		}
	}
	{
		goto IL_0406;
	}

IL_0193:
	{
		V_11 = 0;
		goto IL_01dd;
	}

IL_019b:
	{
		Color32U5BU5D_t30278651* L_35 = V_5;
		bool L_36 = V_7;
		G_B31_0 = L_35;
		if (!L_36)
		{
			G_B32_0 = L_35;
			goto IL_01b0;
		}
	}
	{
		int32_t L_37 = V_8;
		uint8_t L_38 = V_11;
		G_B33_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_37-(int32_t)1))-(int32_t)L_38));
		G_B33_1 = G_B31_0;
		goto IL_01b2;
	}

IL_01b0:
	{
		uint8_t L_39 = V_11;
		G_B33_0 = ((int32_t)(L_39));
		G_B33_1 = G_B32_0;
	}

IL_01b2:
	{
		NullCheck(G_B33_1);
		uint8_t L_40 = V_11;
		uint8_t L_41 = (&V_0)->get_g_1();
		uint8_t L_42 = (&V_0)->get_b_2();
		Color32_t874517518  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Color32__ctor_m1932627809(&L_43, L_40, L_41, L_42, ((int32_t)255), /*hidden argument*/NULL);
		(*(Color32_t874517518 *)((G_B33_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(G_B33_0)))) = L_43;
		uint8_t L_44 = V_11;
		V_11 = (((int32_t)((uint8_t)((int32_t)((int32_t)L_44+(int32_t)1)))));
	}

IL_01dd:
	{
		uint8_t L_45 = V_11;
		int32_t L_46 = V_8;
		if ((((int32_t)L_45) < ((int32_t)L_46)))
		{
			goto IL_019b;
		}
	}
	{
		goto IL_0411;
	}

IL_01eb:
	{
		V_12 = 0;
		goto IL_0235;
	}

IL_01f3:
	{
		Color32U5BU5D_t30278651* L_47 = V_5;
		bool L_48 = V_7;
		G_B38_0 = L_47;
		if (!L_48)
		{
			G_B39_0 = L_47;
			goto IL_0208;
		}
	}
	{
		int32_t L_49 = V_8;
		uint8_t L_50 = V_12;
		G_B40_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_49-(int32_t)1))-(int32_t)L_50));
		G_B40_1 = G_B38_0;
		goto IL_020a;
	}

IL_0208:
	{
		uint8_t L_51 = V_12;
		G_B40_0 = ((int32_t)(L_51));
		G_B40_1 = G_B39_0;
	}

IL_020a:
	{
		NullCheck(G_B40_1);
		uint8_t L_52 = (&V_0)->get_r_0();
		uint8_t L_53 = V_12;
		uint8_t L_54 = (&V_0)->get_b_2();
		Color32_t874517518  L_55;
		memset(&L_55, 0, sizeof(L_55));
		Color32__ctor_m1932627809(&L_55, L_52, L_53, L_54, ((int32_t)255), /*hidden argument*/NULL);
		(*(Color32_t874517518 *)((G_B40_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(G_B40_0)))) = L_55;
		uint8_t L_56 = V_12;
		V_12 = (((int32_t)((uint8_t)((int32_t)((int32_t)L_56+(int32_t)1)))));
	}

IL_0235:
	{
		uint8_t L_57 = V_12;
		int32_t L_58 = V_8;
		if ((((int32_t)L_57) < ((int32_t)L_58)))
		{
			goto IL_01f3;
		}
	}
	{
		goto IL_0411;
	}

IL_0243:
	{
		V_13 = 0;
		goto IL_028d;
	}

IL_024b:
	{
		Color32U5BU5D_t30278651* L_59 = V_5;
		bool L_60 = V_7;
		G_B45_0 = L_59;
		if (!L_60)
		{
			G_B46_0 = L_59;
			goto IL_0260;
		}
	}
	{
		int32_t L_61 = V_8;
		uint8_t L_62 = V_13;
		G_B47_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_61-(int32_t)1))-(int32_t)L_62));
		G_B47_1 = G_B45_0;
		goto IL_0262;
	}

IL_0260:
	{
		uint8_t L_63 = V_13;
		G_B47_0 = ((int32_t)(L_63));
		G_B47_1 = G_B46_0;
	}

IL_0262:
	{
		NullCheck(G_B47_1);
		uint8_t L_64 = (&V_0)->get_r_0();
		uint8_t L_65 = (&V_0)->get_g_1();
		uint8_t L_66 = V_13;
		Color32_t874517518  L_67;
		memset(&L_67, 0, sizeof(L_67));
		Color32__ctor_m1932627809(&L_67, L_64, L_65, L_66, ((int32_t)255), /*hidden argument*/NULL);
		(*(Color32_t874517518 *)((G_B47_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(G_B47_0)))) = L_67;
		uint8_t L_68 = V_13;
		V_13 = (((int32_t)((uint8_t)((int32_t)((int32_t)L_68+(int32_t)1)))));
	}

IL_028d:
	{
		uint8_t L_69 = V_13;
		int32_t L_70 = V_8;
		if ((((int32_t)L_69) < ((int32_t)L_70)))
		{
			goto IL_024b;
		}
	}
	{
		goto IL_0411;
	}

IL_029b:
	{
		V_14 = 0;
		goto IL_02db;
	}

IL_02a3:
	{
		Color32U5BU5D_t30278651* L_71 = V_5;
		bool L_72 = V_7;
		G_B52_0 = L_71;
		if (!L_72)
		{
			G_B53_0 = L_71;
			goto IL_02b8;
		}
	}
	{
		int32_t L_73 = V_8;
		uint8_t L_74 = V_14;
		G_B54_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_73-(int32_t)1))-(int32_t)L_74));
		G_B54_1 = G_B52_0;
		goto IL_02ba;
	}

IL_02b8:
	{
		uint8_t L_75 = V_14;
		G_B54_0 = ((int32_t)(L_75));
		G_B54_1 = G_B53_0;
	}

IL_02ba:
	{
		NullCheck(G_B54_1);
		uint8_t L_76 = V_14;
		uint8_t L_77 = V_14;
		uint8_t L_78 = V_14;
		Color32_t874517518  L_79;
		memset(&L_79, 0, sizeof(L_79));
		Color32__ctor_m1932627809(&L_79, L_76, L_77, L_78, ((int32_t)255), /*hidden argument*/NULL);
		(*(Color32_t874517518 *)((G_B54_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(G_B54_0)))) = L_79;
		uint8_t L_80 = V_14;
		V_14 = (((int32_t)((uint8_t)((int32_t)((int32_t)L_80+(int32_t)1)))));
	}

IL_02db:
	{
		uint8_t L_81 = V_14;
		int32_t L_82 = V_8;
		if ((((int32_t)L_81) < ((int32_t)L_82)))
		{
			goto IL_02a3;
		}
	}
	{
		goto IL_0411;
	}

IL_02e9:
	{
		V_15 = 0;
		goto IL_033c;
	}

IL_02f1:
	{
		Color32U5BU5D_t30278651* L_83 = V_5;
		bool L_84 = V_7;
		G_B59_0 = L_83;
		if (!L_84)
		{
			G_B60_0 = L_83;
			goto IL_0306;
		}
	}
	{
		int32_t L_85 = V_8;
		int32_t L_86 = V_15;
		G_B61_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_85-(int32_t)1))-(int32_t)L_86));
		G_B61_1 = G_B59_0;
		goto IL_0308;
	}

IL_0306:
	{
		int32_t L_87 = V_15;
		G_B61_0 = L_87;
		G_B61_1 = G_B60_0;
	}

IL_0308:
	{
		NullCheck(G_B61_1);
		int32_t L_88 = V_15;
		Color_t2020392075  L_89 = HSVUtil_ConvertHsvToRgb_m2339284600(NULL /*static, unused*/, (((double)((double)L_88))), (1.0), (1.0), (1.0f), /*hidden argument*/NULL);
		Color32_t874517518  L_90 = Color32_op_Implicit_m624191464(NULL /*static, unused*/, L_89, /*hidden argument*/NULL);
		(*(Color32_t874517518 *)((G_B61_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(G_B61_0)))) = L_90;
		int32_t L_91 = V_15;
		V_15 = ((int32_t)((int32_t)L_91+(int32_t)1));
	}

IL_033c:
	{
		int32_t L_92 = V_15;
		int32_t L_93 = V_8;
		if ((((int32_t)L_92) < ((int32_t)L_93)))
		{
			goto IL_02f1;
		}
	}
	{
		goto IL_0411;
	}

IL_034a:
	{
		V_16 = 0;
		goto IL_039a;
	}

IL_0352:
	{
		Color32U5BU5D_t30278651* L_94 = V_5;
		bool L_95 = V_7;
		G_B66_0 = L_94;
		if (!L_95)
		{
			G_B67_0 = L_94;
			goto IL_0367;
		}
	}
	{
		int32_t L_96 = V_8;
		int32_t L_97 = V_16;
		G_B68_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_96-(int32_t)1))-(int32_t)L_97));
		G_B68_1 = G_B66_0;
		goto IL_0369;
	}

IL_0367:
	{
		int32_t L_98 = V_16;
		G_B68_0 = L_98;
		G_B68_1 = G_B67_0;
	}

IL_0369:
	{
		NullCheck(G_B68_1);
		float L_99 = V_1;
		int32_t L_100 = V_16;
		int32_t L_101 = V_8;
		float L_102 = V_3;
		Color_t2020392075  L_103 = HSVUtil_ConvertHsvToRgb_m2339284600(NULL /*static, unused*/, (((double)((double)((float)((float)L_99*(float)(360.0f)))))), (((double)((double)((float)((float)(((float)((float)L_100)))/(float)(((float)((float)L_101)))))))), (((double)((double)L_102))), (1.0f), /*hidden argument*/NULL);
		Color32_t874517518  L_104 = Color32_op_Implicit_m624191464(NULL /*static, unused*/, L_103, /*hidden argument*/NULL);
		(*(Color32_t874517518 *)((G_B68_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(G_B68_0)))) = L_104;
		int32_t L_105 = V_16;
		V_16 = ((int32_t)((int32_t)L_105+(int32_t)1));
	}

IL_039a:
	{
		int32_t L_106 = V_16;
		int32_t L_107 = V_8;
		if ((((int32_t)L_106) < ((int32_t)L_107)))
		{
			goto IL_0352;
		}
	}
	{
		goto IL_0411;
	}

IL_03a8:
	{
		V_17 = 0;
		goto IL_03f8;
	}

IL_03b0:
	{
		Color32U5BU5D_t30278651* L_108 = V_5;
		bool L_109 = V_7;
		G_B73_0 = L_108;
		if (!L_109)
		{
			G_B74_0 = L_108;
			goto IL_03c5;
		}
	}
	{
		int32_t L_110 = V_8;
		int32_t L_111 = V_17;
		G_B75_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_110-(int32_t)1))-(int32_t)L_111));
		G_B75_1 = G_B73_0;
		goto IL_03c7;
	}

IL_03c5:
	{
		int32_t L_112 = V_17;
		G_B75_0 = L_112;
		G_B75_1 = G_B74_0;
	}

IL_03c7:
	{
		NullCheck(G_B75_1);
		float L_113 = V_1;
		float L_114 = V_2;
		int32_t L_115 = V_17;
		int32_t L_116 = V_8;
		Color_t2020392075  L_117 = HSVUtil_ConvertHsvToRgb_m2339284600(NULL /*static, unused*/, (((double)((double)((float)((float)L_113*(float)(360.0f)))))), (((double)((double)L_114))), (((double)((double)((float)((float)(((float)((float)L_115)))/(float)(((float)((float)L_116)))))))), (1.0f), /*hidden argument*/NULL);
		Color32_t874517518  L_118 = Color32_op_Implicit_m624191464(NULL /*static, unused*/, L_117, /*hidden argument*/NULL);
		(*(Color32_t874517518 *)((G_B75_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(G_B75_0)))) = L_118;
		int32_t L_119 = V_17;
		V_17 = ((int32_t)((int32_t)L_119+(int32_t)1));
	}

IL_03f8:
	{
		int32_t L_120 = V_17;
		int32_t L_121 = V_8;
		if ((((int32_t)L_120) < ((int32_t)L_121)))
		{
			goto IL_03b0;
		}
	}
	{
		goto IL_0411;
	}

IL_0406:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_122 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NotImplementedException_t2785117854 * L_123 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_123, L_122, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_123);
	}

IL_0411:
	{
		Texture2D_t3542995729 * L_124 = V_4;
		Color32U5BU5D_t30278651* L_125 = V_5;
		NullCheck(L_124);
		Texture2D_SetPixels32_m2480505405(L_124, L_125, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_126 = V_4;
		NullCheck(L_126);
		Texture2D_Apply_m3543341930(L_126, /*hidden argument*/NULL);
		RawImage_t2749640213 * L_127 = __this->get_image_5();
		NullCheck(L_127);
		Texture_t2243626319 * L_128 = RawImage_get_texture_m2258734143(L_127, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_129 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_128, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_129)
		{
			goto IL_0447;
		}
	}
	{
		RawImage_t2749640213 * L_130 = __this->get_image_5();
		NullCheck(L_130);
		Texture_t2243626319 * L_131 = RawImage_get_texture_m2258734143(L_130, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_131, /*hidden argument*/NULL);
	}

IL_0447:
	{
		RawImage_t2749640213 * L_132 = __this->get_image_5();
		Texture2D_t3542995729 * L_133 = V_4;
		NullCheck(L_132);
		RawImage_set_texture_m2400157626(L_132, L_133, /*hidden argument*/NULL);
		int32_t L_134 = __this->get_direction_4();
		V_18 = L_134;
		int32_t L_135 = V_18;
		if (L_135 == 0)
		{
			goto IL_04a1;
		}
		if (L_135 == 1)
		{
			goto IL_04a1;
		}
		if (L_135 == 2)
		{
			goto IL_0478;
		}
		if (L_135 == 3)
		{
			goto IL_0478;
		}
	}
	{
		goto IL_04ca;
	}

IL_0478:
	{
		RawImage_t2749640213 * L_136 = __this->get_image_5();
		Rect_t3681755626  L_137;
		memset(&L_137, 0, sizeof(L_137));
		Rect__ctor_m1220545469(&L_137, (0.0f), (0.0f), (2.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_136);
		RawImage_set_uvRect_m3807597783(L_136, L_137, /*hidden argument*/NULL);
		goto IL_04cf;
	}

IL_04a1:
	{
		RawImage_t2749640213 * L_138 = __this->get_image_5();
		Rect_t3681755626  L_139;
		memset(&L_139, 0, sizeof(L_139));
		Rect__ctor_m1220545469(&L_139, (0.0f), (0.0f), (1.0f), (2.0f), /*hidden argument*/NULL);
		NullCheck(L_138);
		RawImage_set_uvRect_m3807597783(L_138, L_139, /*hidden argument*/NULL);
		goto IL_04cf;
	}

IL_04ca:
	{
		goto IL_04cf;
	}

IL_04cf:
	{
		return;
	}
}
// System.Void HexColorField::.ctor()
extern "C"  void HexColorField__ctor_m2851903553 (HexColorField_t4192118964 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HexColorField::Awake()
extern Il2CppClass* UnityAction_1_t3395805984_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_1_t3386977826_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisInputField_t1631627530_m1177654614_MethodInfo_var;
extern const MethodInfo* HexColorField_UpdateColor_m4189307031_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m2212746417_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m2046140989_MethodInfo_var;
extern const MethodInfo* HexColorField_UpdateHex_m1796725051_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3329809356_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m903508446_MethodInfo_var;
extern const uint32_t HexColorField_Awake_m306535424_MetadataUsageId;
extern "C"  void HexColorField_Awake_m306535424 (HexColorField_t4192118964 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HexColorField_Awake_m306535424_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InputField_t1631627530 * L_0 = Component_GetComponent_TisInputField_t1631627530_m1177654614(__this, /*hidden argument*/Component_GetComponent_TisInputField_t1631627530_m1177654614_MethodInfo_var);
		__this->set_hexInputField_4(L_0);
		InputField_t1631627530 * L_1 = __this->get_hexInputField_4();
		NullCheck(L_1);
		SubmitEvent_t907918422 * L_2 = InputField_get_onEndEdit_m1618380883(L_1, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)HexColorField_UpdateColor_m4189307031_MethodInfo_var);
		UnityAction_1_t3395805984 * L_4 = (UnityAction_1_t3395805984 *)il2cpp_codegen_object_new(UnityAction_1_t3395805984_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m2212746417(L_4, __this, L_3, /*hidden argument*/UnityAction_1__ctor_m2212746417_MethodInfo_var);
		NullCheck(L_2);
		UnityEvent_1_AddListener_m2046140989(L_2, L_4, /*hidden argument*/UnityEvent_1_AddListener_m2046140989_MethodInfo_var);
		ColorPicker_t3035206225 * L_5 = __this->get_hsvpicker_2();
		NullCheck(L_5);
		ColorChangedEvent_t2990895397 * L_6 = L_5->get_onValueChanged_9();
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)HexColorField_UpdateHex_m1796725051_MethodInfo_var);
		UnityAction_1_t3386977826 * L_8 = (UnityAction_1_t3386977826 *)il2cpp_codegen_object_new(UnityAction_1_t3386977826_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3329809356(L_8, __this, L_7, /*hidden argument*/UnityAction_1__ctor_m3329809356_MethodInfo_var);
		NullCheck(L_6);
		UnityEvent_1_AddListener_m903508446(L_6, L_8, /*hidden argument*/UnityEvent_1_AddListener_m903508446_MethodInfo_var);
		return;
	}
}
// System.Void HexColorField::OnDestroy()
extern Il2CppClass* UnityAction_1_t3395805984_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_1_t3386977826_il2cpp_TypeInfo_var;
extern const MethodInfo* HexColorField_UpdateColor_m4189307031_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m2212746417_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_RemoveListener_m2236604102_MethodInfo_var;
extern const MethodInfo* HexColorField_UpdateHex_m1796725051_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3329809356_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_RemoveListener_m1138414664_MethodInfo_var;
extern const uint32_t HexColorField_OnDestroy_m4113204650_MetadataUsageId;
extern "C"  void HexColorField_OnDestroy_m4113204650 (HexColorField_t4192118964 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HexColorField_OnDestroy_m4113204650_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InputField_t1631627530 * L_0 = __this->get_hexInputField_4();
		NullCheck(L_0);
		OnChangeEvent_t2863344003 * L_1 = InputField_get_onValueChanged_m2097858642(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)HexColorField_UpdateColor_m4189307031_MethodInfo_var);
		UnityAction_1_t3395805984 * L_3 = (UnityAction_1_t3395805984 *)il2cpp_codegen_object_new(UnityAction_1_t3395805984_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m2212746417(L_3, __this, L_2, /*hidden argument*/UnityAction_1__ctor_m2212746417_MethodInfo_var);
		NullCheck(L_1);
		UnityEvent_1_RemoveListener_m2236604102(L_1, L_3, /*hidden argument*/UnityEvent_1_RemoveListener_m2236604102_MethodInfo_var);
		ColorPicker_t3035206225 * L_4 = __this->get_hsvpicker_2();
		NullCheck(L_4);
		ColorChangedEvent_t2990895397 * L_5 = L_4->get_onValueChanged_9();
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)HexColorField_UpdateHex_m1796725051_MethodInfo_var);
		UnityAction_1_t3386977826 * L_7 = (UnityAction_1_t3386977826 *)il2cpp_codegen_object_new(UnityAction_1_t3386977826_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3329809356(L_7, __this, L_6, /*hidden argument*/UnityAction_1__ctor_m3329809356_MethodInfo_var);
		NullCheck(L_5);
		UnityEvent_1_RemoveListener_m1138414664(L_5, L_7, /*hidden argument*/UnityEvent_1_RemoveListener_m1138414664_MethodInfo_var);
		return;
	}
}
// System.Void HexColorField::UpdateHex(UnityEngine.Color)
extern "C"  void HexColorField_UpdateHex_m1796725051 (HexColorField_t4192118964 * __this, Color_t2020392075  ___newColor0, const MethodInfo* method)
{
	{
		InputField_t1631627530 * L_0 = __this->get_hexInputField_4();
		Color_t2020392075  L_1 = ___newColor0;
		Color32_t874517518  L_2 = Color32_op_Implicit_m624191464(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_3 = HexColorField_ColorToHex_m410116926(__this, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		InputField_set_text_m114077119(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HexColorField::UpdateColor(System.String)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3567086134;
extern const uint32_t HexColorField_UpdateColor_m4189307031_MetadataUsageId;
extern "C"  void HexColorField_UpdateColor_m4189307031 (HexColorField_t4192118964 * __this, String_t* ___newHex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HexColorField_UpdateColor_m4189307031_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color32_t874517518  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___newHex0;
		bool L_1 = HexColorField_HexToColor_m3970770477(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ColorPicker_t3035206225 * L_2 = __this->get_hsvpicker_2();
		Color32_t874517518  L_3 = V_0;
		Color_t2020392075  L_4 = Color32_op_Implicit_m889975790(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		ColorPicker_set_CurrentColor_m2125228471(L_2, L_4, /*hidden argument*/NULL);
		goto IL_002d;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3567086134, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.String HexColorField::ColorToHex(UnityEngine.Color32)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3554759231;
extern Il2CppCodeGenString* _stringLiteral2608826782;
extern const uint32_t HexColorField_ColorToHex_m410116926_MetadataUsageId;
extern "C"  String_t* HexColorField_ColorToHex_m410116926 (HexColorField_t4192118964 * __this, Color32_t874517518  ___color0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HexColorField_ColorToHex_m410116926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_displayAlpha_3();
		if (!L_0)
		{
			goto IL_0058;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_1 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		uint8_t L_2 = (&___color0)->get_r_0();
		uint8_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_1;
		uint8_t L_6 = (&___color0)->get_g_1();
		uint8_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_8);
		ObjectU5BU5D_t3614634134* L_9 = L_5;
		uint8_t L_10 = (&___color0)->get_b_2();
		uint8_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = L_9;
		uint8_t L_14 = (&___color0)->get_a_3();
		uint8_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral3554759231, L_13, /*hidden argument*/NULL);
		return L_17;
	}

IL_0058:
	{
		uint8_t L_18 = (&___color0)->get_r_0();
		uint8_t L_19 = L_18;
		Il2CppObject * L_20 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_19);
		uint8_t L_21 = (&___color0)->get_g_1();
		uint8_t L_22 = L_21;
		Il2CppObject * L_23 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_22);
		uint8_t L_24 = (&___color0)->get_b_2();
		uint8_t L_25 = L_24;
		Il2CppObject * L_26 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_25);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral2608826782, L_20, L_23, L_26, /*hidden argument*/NULL);
		return L_27;
	}
}
// System.Boolean HexColorField::HexToColor(System.String,UnityEngine.Color32&)
extern Il2CppClass* Regex_t1803876613_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppClass* Color32_t874517518_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4264548006;
extern Il2CppCodeGenString* _stringLiteral372029311;
extern const uint32_t HexColorField_HexToColor_m3970770477_MetadataUsageId;
extern "C"  bool HexColorField_HexToColor_m3970770477 (Il2CppObject * __this /* static, unused */, String_t* ___hex0, Color32_t874517518 * ___color1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HexColorField_HexToColor_m3970770477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___hex0;
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t1803876613_il2cpp_TypeInfo_var);
		bool L_1 = Regex_IsMatch_m2218618503(NULL /*static, unused*/, L_0, _stringLiteral4264548006, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0255;
		}
	}
	{
		String_t* L_2 = ___hex0;
		NullCheck(L_2);
		bool L_3 = String_StartsWith_m1841920685(L_2, _stringLiteral372029311, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		G_B4_0 = 1;
		goto IL_0027;
	}

IL_0026:
	{
		G_B4_0 = 0;
	}

IL_0027:
	{
		V_0 = G_B4_0;
		String_t* L_4 = ___hex0;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m1606060069(L_4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)((int32_t)L_6+(int32_t)8))))))
		{
			goto IL_008f;
		}
	}
	{
		Color32_t874517518 * L_7 = ___color1;
		String_t* L_8 = ___hex0;
		int32_t L_9 = V_0;
		NullCheck(L_8);
		String_t* L_10 = String_Substring_m12482732(L_8, L_9, 2, /*hidden argument*/NULL);
		uint8_t L_11 = Byte_Parse_m4137294155(NULL /*static, unused*/, L_10, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_12 = ___hex0;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		String_t* L_14 = String_Substring_m12482732(L_12, ((int32_t)((int32_t)L_13+(int32_t)2)), 2, /*hidden argument*/NULL);
		uint8_t L_15 = Byte_Parse_m4137294155(NULL /*static, unused*/, L_14, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_16 = ___hex0;
		int32_t L_17 = V_0;
		NullCheck(L_16);
		String_t* L_18 = String_Substring_m12482732(L_16, ((int32_t)((int32_t)L_17+(int32_t)4)), 2, /*hidden argument*/NULL);
		uint8_t L_19 = Byte_Parse_m4137294155(NULL /*static, unused*/, L_18, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_20 = ___hex0;
		int32_t L_21 = V_0;
		NullCheck(L_20);
		String_t* L_22 = String_Substring_m12482732(L_20, ((int32_t)((int32_t)L_21+(int32_t)6)), 2, /*hidden argument*/NULL);
		uint8_t L_23 = Byte_Parse_m4137294155(NULL /*static, unused*/, L_22, ((int32_t)512), /*hidden argument*/NULL);
		Color32__ctor_m1932627809(L_7, L_11, L_15, L_19, L_23, /*hidden argument*/NULL);
		goto IL_0253;
	}

IL_008f:
	{
		String_t* L_24 = ___hex0;
		NullCheck(L_24);
		int32_t L_25 = String_get_Length_m1606060069(L_24, /*hidden argument*/NULL);
		int32_t L_26 = V_0;
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)((int32_t)L_26+(int32_t)6))))))
		{
			goto IL_00e7;
		}
	}
	{
		Color32_t874517518 * L_27 = ___color1;
		String_t* L_28 = ___hex0;
		int32_t L_29 = V_0;
		NullCheck(L_28);
		String_t* L_30 = String_Substring_m12482732(L_28, L_29, 2, /*hidden argument*/NULL);
		uint8_t L_31 = Byte_Parse_m4137294155(NULL /*static, unused*/, L_30, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_32 = ___hex0;
		int32_t L_33 = V_0;
		NullCheck(L_32);
		String_t* L_34 = String_Substring_m12482732(L_32, ((int32_t)((int32_t)L_33+(int32_t)2)), 2, /*hidden argument*/NULL);
		uint8_t L_35 = Byte_Parse_m4137294155(NULL /*static, unused*/, L_34, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_36 = ___hex0;
		int32_t L_37 = V_0;
		NullCheck(L_36);
		String_t* L_38 = String_Substring_m12482732(L_36, ((int32_t)((int32_t)L_37+(int32_t)4)), 2, /*hidden argument*/NULL);
		uint8_t L_39 = Byte_Parse_m4137294155(NULL /*static, unused*/, L_38, ((int32_t)512), /*hidden argument*/NULL);
		Color32__ctor_m1932627809(L_27, L_31, L_35, L_39, ((int32_t)255), /*hidden argument*/NULL);
		goto IL_0253;
	}

IL_00e7:
	{
		String_t* L_40 = ___hex0;
		NullCheck(L_40);
		int32_t L_41 = String_get_Length_m1606060069(L_40, /*hidden argument*/NULL);
		int32_t L_42 = V_0;
		if ((!(((uint32_t)L_41) == ((uint32_t)((int32_t)((int32_t)L_42+(int32_t)4))))))
		{
			goto IL_01bc;
		}
	}
	{
		Color32_t874517518 * L_43 = ___color1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_45 = ___hex0;
		int32_t L_46 = V_0;
		NullCheck(L_45);
		Il2CppChar L_47 = String_get_Chars_m4230566705(L_45, L_46, /*hidden argument*/NULL);
		Il2CppChar L_48 = L_47;
		Il2CppObject * L_49 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_48);
		String_t* L_50 = ___hex0;
		int32_t L_51 = V_0;
		NullCheck(L_50);
		Il2CppChar L_52 = String_get_Chars_m4230566705(L_50, L_51, /*hidden argument*/NULL);
		Il2CppChar L_53 = L_52;
		Il2CppObject * L_54 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_53);
		String_t* L_55 = String_Concat_m2000667605(NULL /*static, unused*/, L_44, L_49, L_54, /*hidden argument*/NULL);
		uint8_t L_56 = Byte_Parse_m4137294155(NULL /*static, unused*/, L_55, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_57 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_58 = ___hex0;
		int32_t L_59 = V_0;
		NullCheck(L_58);
		Il2CppChar L_60 = String_get_Chars_m4230566705(L_58, ((int32_t)((int32_t)L_59+(int32_t)1)), /*hidden argument*/NULL);
		Il2CppChar L_61 = L_60;
		Il2CppObject * L_62 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_61);
		String_t* L_63 = ___hex0;
		int32_t L_64 = V_0;
		NullCheck(L_63);
		Il2CppChar L_65 = String_get_Chars_m4230566705(L_63, ((int32_t)((int32_t)L_64+(int32_t)1)), /*hidden argument*/NULL);
		Il2CppChar L_66 = L_65;
		Il2CppObject * L_67 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_66);
		String_t* L_68 = String_Concat_m2000667605(NULL /*static, unused*/, L_57, L_62, L_67, /*hidden argument*/NULL);
		uint8_t L_69 = Byte_Parse_m4137294155(NULL /*static, unused*/, L_68, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_70 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_71 = ___hex0;
		int32_t L_72 = V_0;
		NullCheck(L_71);
		Il2CppChar L_73 = String_get_Chars_m4230566705(L_71, ((int32_t)((int32_t)L_72+(int32_t)2)), /*hidden argument*/NULL);
		Il2CppChar L_74 = L_73;
		Il2CppObject * L_75 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_74);
		String_t* L_76 = ___hex0;
		int32_t L_77 = V_0;
		NullCheck(L_76);
		Il2CppChar L_78 = String_get_Chars_m4230566705(L_76, ((int32_t)((int32_t)L_77+(int32_t)2)), /*hidden argument*/NULL);
		Il2CppChar L_79 = L_78;
		Il2CppObject * L_80 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_79);
		String_t* L_81 = String_Concat_m2000667605(NULL /*static, unused*/, L_70, L_75, L_80, /*hidden argument*/NULL);
		uint8_t L_82 = Byte_Parse_m4137294155(NULL /*static, unused*/, L_81, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_83 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_84 = ___hex0;
		int32_t L_85 = V_0;
		NullCheck(L_84);
		Il2CppChar L_86 = String_get_Chars_m4230566705(L_84, ((int32_t)((int32_t)L_85+(int32_t)3)), /*hidden argument*/NULL);
		Il2CppChar L_87 = L_86;
		Il2CppObject * L_88 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_87);
		String_t* L_89 = ___hex0;
		int32_t L_90 = V_0;
		NullCheck(L_89);
		Il2CppChar L_91 = String_get_Chars_m4230566705(L_89, ((int32_t)((int32_t)L_90+(int32_t)3)), /*hidden argument*/NULL);
		Il2CppChar L_92 = L_91;
		Il2CppObject * L_93 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_92);
		String_t* L_94 = String_Concat_m2000667605(NULL /*static, unused*/, L_83, L_88, L_93, /*hidden argument*/NULL);
		uint8_t L_95 = Byte_Parse_m4137294155(NULL /*static, unused*/, L_94, ((int32_t)512), /*hidden argument*/NULL);
		Color32__ctor_m1932627809(L_43, L_56, L_69, L_82, L_95, /*hidden argument*/NULL);
		goto IL_0253;
	}

IL_01bc:
	{
		Color32_t874517518 * L_96 = ___color1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_97 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_98 = ___hex0;
		int32_t L_99 = V_0;
		NullCheck(L_98);
		Il2CppChar L_100 = String_get_Chars_m4230566705(L_98, L_99, /*hidden argument*/NULL);
		Il2CppChar L_101 = L_100;
		Il2CppObject * L_102 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_101);
		String_t* L_103 = ___hex0;
		int32_t L_104 = V_0;
		NullCheck(L_103);
		Il2CppChar L_105 = String_get_Chars_m4230566705(L_103, L_104, /*hidden argument*/NULL);
		Il2CppChar L_106 = L_105;
		Il2CppObject * L_107 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_106);
		String_t* L_108 = String_Concat_m2000667605(NULL /*static, unused*/, L_97, L_102, L_107, /*hidden argument*/NULL);
		uint8_t L_109 = Byte_Parse_m4137294155(NULL /*static, unused*/, L_108, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_110 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_111 = ___hex0;
		int32_t L_112 = V_0;
		NullCheck(L_111);
		Il2CppChar L_113 = String_get_Chars_m4230566705(L_111, ((int32_t)((int32_t)L_112+(int32_t)1)), /*hidden argument*/NULL);
		Il2CppChar L_114 = L_113;
		Il2CppObject * L_115 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_114);
		String_t* L_116 = ___hex0;
		int32_t L_117 = V_0;
		NullCheck(L_116);
		Il2CppChar L_118 = String_get_Chars_m4230566705(L_116, ((int32_t)((int32_t)L_117+(int32_t)1)), /*hidden argument*/NULL);
		Il2CppChar L_119 = L_118;
		Il2CppObject * L_120 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_119);
		String_t* L_121 = String_Concat_m2000667605(NULL /*static, unused*/, L_110, L_115, L_120, /*hidden argument*/NULL);
		uint8_t L_122 = Byte_Parse_m4137294155(NULL /*static, unused*/, L_121, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_123 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_124 = ___hex0;
		int32_t L_125 = V_0;
		NullCheck(L_124);
		Il2CppChar L_126 = String_get_Chars_m4230566705(L_124, ((int32_t)((int32_t)L_125+(int32_t)2)), /*hidden argument*/NULL);
		Il2CppChar L_127 = L_126;
		Il2CppObject * L_128 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_127);
		String_t* L_129 = ___hex0;
		int32_t L_130 = V_0;
		NullCheck(L_129);
		Il2CppChar L_131 = String_get_Chars_m4230566705(L_129, ((int32_t)((int32_t)L_130+(int32_t)2)), /*hidden argument*/NULL);
		Il2CppChar L_132 = L_131;
		Il2CppObject * L_133 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_132);
		String_t* L_134 = String_Concat_m2000667605(NULL /*static, unused*/, L_123, L_128, L_133, /*hidden argument*/NULL);
		uint8_t L_135 = Byte_Parse_m4137294155(NULL /*static, unused*/, L_134, ((int32_t)512), /*hidden argument*/NULL);
		Color32__ctor_m1932627809(L_96, L_109, L_122, L_135, ((int32_t)255), /*hidden argument*/NULL);
	}

IL_0253:
	{
		return (bool)1;
	}

IL_0255:
	{
		Color32_t874517518 * L_136 = ___color1;
		Initobj (Color32_t874517518_il2cpp_TypeInfo_var, L_136);
		return (bool)0;
	}
}
// System.Void HSVChangedEvent::.ctor()
extern const MethodInfo* UnityEvent_3__ctor_m3100550874_MethodInfo_var;
extern const uint32_t HSVChangedEvent__ctor_m3043072144_MetadataUsageId;
extern "C"  void HSVChangedEvent__ctor_m3043072144 (HSVChangedEvent_t1170297569 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HSVChangedEvent__ctor_m3043072144_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_3__ctor_m3100550874(__this, /*hidden argument*/UnityEvent_3__ctor_m3100550874_MethodInfo_var);
		return;
	}
}
// System.Void HsvColor::.ctor(System.Double,System.Double,System.Double)
extern "C"  void HsvColor__ctor_m2096855601 (HsvColor_t1057062332 * __this, double ___h0, double ___s1, double ___v2, const MethodInfo* method)
{
	{
		double L_0 = ___h0;
		__this->set_H_0(L_0);
		double L_1 = ___s1;
		__this->set_S_1(L_1);
		double L_2 = ___v2;
		__this->set_V_2(L_2);
		return;
	}
}
extern "C"  void HsvColor__ctor_m2096855601_AdjustorThunk (Il2CppObject * __this, double ___h0, double ___s1, double ___v2, const MethodInfo* method)
{
	HsvColor_t1057062332 * _thisAdjusted = reinterpret_cast<HsvColor_t1057062332 *>(__this + 1);
	HsvColor__ctor_m2096855601(_thisAdjusted, ___h0, ___s1, ___v2, method);
}
// System.Single HsvColor::get_normalizedH()
extern "C"  float HsvColor_get_normalizedH_m3607787917 (HsvColor_t1057062332 * __this, const MethodInfo* method)
{
	{
		double L_0 = __this->get_H_0();
		return ((float)((float)(((float)((float)L_0)))/(float)(360.0f)));
	}
}
extern "C"  float HsvColor_get_normalizedH_m3607787917_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	HsvColor_t1057062332 * _thisAdjusted = reinterpret_cast<HsvColor_t1057062332 *>(__this + 1);
	return HsvColor_get_normalizedH_m3607787917(_thisAdjusted, method);
}
// System.Void HsvColor::set_normalizedH(System.Single)
extern "C"  void HsvColor_set_normalizedH_m2139074108 (HsvColor_t1057062332 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_H_0(((double)((double)(((double)((double)L_0)))*(double)(360.0))));
		return;
	}
}
extern "C"  void HsvColor_set_normalizedH_m2139074108_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	HsvColor_t1057062332 * _thisAdjusted = reinterpret_cast<HsvColor_t1057062332 *>(__this + 1);
	HsvColor_set_normalizedH_m2139074108(_thisAdjusted, ___value0, method);
}
// System.Single HsvColor::get_normalizedS()
extern "C"  float HsvColor_get_normalizedS_m3607786980 (HsvColor_t1057062332 * __this, const MethodInfo* method)
{
	{
		double L_0 = __this->get_S_1();
		return (((float)((float)L_0)));
	}
}
extern "C"  float HsvColor_get_normalizedS_m3607786980_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	HsvColor_t1057062332 * _thisAdjusted = reinterpret_cast<HsvColor_t1057062332 *>(__this + 1);
	return HsvColor_get_normalizedS_m3607786980(_thisAdjusted, method);
}
// System.Void HsvColor::set_normalizedS(System.Single)
extern "C"  void HsvColor_set_normalizedS_m413849441 (HsvColor_t1057062332 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_S_1((((double)((double)L_0))));
		return;
	}
}
extern "C"  void HsvColor_set_normalizedS_m413849441_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	HsvColor_t1057062332 * _thisAdjusted = reinterpret_cast<HsvColor_t1057062332 *>(__this + 1);
	HsvColor_set_normalizedS_m413849441(_thisAdjusted, ___value0, method);
}
// System.Single HsvColor::get_normalizedV()
extern "C"  float HsvColor_get_normalizedV_m3607786943 (HsvColor_t1057062332 * __this, const MethodInfo* method)
{
	{
		double L_0 = __this->get_V_2();
		return (((float)((float)L_0)));
	}
}
extern "C"  float HsvColor_get_normalizedV_m3607786943_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	HsvColor_t1057062332 * _thisAdjusted = reinterpret_cast<HsvColor_t1057062332 *>(__this + 1);
	return HsvColor_get_normalizedV_m3607786943(_thisAdjusted, method);
}
// System.Void HsvColor::set_normalizedV(System.Single)
extern "C"  void HsvColor_set_normalizedV_m460463238 (HsvColor_t1057062332 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_V_2((((double)((double)L_0))));
		return;
	}
}
extern "C"  void HsvColor_set_normalizedV_m460463238_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	HsvColor_t1057062332 * _thisAdjusted = reinterpret_cast<HsvColor_t1057062332 *>(__this + 1);
	HsvColor_set_normalizedV_m460463238(_thisAdjusted, ___value0, method);
}
// System.String HsvColor::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029399;
extern Il2CppCodeGenString* _stringLiteral3231012694;
extern Il2CppCodeGenString* _stringLiteral372029314;
extern Il2CppCodeGenString* _stringLiteral372029393;
extern const uint32_t HsvColor_ToString_m1590809902_MetadataUsageId;
extern "C"  String_t* HsvColor_ToString_m1590809902 (HsvColor_t1057062332 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HsvColor_ToString_m1590809902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029399);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029399);
		StringU5BU5D_t1642385972* L_1 = L_0;
		double* L_2 = __this->get_address_of_H_0();
		String_t* L_3 = Double_ToString_m2210043919(L_2, _stringLiteral3231012694, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1642385972* L_4 = L_1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral372029314);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029314);
		StringU5BU5D_t1642385972* L_5 = L_4;
		double* L_6 = __this->get_address_of_S_1();
		String_t* L_7 = Double_ToString_m2210043919(L_6, _stringLiteral3231012694, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t1642385972* L_8 = L_5;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral372029314);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029314);
		StringU5BU5D_t1642385972* L_9 = L_8;
		double* L_10 = __this->get_address_of_V_2();
		String_t* L_11 = Double_ToString_m2210043919(L_10, _stringLiteral3231012694, /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_11);
		StringU5BU5D_t1642385972* L_12 = L_9;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029393);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral372029393);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* HsvColor_ToString_m1590809902_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	HsvColor_t1057062332 * _thisAdjusted = reinterpret_cast<HsvColor_t1057062332 *>(__this + 1);
	return HsvColor_ToString_m1590809902(_thisAdjusted, method);
}
// HsvColor HSVUtil::ConvertRgbToHsv(UnityEngine.Color)
extern "C"  HsvColor_t1057062332  HSVUtil_ConvertRgbToHsv_m4088715697 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, const MethodInfo* method)
{
	{
		float L_0 = (&___color0)->get_r_0();
		float L_1 = (&___color0)->get_g_1();
		float L_2 = (&___color0)->get_b_2();
		HsvColor_t1057062332  L_3 = HSVUtil_ConvertRgbToHsv_m2238362913(NULL /*static, unused*/, (((double)((double)(((int32_t)((int32_t)((float)((float)L_0*(float)(255.0f))))))))), (((double)((double)(((int32_t)((int32_t)((float)((float)L_1*(float)(255.0f))))))))), (((double)((double)(((int32_t)((int32_t)((float)((float)L_2*(float)(255.0f))))))))), /*hidden argument*/NULL);
		return L_3;
	}
}
// HsvColor HSVUtil::ConvertRgbToHsv(System.Double,System.Double,System.Double)
extern Il2CppClass* HsvColor_t1057062332_il2cpp_TypeInfo_var;
extern const uint32_t HSVUtil_ConvertRgbToHsv_m2238362913_MetadataUsageId;
extern "C"  HsvColor_t1057062332  HSVUtil_ConvertRgbToHsv_m2238362913 (Il2CppObject * __this /* static, unused */, double ___r0, double ___b1, double ___g2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HSVUtil_ConvertRgbToHsv_m2238362913_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	double V_1 = 0.0;
	double V_2 = 0.0;
	double V_3 = 0.0;
	double V_4 = 0.0;
	HsvColor_t1057062332  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		V_2 = (0.0);
		double L_0 = ___r0;
		double L_1 = ___g2;
		double L_2 = Math_Min_m2551484304(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		double L_3 = ___b1;
		double L_4 = Math_Min_m2551484304(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		double L_5 = ___r0;
		double L_6 = ___g2;
		double L_7 = Math_Max_m3248989870(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		double L_8 = ___b1;
		double L_9 = Math_Max_m3248989870(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_4 = L_9;
		double L_10 = V_4;
		double L_11 = V_1;
		V_0 = ((double)((double)L_10-(double)L_11));
		double L_12 = V_4;
		if ((!(((double)L_12) == ((double)(0.0)))))
		{
			goto IL_004b;
		}
	}
	{
		V_3 = (0.0);
		goto IL_0050;
	}

IL_004b:
	{
		double L_13 = V_0;
		double L_14 = V_4;
		V_3 = ((double)((double)L_13/(double)L_14));
	}

IL_0050:
	{
		double L_15 = V_3;
		if ((!(((double)L_15) == ((double)(0.0)))))
		{
			goto IL_006e;
		}
	}
	{
		V_2 = (360.0);
		goto IL_00dd;
	}

IL_006e:
	{
		double L_16 = ___r0;
		double L_17 = V_4;
		if ((!(((double)L_16) == ((double)L_17))))
		{
			goto IL_0081;
		}
	}
	{
		double L_18 = ___g2;
		double L_19 = ___b1;
		double L_20 = V_0;
		V_2 = ((double)((double)((double)((double)L_18-(double)L_19))/(double)L_20));
		goto IL_00b6;
	}

IL_0081:
	{
		double L_21 = ___g2;
		double L_22 = V_4;
		if ((!(((double)L_21) == ((double)L_22))))
		{
			goto IL_009e;
		}
	}
	{
		double L_23 = ___b1;
		double L_24 = ___r0;
		double L_25 = V_0;
		V_2 = ((double)((double)(2.0)+(double)((double)((double)((double)((double)L_23-(double)L_24))/(double)L_25))));
		goto IL_00b6;
	}

IL_009e:
	{
		double L_26 = ___b1;
		double L_27 = V_4;
		if ((!(((double)L_26) == ((double)L_27))))
		{
			goto IL_00b6;
		}
	}
	{
		double L_28 = ___r0;
		double L_29 = ___g2;
		double L_30 = V_0;
		V_2 = ((double)((double)(4.0)+(double)((double)((double)((double)((double)L_28-(double)L_29))/(double)L_30))));
	}

IL_00b6:
	{
		double L_31 = V_2;
		V_2 = ((double)((double)L_31*(double)(60.0)));
		double L_32 = V_2;
		if ((!(((double)L_32) <= ((double)(0.0)))))
		{
			goto IL_00dd;
		}
	}
	{
		double L_33 = V_2;
		V_2 = ((double)((double)L_33+(double)(360.0)));
	}

IL_00dd:
	{
		Initobj (HsvColor_t1057062332_il2cpp_TypeInfo_var, (&V_5));
		double L_34 = V_2;
		(&V_5)->set_H_0(((double)((double)(360.0)-(double)L_34)));
		double L_35 = V_3;
		(&V_5)->set_S_1(L_35);
		double L_36 = V_4;
		(&V_5)->set_V_2(((double)((double)L_36/(double)(255.0))));
		HsvColor_t1057062332  L_37 = V_5;
		return L_37;
	}
}
// UnityEngine.Color HSVUtil::ConvertHsvToRgb(System.Double,System.Double,System.Double,System.Single)
extern "C"  Color_t2020392075  HSVUtil_ConvertHsvToRgb_m2339284600 (Il2CppObject * __this /* static, unused */, double ___h0, double ___s1, double ___v2, float ___alpha3, const MethodInfo* method)
{
	double V_0 = 0.0;
	double V_1 = 0.0;
	double V_2 = 0.0;
	int32_t V_3 = 0;
	double V_4 = 0.0;
	double V_5 = 0.0;
	double V_6 = 0.0;
	double V_7 = 0.0;
	{
		V_0 = (0.0);
		V_1 = (0.0);
		V_2 = (0.0);
		double L_0 = ___s1;
		if ((!(((double)L_0) == ((double)(0.0)))))
		{
			goto IL_0038;
		}
	}
	{
		double L_1 = ___v2;
		V_0 = L_1;
		double L_2 = ___v2;
		V_1 = L_2;
		double L_3 = ___v2;
		V_2 = L_3;
		goto IL_0117;
	}

IL_0038:
	{
		double L_4 = ___h0;
		if ((!(((double)L_4) == ((double)(360.0)))))
		{
			goto IL_0057;
		}
	}
	{
		___h0 = (0.0);
		goto IL_0064;
	}

IL_0057:
	{
		double L_5 = ___h0;
		___h0 = ((double)((double)L_5/(double)(60.0)));
	}

IL_0064:
	{
		double L_6 = ___h0;
		V_3 = (((int32_t)((int32_t)L_6)));
		double L_7 = ___h0;
		int32_t L_8 = V_3;
		V_4 = ((double)((double)L_7-(double)(((double)((double)L_8)))));
		double L_9 = ___v2;
		double L_10 = ___s1;
		V_5 = ((double)((double)L_9*(double)((double)((double)(1.0)-(double)L_10))));
		double L_11 = ___v2;
		double L_12 = ___s1;
		double L_13 = V_4;
		V_6 = ((double)((double)L_11*(double)((double)((double)(1.0)-(double)((double)((double)L_12*(double)L_13))))));
		double L_14 = ___v2;
		double L_15 = ___s1;
		double L_16 = V_4;
		V_7 = ((double)((double)L_14*(double)((double)((double)(1.0)-(double)((double)((double)L_15*(double)((double)((double)(1.0)-(double)L_16))))))));
		int32_t L_17 = V_3;
		if (L_17 == 0)
		{
			goto IL_00c9;
		}
		if (L_17 == 1)
		{
			goto IL_00d6;
		}
		if (L_17 == 2)
		{
			goto IL_00e3;
		}
		if (L_17 == 3)
		{
			goto IL_00f0;
		}
		if (L_17 == 4)
		{
			goto IL_00fd;
		}
	}
	{
		goto IL_010a;
	}

IL_00c9:
	{
		double L_18 = ___v2;
		V_0 = L_18;
		double L_19 = V_7;
		V_1 = L_19;
		double L_20 = V_5;
		V_2 = L_20;
		goto IL_0117;
	}

IL_00d6:
	{
		double L_21 = V_6;
		V_0 = L_21;
		double L_22 = ___v2;
		V_1 = L_22;
		double L_23 = V_5;
		V_2 = L_23;
		goto IL_0117;
	}

IL_00e3:
	{
		double L_24 = V_5;
		V_0 = L_24;
		double L_25 = ___v2;
		V_1 = L_25;
		double L_26 = V_7;
		V_2 = L_26;
		goto IL_0117;
	}

IL_00f0:
	{
		double L_27 = V_5;
		V_0 = L_27;
		double L_28 = V_6;
		V_1 = L_28;
		double L_29 = ___v2;
		V_2 = L_29;
		goto IL_0117;
	}

IL_00fd:
	{
		double L_30 = V_7;
		V_0 = L_30;
		double L_31 = V_5;
		V_1 = L_31;
		double L_32 = ___v2;
		V_2 = L_32;
		goto IL_0117;
	}

IL_010a:
	{
		double L_33 = ___v2;
		V_0 = L_33;
		double L_34 = V_5;
		V_1 = L_34;
		double L_35 = V_6;
		V_2 = L_35;
		goto IL_0117;
	}

IL_0117:
	{
		double L_36 = V_0;
		double L_37 = V_1;
		double L_38 = V_2;
		float L_39 = ___alpha3;
		Color_t2020392075  L_40;
		memset(&L_40, 0, sizeof(L_40));
		Color__ctor_m1909920690(&L_40, (((float)((float)L_36))), (((float)((float)L_37))), (((float)((float)L_38))), L_39, /*hidden argument*/NULL);
		return L_40;
	}
}
// System.Void SVBoxSlider::.ctor()
extern "C"  void SVBoxSlider__ctor_m2623596066 (SVBoxSlider_t1173082351 * __this, const MethodInfo* method)
{
	{
		__this->set_lastH_5((-1.0f));
		__this->set_listen_6((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RectTransform SVBoxSlider::get_rectTransform()
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern const uint32_t SVBoxSlider_get_rectTransform_m3427717911_MetadataUsageId;
extern "C"  RectTransform_t3349966182 * SVBoxSlider_get_rectTransform_m3427717911 (SVBoxSlider_t1173082351 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SVBoxSlider_get_rectTransform_m3427717911_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		return ((RectTransform_t3349966182 *)IsInstSealed(L_0, RectTransform_t3349966182_il2cpp_TypeInfo_var));
	}
}
// System.Void SVBoxSlider::Awake()
extern const MethodInfo* Component_GetComponent_TisBoxSlider_t1871650694_m3168588160_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRawImage_t2749640213_m1817787565_MethodInfo_var;
extern const uint32_t SVBoxSlider_Awake_m1174811035_MetadataUsageId;
extern "C"  void SVBoxSlider_Awake_m1174811035 (SVBoxSlider_t1173082351 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SVBoxSlider_Awake_m1174811035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BoxSlider_t1871650694 * L_0 = Component_GetComponent_TisBoxSlider_t1871650694_m3168588160(__this, /*hidden argument*/Component_GetComponent_TisBoxSlider_t1871650694_m3168588160_MethodInfo_var);
		__this->set_slider_3(L_0);
		RawImage_t2749640213 * L_1 = Component_GetComponent_TisRawImage_t2749640213_m1817787565(__this, /*hidden argument*/Component_GetComponent_TisRawImage_t2749640213_m1817787565_MethodInfo_var);
		__this->set_image_4(L_1);
		SVBoxSlider_RegenerateSVTexture_m4094171080(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SVBoxSlider::OnEnable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_2_t134459182_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_3_t235051313_il2cpp_TypeInfo_var;
extern const MethodInfo* SVBoxSlider_SliderChanged_m3739635521_MethodInfo_var;
extern const MethodInfo* UnityAction_2__ctor_m2891411084_MethodInfo_var;
extern const MethodInfo* UnityEvent_2_AddListener_m297354571_MethodInfo_var;
extern const MethodInfo* SVBoxSlider_HSVChanged_m3533720340_MethodInfo_var;
extern const MethodInfo* UnityAction_3__ctor_m3626891334_MethodInfo_var;
extern const MethodInfo* UnityEvent_3_AddListener_m3608966849_MethodInfo_var;
extern const uint32_t SVBoxSlider_OnEnable_m542656758_MetadataUsageId;
extern "C"  void SVBoxSlider_OnEnable_m542656758 (SVBoxSlider_t1173082351 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SVBoxSlider_OnEnable_m542656758_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0053;
		}
	}
	{
		ColorPicker_t3035206225 * L_1 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0053;
		}
	}
	{
		BoxSlider_t1871650694 * L_3 = __this->get_slider_3();
		NullCheck(L_3);
		BoxSliderEvent_t1774115848 * L_4 = BoxSlider_get_onValueChanged_m1694276966(L_3, /*hidden argument*/NULL);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)SVBoxSlider_SliderChanged_m3739635521_MethodInfo_var);
		UnityAction_2_t134459182 * L_6 = (UnityAction_2_t134459182 *)il2cpp_codegen_object_new(UnityAction_2_t134459182_il2cpp_TypeInfo_var);
		UnityAction_2__ctor_m2891411084(L_6, __this, L_5, /*hidden argument*/UnityAction_2__ctor_m2891411084_MethodInfo_var);
		NullCheck(L_4);
		UnityEvent_2_AddListener_m297354571(L_4, L_6, /*hidden argument*/UnityEvent_2_AddListener_m297354571_MethodInfo_var);
		ColorPicker_t3035206225 * L_7 = __this->get_picker_2();
		NullCheck(L_7);
		HSVChangedEvent_t1170297569 * L_8 = L_7->get_onHSVChanged_10();
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)SVBoxSlider_HSVChanged_m3533720340_MethodInfo_var);
		UnityAction_3_t235051313 * L_10 = (UnityAction_3_t235051313 *)il2cpp_codegen_object_new(UnityAction_3_t235051313_il2cpp_TypeInfo_var);
		UnityAction_3__ctor_m3626891334(L_10, __this, L_9, /*hidden argument*/UnityAction_3__ctor_m3626891334_MethodInfo_var);
		NullCheck(L_8);
		UnityEvent_3_AddListener_m3608966849(L_8, L_10, /*hidden argument*/UnityEvent_3_AddListener_m3608966849_MethodInfo_var);
	}

IL_0053:
	{
		return;
	}
}
// System.Void SVBoxSlider::OnDisable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_2_t134459182_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_3_t235051313_il2cpp_TypeInfo_var;
extern const MethodInfo* SVBoxSlider_SliderChanged_m3739635521_MethodInfo_var;
extern const MethodInfo* UnityAction_2__ctor_m2891411084_MethodInfo_var;
extern const MethodInfo* UnityEvent_2_RemoveListener_m491466926_MethodInfo_var;
extern const MethodInfo* SVBoxSlider_HSVChanged_m3533720340_MethodInfo_var;
extern const MethodInfo* UnityAction_3__ctor_m3626891334_MethodInfo_var;
extern const MethodInfo* UnityEvent_3_RemoveListener_m2407167912_MethodInfo_var;
extern const uint32_t SVBoxSlider_OnDisable_m2237433047_MetadataUsageId;
extern "C"  void SVBoxSlider_OnDisable_m2237433047 (SVBoxSlider_t1173082351 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SVBoxSlider_OnDisable_m2237433047_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorPicker_t3035206225 * L_0 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0049;
		}
	}
	{
		BoxSlider_t1871650694 * L_2 = __this->get_slider_3();
		NullCheck(L_2);
		BoxSliderEvent_t1774115848 * L_3 = BoxSlider_get_onValueChanged_m1694276966(L_2, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)SVBoxSlider_SliderChanged_m3739635521_MethodInfo_var);
		UnityAction_2_t134459182 * L_5 = (UnityAction_2_t134459182 *)il2cpp_codegen_object_new(UnityAction_2_t134459182_il2cpp_TypeInfo_var);
		UnityAction_2__ctor_m2891411084(L_5, __this, L_4, /*hidden argument*/UnityAction_2__ctor_m2891411084_MethodInfo_var);
		NullCheck(L_3);
		UnityEvent_2_RemoveListener_m491466926(L_3, L_5, /*hidden argument*/UnityEvent_2_RemoveListener_m491466926_MethodInfo_var);
		ColorPicker_t3035206225 * L_6 = __this->get_picker_2();
		NullCheck(L_6);
		HSVChangedEvent_t1170297569 * L_7 = L_6->get_onHSVChanged_10();
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)SVBoxSlider_HSVChanged_m3533720340_MethodInfo_var);
		UnityAction_3_t235051313 * L_9 = (UnityAction_3_t235051313 *)il2cpp_codegen_object_new(UnityAction_3_t235051313_il2cpp_TypeInfo_var);
		UnityAction_3__ctor_m3626891334(L_9, __this, L_8, /*hidden argument*/UnityAction_3__ctor_m3626891334_MethodInfo_var);
		NullCheck(L_7);
		UnityEvent_3_RemoveListener_m2407167912(L_7, L_9, /*hidden argument*/UnityEvent_3_RemoveListener_m2407167912_MethodInfo_var);
	}

IL_0049:
	{
		return;
	}
}
// System.Void SVBoxSlider::OnDestroy()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t SVBoxSlider_OnDestroy_m1087675145_MetadataUsageId;
extern "C"  void SVBoxSlider_OnDestroy_m1087675145 (SVBoxSlider_t1173082351 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SVBoxSlider_OnDestroy_m1087675145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RawImage_t2749640213 * L_0 = __this->get_image_4();
		NullCheck(L_0);
		Texture_t2243626319 * L_1 = RawImage_get_texture_m2258734143(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		RawImage_t2749640213 * L_3 = __this->get_image_4();
		NullCheck(L_3);
		Texture_t2243626319 * L_4 = RawImage_get_texture_m2258734143(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void SVBoxSlider::SliderChanged(System.Single,System.Single)
extern "C"  void SVBoxSlider_SliderChanged_m3739635521 (SVBoxSlider_t1173082351 * __this, float ___saturation0, float ___value1, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_listen_6();
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		ColorPicker_t3035206225 * L_1 = __this->get_picker_2();
		float L_2 = ___saturation0;
		NullCheck(L_1);
		ColorPicker_AssignColor_m579716850(L_1, 5, L_2, /*hidden argument*/NULL);
		ColorPicker_t3035206225 * L_3 = __this->get_picker_2();
		float L_4 = ___value1;
		NullCheck(L_3);
		ColorPicker_AssignColor_m579716850(L_3, 6, L_4, /*hidden argument*/NULL);
	}

IL_0025:
	{
		__this->set_listen_6((bool)1);
		return;
	}
}
// System.Void SVBoxSlider::HSVChanged(System.Single,System.Single,System.Single)
extern "C"  void SVBoxSlider_HSVChanged_m3533720340 (SVBoxSlider_t1173082351 * __this, float ___h0, float ___s1, float ___v2, const MethodInfo* method)
{
	{
		float L_0 = __this->get_lastH_5();
		float L_1 = ___h0;
		if ((((float)L_0) == ((float)L_1)))
		{
			goto IL_0019;
		}
	}
	{
		float L_2 = ___h0;
		__this->set_lastH_5(L_2);
		SVBoxSlider_RegenerateSVTexture_m4094171080(__this, /*hidden argument*/NULL);
	}

IL_0019:
	{
		float L_3 = ___s1;
		BoxSlider_t1871650694 * L_4 = __this->get_slider_3();
		NullCheck(L_4);
		float L_5 = BoxSlider_get_normalizedValue_m911068691(L_4, /*hidden argument*/NULL);
		if ((((float)L_3) == ((float)L_5)))
		{
			goto IL_003d;
		}
	}
	{
		__this->set_listen_6((bool)0);
		BoxSlider_t1871650694 * L_6 = __this->get_slider_3();
		float L_7 = ___s1;
		NullCheck(L_6);
		BoxSlider_set_normalizedValue_m2010660092(L_6, L_7, /*hidden argument*/NULL);
	}

IL_003d:
	{
		float L_8 = ___v2;
		BoxSlider_t1871650694 * L_9 = __this->get_slider_3();
		NullCheck(L_9);
		float L_10 = BoxSlider_get_normalizedValueY_m3976688554(L_9, /*hidden argument*/NULL);
		if ((((float)L_8) == ((float)L_10)))
		{
			goto IL_0061;
		}
	}
	{
		__this->set_listen_6((bool)0);
		BoxSlider_t1871650694 * L_11 = __this->get_slider_3();
		float L_12 = ___v2;
		NullCheck(L_11);
		BoxSlider_set_normalizedValueY_m851020601(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0061:
	{
		return;
	}
}
// System.Void SVBoxSlider::RegenerateSVTexture()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppClass* Color32U5BU5D_t30278651_il2cpp_TypeInfo_var;
extern const uint32_t SVBoxSlider_RegenerateSVTexture_m4094171080_MetadataUsageId;
extern "C"  void SVBoxSlider_RegenerateSVTexture_m4094171080 (SVBoxSlider_t1173082351 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SVBoxSlider_RegenerateSVTexture_m4094171080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	Texture2D_t3542995729 * V_1 = NULL;
	int32_t V_2 = 0;
	Color32U5BU5D_t30278651* V_3 = NULL;
	int32_t V_4 = 0;
	float G_B3_0 = 0.0f;
	{
		ColorPicker_t3035206225 * L_0 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		ColorPicker_t3035206225 * L_2 = __this->get_picker_2();
		NullCheck(L_2);
		float L_3 = ColorPicker_get_H_m3860865411(L_2, /*hidden argument*/NULL);
		G_B3_0 = ((float)((float)L_3*(float)(360.0f)));
		goto IL_002c;
	}

IL_0027:
	{
		G_B3_0 = (0.0f);
	}

IL_002c:
	{
		V_0 = (((double)((double)G_B3_0)));
		RawImage_t2749640213 * L_4 = __this->get_image_4();
		NullCheck(L_4);
		Texture_t2243626319 * L_5 = RawImage_get_texture_m2258734143(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0054;
		}
	}
	{
		RawImage_t2749640213 * L_7 = __this->get_image_4();
		NullCheck(L_7);
		Texture_t2243626319 * L_8 = RawImage_get_texture_m2258734143(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0054:
	{
		Texture2D_t3542995729 * L_9 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m3598323350(L_9, ((int32_t)100), ((int32_t)100), /*hidden argument*/NULL);
		V_1 = L_9;
		Texture2D_t3542995729 * L_10 = V_1;
		NullCheck(L_10);
		Object_set_hideFlags_m2204253440(L_10, ((int32_t)52), /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_00cc;
	}

IL_006d:
	{
		V_3 = ((Color32U5BU5D_t30278651*)SZArrayNew(Color32U5BU5D_t30278651_il2cpp_TypeInfo_var, (uint32_t)((int32_t)100)));
		V_4 = 0;
		goto IL_00b3;
	}

IL_007d:
	{
		Color32U5BU5D_t30278651* L_11 = V_3;
		int32_t L_12 = V_4;
		NullCheck(L_11);
		double L_13 = V_0;
		int32_t L_14 = V_2;
		int32_t L_15 = V_4;
		Color_t2020392075  L_16 = HSVUtil_ConvertHsvToRgb_m2339284600(NULL /*static, unused*/, L_13, (((double)((double)((float)((float)(((float)((float)L_14)))/(float)(100.0f)))))), (((double)((double)((float)((float)(((float)((float)L_15)))/(float)(100.0f)))))), (1.0f), /*hidden argument*/NULL);
		Color32_t874517518  L_17 = Color32_op_Implicit_m624191464(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		(*(Color32_t874517518 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))) = L_17;
		int32_t L_18 = V_4;
		V_4 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_00b3:
	{
		int32_t L_19 = V_4;
		if ((((int32_t)L_19) < ((int32_t)((int32_t)100))))
		{
			goto IL_007d;
		}
	}
	{
		Texture2D_t3542995729 * L_20 = V_1;
		int32_t L_21 = V_2;
		Color32U5BU5D_t30278651* L_22 = V_3;
		NullCheck(L_20);
		Texture2D_SetPixels32_m1440518781(L_20, L_21, 0, 1, ((int32_t)100), L_22, /*hidden argument*/NULL);
		int32_t L_23 = V_2;
		V_2 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_00cc:
	{
		int32_t L_24 = V_2;
		if ((((int32_t)L_24) < ((int32_t)((int32_t)100))))
		{
			goto IL_006d;
		}
	}
	{
		Texture2D_t3542995729 * L_25 = V_1;
		NullCheck(L_25);
		Texture2D_Apply_m3543341930(L_25, /*hidden argument*/NULL);
		RawImage_t2749640213 * L_26 = __this->get_image_4();
		Texture2D_t3542995729 * L_27 = V_1;
		NullCheck(L_26);
		RawImage_set_texture_m2400157626(L_26, L_27, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TiltWindow::.ctor()
extern "C"  void TiltWindow__ctor_m3071582230 (TiltWindow_t1839185375 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3067419446(&L_0, (5.0f), (3.0f), /*hidden argument*/NULL);
		__this->set_range_2(L_0);
		Vector2_t2243707579  L_1 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mRot_5(L_1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TiltWindow::Start()
extern "C"  void TiltWindow_Start_m3112429194 (TiltWindow_t1839185375 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set_mTrans_3(L_0);
		Transform_t3275118058 * L_1 = __this->get_mTrans_3();
		NullCheck(L_1);
		Quaternion_t4030073918  L_2 = Transform_get_localRotation_m4001487205(L_1, /*hidden argument*/NULL);
		__this->set_mStart_4(L_2);
		return;
	}
}
// System.Void TiltWindow::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t TiltWindow_Update_m460467911_MetadataUsageId;
extern "C"  void TiltWindow_Update_m460467911 (TiltWindow_t1839185375 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TiltWindow_Update_m460467911_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_0 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)(((float)((float)L_1)))*(float)(0.5f)));
		int32_t L_2 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = ((float)((float)(((float)((float)L_2)))*(float)(0.5f)));
		float L_3 = (&V_0)->get_x_1();
		float L_4 = V_1;
		float L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, ((float)((float)((float)((float)L_3-(float)L_4))/(float)L_5)), (-1.0f), (1.0f), /*hidden argument*/NULL);
		V_3 = L_6;
		float L_7 = (&V_0)->get_y_2();
		float L_8 = V_2;
		float L_9 = V_2;
		float L_10 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, ((float)((float)((float)((float)L_7-(float)L_8))/(float)L_9)), (-1.0f), (1.0f), /*hidden argument*/NULL);
		V_4 = L_10;
		Vector2_t2243707579  L_11 = __this->get_mRot_5();
		float L_12 = V_3;
		float L_13 = V_4;
		Vector2_t2243707579  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector2__ctor_m3067419446(&L_14, L_12, L_13, /*hidden argument*/NULL);
		float L_15 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_16 = Vector2_Lerp_m1511850087(NULL /*static, unused*/, L_11, L_14, ((float)((float)L_15*(float)(5.0f))), /*hidden argument*/NULL);
		__this->set_mRot_5(L_16);
		Transform_t3275118058 * L_17 = __this->get_mTrans_3();
		Quaternion_t4030073918  L_18 = __this->get_mStart_4();
		Vector2_t2243707579 * L_19 = __this->get_address_of_mRot_5();
		float L_20 = L_19->get_y_1();
		Vector2_t2243707579 * L_21 = __this->get_address_of_range_2();
		float L_22 = L_21->get_y_1();
		Vector2_t2243707579 * L_23 = __this->get_address_of_mRot_5();
		float L_24 = L_23->get_x_0();
		Vector2_t2243707579 * L_25 = __this->get_address_of_range_2();
		float L_26 = L_25->get_x_0();
		Quaternion_t4030073918  L_27 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, ((float)((float)((-L_20))*(float)L_22)), ((float)((float)L_24*(float)L_26)), (0.0f), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_28 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_18, L_27, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_localRotation_m2055111962(L_17, L_28, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::.ctor()
extern Il2CppClass* BoxSliderEvent_t1774115848_il2cpp_TypeInfo_var;
extern Il2CppClass* Selectable_t1490392188_il2cpp_TypeInfo_var;
extern const uint32_t BoxSlider__ctor_m1006728114_MetadataUsageId;
extern "C"  void BoxSlider__ctor_m1006728114 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider__ctor_m1006728114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_MaxValue_18((1.0f));
		__this->set_m_Value_20((1.0f));
		__this->set_m_ValueY_21((1.0f));
		BoxSliderEvent_t1774115848 * L_0 = (BoxSliderEvent_t1774115848 *)il2cpp_codegen_object_new(BoxSliderEvent_t1774115848_il2cpp_TypeInfo_var);
		BoxSliderEvent__ctor_m304261805(L_0, /*hidden argument*/NULL);
		__this->set_m_OnValueChanged_22(L_0);
		Vector2_t2243707579  L_1 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Offset_25(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Selectable_t1490392188_il2cpp_TypeInfo_var);
		Selectable__ctor_m1440593935(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RectTransform UnityEngine.UI.BoxSlider::get_handleRect()
extern "C"  RectTransform_t3349966182 * BoxSlider_get_handleRect_m2621304777 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_m_HandleRect_16();
		return L_0;
	}
}
// System.Void UnityEngine.UI.BoxSlider::set_handleRect(UnityEngine.RectTransform)
extern const MethodInfo* BoxSlider_SetClass_TisRectTransform_t3349966182_m272479997_MethodInfo_var;
extern const uint32_t BoxSlider_set_handleRect_m168119168_MetadataUsageId;
extern "C"  void BoxSlider_set_handleRect_m168119168 (BoxSlider_t1871650694 * __this, RectTransform_t3349966182 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_set_handleRect_m168119168_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t3349966182 ** L_0 = __this->get_address_of_m_HandleRect_16();
		RectTransform_t3349966182 * L_1 = ___value0;
		bool L_2 = BoxSlider_SetClass_TisRectTransform_t3349966182_m272479997(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/BoxSlider_SetClass_TisRectTransform_t3349966182_m272479997_MethodInfo_var);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		BoxSlider_UpdateCachedReferences_m3248150675(__this, /*hidden argument*/NULL);
		BoxSlider_UpdateVisuals_m495585056(__this, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Single UnityEngine.UI.BoxSlider::get_minValue()
extern "C"  float BoxSlider_get_minValue_m3041624170 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_MinValue_17();
		return L_0;
	}
}
// System.Void UnityEngine.UI.BoxSlider::set_minValue(System.Single)
extern const MethodInfo* BoxSlider_SetStruct_TisSingle_t2076509932_m2710312218_MethodInfo_var;
extern const uint32_t BoxSlider_set_minValue_m1004770411_MetadataUsageId;
extern "C"  void BoxSlider_set_minValue_m1004770411 (BoxSlider_t1871650694 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_set_minValue_m1004770411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float* L_0 = __this->get_address_of_m_MinValue_17();
		float L_1 = ___value0;
		bool L_2 = BoxSlider_SetStruct_TisSingle_t2076509932_m2710312218(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/BoxSlider_SetStruct_TisSingle_t2076509932_m2710312218_MethodInfo_var);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		float L_3 = __this->get_m_Value_20();
		BoxSlider_Set_m3989333985(__this, L_3, /*hidden argument*/NULL);
		float L_4 = __this->get_m_ValueY_21();
		BoxSlider_SetY_m1241843396(__this, L_4, /*hidden argument*/NULL);
		BoxSlider_UpdateVisuals_m495585056(__this, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Single UnityEngine.UI.BoxSlider::get_maxValue()
extern "C"  float BoxSlider_get_maxValue_m1317285860 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_MaxValue_18();
		return L_0;
	}
}
// System.Void UnityEngine.UI.BoxSlider::set_maxValue(System.Single)
extern const MethodInfo* BoxSlider_SetStruct_TisSingle_t2076509932_m2710312218_MethodInfo_var;
extern const uint32_t BoxSlider_set_maxValue_m1702472861_MetadataUsageId;
extern "C"  void BoxSlider_set_maxValue_m1702472861 (BoxSlider_t1871650694 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_set_maxValue_m1702472861_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float* L_0 = __this->get_address_of_m_MaxValue_18();
		float L_1 = ___value0;
		bool L_2 = BoxSlider_SetStruct_TisSingle_t2076509932_m2710312218(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/BoxSlider_SetStruct_TisSingle_t2076509932_m2710312218_MethodInfo_var);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		float L_3 = __this->get_m_Value_20();
		BoxSlider_Set_m3989333985(__this, L_3, /*hidden argument*/NULL);
		float L_4 = __this->get_m_ValueY_21();
		BoxSlider_SetY_m1241843396(__this, L_4, /*hidden argument*/NULL);
		BoxSlider_UpdateVisuals_m495585056(__this, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Boolean UnityEngine.UI.BoxSlider::get_wholeNumbers()
extern "C"  bool BoxSlider_get_wholeNumbers_m627638124 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_WholeNumbers_19();
		return L_0;
	}
}
// System.Void UnityEngine.UI.BoxSlider::set_wholeNumbers(System.Boolean)
extern const MethodInfo* BoxSlider_SetStruct_TisBoolean_t3825574718_m1967921404_MethodInfo_var;
extern const uint32_t BoxSlider_set_wholeNumbers_m2694006389_MetadataUsageId;
extern "C"  void BoxSlider_set_wholeNumbers_m2694006389 (BoxSlider_t1871650694 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_set_wholeNumbers_m2694006389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool* L_0 = __this->get_address_of_m_WholeNumbers_19();
		bool L_1 = ___value0;
		bool L_2 = BoxSlider_SetStruct_TisBoolean_t3825574718_m1967921404(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/BoxSlider_SetStruct_TisBoolean_t3825574718_m1967921404_MethodInfo_var);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		float L_3 = __this->get_m_Value_20();
		BoxSlider_Set_m3989333985(__this, L_3, /*hidden argument*/NULL);
		float L_4 = __this->get_m_ValueY_21();
		BoxSlider_SetY_m1241843396(__this, L_4, /*hidden argument*/NULL);
		BoxSlider_UpdateVisuals_m495585056(__this, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Single UnityEngine.UI.BoxSlider::get_value()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t BoxSlider_get_value_m2439160562_MetadataUsageId;
extern "C"  float BoxSlider_get_value_m2439160562 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_get_value_m2439160562_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = BoxSlider_get_wholeNumbers_m627638124(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		float L_1 = __this->get_m_Value_20();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = bankers_roundf(L_1);
		return L_2;
	}

IL_0017:
	{
		float L_3 = __this->get_m_Value_20();
		return L_3;
	}
}
// System.Void UnityEngine.UI.BoxSlider::set_value(System.Single)
extern "C"  void BoxSlider_set_value_m3164710557 (BoxSlider_t1871650694 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		BoxSlider_Set_m3989333985(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.UI.BoxSlider::get_normalizedValue()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t BoxSlider_get_normalizedValue_m911068691_MetadataUsageId;
extern "C"  float BoxSlider_get_normalizedValue_m911068691 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_get_normalizedValue_m911068691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = BoxSlider_get_minValue_m3041624170(__this, /*hidden argument*/NULL);
		float L_1 = BoxSlider_get_maxValue_m1317285860(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_2 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		return (0.0f);
	}

IL_001c:
	{
		float L_3 = BoxSlider_get_minValue_m3041624170(__this, /*hidden argument*/NULL);
		float L_4 = BoxSlider_get_maxValue_m1317285860(__this, /*hidden argument*/NULL);
		float L_5 = BoxSlider_get_value_m2439160562(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_6 = Mathf_InverseLerp_m55890283(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.UI.BoxSlider::set_normalizedValue(System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t BoxSlider_set_normalizedValue_m2010660092_MetadataUsageId;
extern "C"  void BoxSlider_set_normalizedValue_m2010660092 (BoxSlider_t1871650694 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_set_normalizedValue_m2010660092_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = BoxSlider_get_minValue_m3041624170(__this, /*hidden argument*/NULL);
		float L_1 = BoxSlider_get_maxValue_m1317285860(__this, /*hidden argument*/NULL);
		float L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		BoxSlider_set_value_m3164710557(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.UI.BoxSlider::get_valueY()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t BoxSlider_get_valueY_m2581533163_MetadataUsageId;
extern "C"  float BoxSlider_get_valueY_m2581533163 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_get_valueY_m2581533163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = BoxSlider_get_wholeNumbers_m627638124(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		float L_1 = __this->get_m_ValueY_21();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = bankers_roundf(L_1);
		return L_2;
	}

IL_0017:
	{
		float L_3 = __this->get_m_ValueY_21();
		return L_3;
	}
}
// System.Void UnityEngine.UI.BoxSlider::set_valueY(System.Single)
extern "C"  void BoxSlider_set_valueY_m2217414554 (BoxSlider_t1871650694 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		BoxSlider_SetY_m1241843396(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.UI.BoxSlider::get_normalizedValueY()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t BoxSlider_get_normalizedValueY_m3976688554_MetadataUsageId;
extern "C"  float BoxSlider_get_normalizedValueY_m3976688554 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_get_normalizedValueY_m3976688554_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = BoxSlider_get_minValue_m3041624170(__this, /*hidden argument*/NULL);
		float L_1 = BoxSlider_get_maxValue_m1317285860(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_2 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		return (0.0f);
	}

IL_001c:
	{
		float L_3 = BoxSlider_get_minValue_m3041624170(__this, /*hidden argument*/NULL);
		float L_4 = BoxSlider_get_maxValue_m1317285860(__this, /*hidden argument*/NULL);
		float L_5 = BoxSlider_get_valueY_m2581533163(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_6 = Mathf_InverseLerp_m55890283(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.UI.BoxSlider::set_normalizedValueY(System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t BoxSlider_set_normalizedValueY_m851020601_MetadataUsageId;
extern "C"  void BoxSlider_set_normalizedValueY_m851020601 (BoxSlider_t1871650694 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_set_normalizedValueY_m851020601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = BoxSlider_get_minValue_m3041624170(__this, /*hidden argument*/NULL);
		float L_1 = BoxSlider_get_maxValue_m1317285860(__this, /*hidden argument*/NULL);
		float L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		BoxSlider_set_valueY_m2217414554(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.BoxSlider/BoxSliderEvent UnityEngine.UI.BoxSlider::get_onValueChanged()
extern "C"  BoxSliderEvent_t1774115848 * BoxSlider_get_onValueChanged_m1694276966 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	{
		BoxSliderEvent_t1774115848 * L_0 = __this->get_m_OnValueChanged_22();
		return L_0;
	}
}
// System.Void UnityEngine.UI.BoxSlider::set_onValueChanged(UnityEngine.UI.BoxSlider/BoxSliderEvent)
extern "C"  void BoxSlider_set_onValueChanged_m1579271121 (BoxSlider_t1871650694 * __this, BoxSliderEvent_t1774115848 * ___value0, const MethodInfo* method)
{
	{
		BoxSliderEvent_t1774115848 * L_0 = ___value0;
		__this->set_m_OnValueChanged_22(L_0);
		return;
	}
}
// System.Single UnityEngine.UI.BoxSlider::get_stepSize()
extern "C"  float BoxSlider_get_stepSize_m1073340932 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		bool L_0 = BoxSlider_get_wholeNumbers_m627638124(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		G_B3_0 = (1.0f);
		goto IL_0028;
	}

IL_0015:
	{
		float L_1 = BoxSlider_get_maxValue_m1317285860(__this, /*hidden argument*/NULL);
		float L_2 = BoxSlider_get_minValue_m3041624170(__this, /*hidden argument*/NULL);
		G_B3_0 = ((float)((float)((float)((float)L_1-(float)L_2))*(float)(0.1f)));
	}

IL_0028:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.UI.BoxSlider::Rebuild(UnityEngine.UI.CanvasUpdate)
extern "C"  void BoxSlider_Rebuild_m3577117895 (BoxSlider_t1871650694 * __this, int32_t ___executing0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::LayoutComplete()
extern "C"  void BoxSlider_LayoutComplete_m2656874525 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::GraphicUpdateComplete()
extern "C"  void BoxSlider_GraphicUpdateComplete_m2980925808 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::OnEnable()
extern "C"  void BoxSlider_OnEnable_m2230558070 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	{
		Selectable_OnEnable_m3825327683(__this, /*hidden argument*/NULL);
		BoxSlider_UpdateCachedReferences_m3248150675(__this, /*hidden argument*/NULL);
		float L_0 = __this->get_m_Value_20();
		BoxSlider_Set_m1512577286(__this, L_0, (bool)0, /*hidden argument*/NULL);
		float L_1 = __this->get_m_ValueY_21();
		BoxSlider_SetY_m956310887(__this, L_1, (bool)0, /*hidden argument*/NULL);
		BoxSlider_UpdateVisuals_m495585056(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::OnDisable()
extern "C"  void BoxSlider_OnDisable_m3760522835 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	{
		DrivenRectTransformTracker_t154385424 * L_0 = __this->get_address_of_m_Tracker_26();
		DrivenRectTransformTracker_Clear_m864483440(L_0, /*hidden argument*/NULL);
		Selectable_OnDisable_m2660228016(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::UpdateCachedReferences()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRectTransform_t3349966182_m1310250299_MethodInfo_var;
extern const uint32_t BoxSlider_UpdateCachedReferences_m3248150675_MetadataUsageId;
extern "C"  void BoxSlider_UpdateCachedReferences_m3248150675 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_UpdateCachedReferences_m3248150675_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t3349966182 * L_0 = __this->get_m_HandleRect_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0052;
		}
	}
	{
		RectTransform_t3349966182 * L_2 = __this->get_m_HandleRect_16();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		__this->set_m_HandleTransform_23(L_3);
		Transform_t3275118058 * L_4 = __this->get_m_HandleTransform_23();
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Transform_get_parent_m147407266(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004d;
		}
	}
	{
		Transform_t3275118058 * L_7 = __this->get_m_HandleTransform_23();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = Transform_get_parent_m147407266(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		RectTransform_t3349966182 * L_9 = Component_GetComponent_TisRectTransform_t3349966182_m1310250299(L_8, /*hidden argument*/Component_GetComponent_TisRectTransform_t3349966182_m1310250299_MethodInfo_var);
		__this->set_m_HandleContainerRect_24(L_9);
	}

IL_004d:
	{
		goto IL_0059;
	}

IL_0052:
	{
		__this->set_m_HandleContainerRect_24((RectTransform_t3349966182 *)NULL);
	}

IL_0059:
	{
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::Set(System.Single)
extern "C"  void BoxSlider_Set_m3989333985 (BoxSlider_t1871650694 * __this, float ___input0, const MethodInfo* method)
{
	{
		float L_0 = ___input0;
		BoxSlider_Set_m1512577286(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::Set(System.Single,System.Boolean)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityEvent_2_Invoke_m4237217379_MethodInfo_var;
extern const uint32_t BoxSlider_Set_m1512577286_MetadataUsageId;
extern "C"  void BoxSlider_Set_m1512577286 (BoxSlider_t1871650694 * __this, float ___input0, bool ___sendCallback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_Set_m1512577286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___input0;
		float L_1 = BoxSlider_get_minValue_m3041624170(__this, /*hidden argument*/NULL);
		float L_2 = BoxSlider_get_maxValue_m1317285860(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		bool L_4 = BoxSlider_get_wholeNumbers_m627638124(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_6 = bankers_roundf(L_5);
		V_0 = L_6;
	}

IL_0025:
	{
		float L_7 = __this->get_m_Value_20();
		float L_8 = V_0;
		if ((!(((float)L_7) == ((float)L_8))))
		{
			goto IL_0032;
		}
	}
	{
		return;
	}

IL_0032:
	{
		float L_9 = V_0;
		__this->set_m_Value_20(L_9);
		BoxSlider_UpdateVisuals_m495585056(__this, /*hidden argument*/NULL);
		bool L_10 = ___sendCallback1;
		if (!L_10)
		{
			goto IL_0057;
		}
	}
	{
		BoxSliderEvent_t1774115848 * L_11 = __this->get_m_OnValueChanged_22();
		float L_12 = V_0;
		float L_13 = BoxSlider_get_valueY_m2581533163(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		UnityEvent_2_Invoke_m4237217379(L_11, L_12, L_13, /*hidden argument*/UnityEvent_2_Invoke_m4237217379_MethodInfo_var);
	}

IL_0057:
	{
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::SetY(System.Single)
extern "C"  void BoxSlider_SetY_m1241843396 (BoxSlider_t1871650694 * __this, float ___input0, const MethodInfo* method)
{
	{
		float L_0 = ___input0;
		BoxSlider_SetY_m956310887(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::SetY(System.Single,System.Boolean)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityEvent_2_Invoke_m4237217379_MethodInfo_var;
extern const uint32_t BoxSlider_SetY_m956310887_MetadataUsageId;
extern "C"  void BoxSlider_SetY_m956310887 (BoxSlider_t1871650694 * __this, float ___input0, bool ___sendCallback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_SetY_m956310887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___input0;
		float L_1 = BoxSlider_get_minValue_m3041624170(__this, /*hidden argument*/NULL);
		float L_2 = BoxSlider_get_maxValue_m1317285860(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		bool L_4 = BoxSlider_get_wholeNumbers_m627638124(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_6 = bankers_roundf(L_5);
		V_0 = L_6;
	}

IL_0025:
	{
		float L_7 = __this->get_m_ValueY_21();
		float L_8 = V_0;
		if ((!(((float)L_7) == ((float)L_8))))
		{
			goto IL_0032;
		}
	}
	{
		return;
	}

IL_0032:
	{
		float L_9 = V_0;
		__this->set_m_ValueY_21(L_9);
		BoxSlider_UpdateVisuals_m495585056(__this, /*hidden argument*/NULL);
		bool L_10 = ___sendCallback1;
		if (!L_10)
		{
			goto IL_0057;
		}
	}
	{
		BoxSliderEvent_t1774115848 * L_11 = __this->get_m_OnValueChanged_22();
		float L_12 = BoxSlider_get_value_m2439160562(__this, /*hidden argument*/NULL);
		float L_13 = V_0;
		NullCheck(L_11);
		UnityEvent_2_Invoke_m4237217379(L_11, L_12, L_13, /*hidden argument*/UnityEvent_2_Invoke_m4237217379_MethodInfo_var);
	}

IL_0057:
	{
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::OnRectTransformDimensionsChange()
extern "C"  void BoxSlider_OnRectTransformDimensionsChange_m1273303486 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnRectTransformDimensionsChange_m2743105076(__this, /*hidden argument*/NULL);
		BoxSlider_UpdateVisuals_m495585056(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::UpdateVisuals()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t BoxSlider_UpdateVisuals_m495585056_MetadataUsageId;
extern "C"  void BoxSlider_UpdateVisuals_m495585056 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_UpdateVisuals_m495585056_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		DrivenRectTransformTracker_t154385424 * L_0 = __this->get_address_of_m_Tracker_26();
		DrivenRectTransformTracker_Clear_m864483440(L_0, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_1 = __this->get_m_HandleContainerRect_24();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0089;
		}
	}
	{
		DrivenRectTransformTracker_t154385424 * L_3 = __this->get_address_of_m_Tracker_26();
		RectTransform_t3349966182 * L_4 = __this->get_m_HandleRect_16();
		DrivenRectTransformTracker_Add_m310530075(L_3, __this, L_4, ((int32_t)3840), /*hidden argument*/NULL);
		Vector2_t2243707579  L_5 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_5;
		Vector2_t2243707579  L_6 = Vector2_get_one_m3174311904(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = BoxSlider_get_normalizedValue_m911068691(__this, /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = V_2;
		Vector2_set_Item_m3881967114((&V_1), 0, L_8, /*hidden argument*/NULL);
		float L_9 = V_2;
		Vector2_set_Item_m3881967114((&V_0), 0, L_9, /*hidden argument*/NULL);
		float L_10 = BoxSlider_get_normalizedValueY_m3976688554(__this, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = V_2;
		Vector2_set_Item_m3881967114((&V_1), 1, L_11, /*hidden argument*/NULL);
		float L_12 = V_2;
		Vector2_set_Item_m3881967114((&V_0), 1, L_12, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_13 = __this->get_m_HandleRect_16();
		Vector2_t2243707579  L_14 = V_0;
		NullCheck(L_13);
		RectTransform_set_anchorMin_m4247668187(L_13, L_14, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_15 = __this->get_m_HandleRect_16();
		Vector2_t2243707579  L_16 = V_1;
		NullCheck(L_15);
		RectTransform_set_anchorMax_m2955899993(L_15, L_16, /*hidden argument*/NULL);
	}

IL_0089:
	{
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::UpdateDrag(UnityEngine.EventSystems.PointerEventData,UnityEngine.Camera)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t BoxSlider_UpdateDrag_m1281607389_MetadataUsageId;
extern "C"  void BoxSlider_UpdateDrag_m1281607389 (BoxSlider_t1871650694 * __this, PointerEventData_t1599784723 * ___eventData0, Camera_t189460977 * ___cam1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_UpdateDrag_m1281607389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3349966182 * V_0 = NULL;
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t3681755626  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Vector2_t2243707579  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Rect_t3681755626  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector2_t2243707579  V_8;
	memset(&V_8, 0, sizeof(V_8));
	float V_9 = 0.0f;
	Vector2_t2243707579  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Rect_t3681755626  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector2_t2243707579  V_12;
	memset(&V_12, 0, sizeof(V_12));
	{
		RectTransform_t3349966182 * L_0 = __this->get_m_HandleContainerRect_24();
		V_0 = L_0;
		RectTransform_t3349966182 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00dd;
		}
	}
	{
		RectTransform_t3349966182 * L_3 = V_0;
		NullCheck(L_3);
		Rect_t3681755626  L_4 = RectTransform_get_rect_m73954734(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		Vector2_t2243707579  L_5 = Rect_get_size_m3833121112((&V_1), /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Vector2_get_Item_m2792130561((&V_2), 0, /*hidden argument*/NULL);
		if ((!(((float)L_6) > ((float)(0.0f)))))
		{
			goto IL_00dd;
		}
	}
	{
		RectTransform_t3349966182 * L_7 = V_0;
		PointerEventData_t1599784723 * L_8 = ___eventData0;
		NullCheck(L_8);
		Vector2_t2243707579  L_9 = PointerEventData_get_position_m2131765015(L_8, /*hidden argument*/NULL);
		Camera_t189460977 * L_10 = ___cam1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		bool L_11 = RectTransformUtility_ScreenPointToLocalPointInRectangle_m2398565080(NULL /*static, unused*/, L_7, L_9, L_10, (&V_3), /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0049;
		}
	}
	{
		return;
	}

IL_0049:
	{
		Vector2_t2243707579  L_12 = V_3;
		RectTransform_t3349966182 * L_13 = V_0;
		NullCheck(L_13);
		Rect_t3681755626  L_14 = RectTransform_get_rect_m73954734(L_13, /*hidden argument*/NULL);
		V_4 = L_14;
		Vector2_t2243707579  L_15 = Rect_get_position_m24550734((&V_4), /*hidden argument*/NULL);
		Vector2_t2243707579  L_16 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_12, L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		Vector2_t2243707579  L_17 = V_3;
		Vector2_t2243707579  L_18 = __this->get_m_Offset_25();
		Vector2_t2243707579  L_19 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_6 = L_19;
		float L_20 = Vector2_get_Item_m2792130561((&V_6), 0, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_21 = V_0;
		NullCheck(L_21);
		Rect_t3681755626  L_22 = RectTransform_get_rect_m73954734(L_21, /*hidden argument*/NULL);
		V_7 = L_22;
		Vector2_t2243707579  L_23 = Rect_get_size_m3833121112((&V_7), /*hidden argument*/NULL);
		V_8 = L_23;
		float L_24 = Vector2_get_Item_m2792130561((&V_8), 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_25 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, ((float)((float)L_20/(float)L_24)), /*hidden argument*/NULL);
		V_5 = L_25;
		float L_26 = V_5;
		BoxSlider_set_normalizedValue_m2010660092(__this, L_26, /*hidden argument*/NULL);
		Vector2_t2243707579  L_27 = V_3;
		Vector2_t2243707579  L_28 = __this->get_m_Offset_25();
		Vector2_t2243707579  L_29 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		V_10 = L_29;
		float L_30 = Vector2_get_Item_m2792130561((&V_10), 1, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_31 = V_0;
		NullCheck(L_31);
		Rect_t3681755626  L_32 = RectTransform_get_rect_m73954734(L_31, /*hidden argument*/NULL);
		V_11 = L_32;
		Vector2_t2243707579  L_33 = Rect_get_size_m3833121112((&V_11), /*hidden argument*/NULL);
		V_12 = L_33;
		float L_34 = Vector2_get_Item_m2792130561((&V_12), 1, /*hidden argument*/NULL);
		float L_35 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, ((float)((float)L_30/(float)L_34)), /*hidden argument*/NULL);
		V_9 = L_35;
		float L_36 = V_9;
		BoxSlider_set_normalizedValueY_m851020601(__this, L_36, /*hidden argument*/NULL);
	}

IL_00dd:
	{
		return;
	}
}
// System.Boolean UnityEngine.UI.BoxSlider::MayDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  bool BoxSlider_MayDrag_m1941009447 (BoxSlider_t1871650694 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean UnityEngine.UI.Selectable::IsInteractable() */, __this);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		PointerEventData_t1599784723 * L_2 = ___eventData0;
		NullCheck(L_2);
		int32_t L_3 = PointerEventData_get_button_m2339189303(L_2, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B4_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B4_0;
	}
}
// System.Void UnityEngine.UI.BoxSlider::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t BoxSlider_OnPointerDown_m523869696_MetadataUsageId;
extern "C"  void BoxSlider_OnPointerDown_m523869696 (BoxSlider_t1871650694 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_OnPointerDown_m523869696_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PointerEventData_t1599784723 * L_0 = ___eventData0;
		bool L_1 = BoxSlider_MayDrag_m1941009447(__this, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		PointerEventData_t1599784723 * L_2 = ___eventData0;
		Selectable_OnPointerDown_m3110480835(__this, L_2, /*hidden argument*/NULL);
		Vector2_t2243707579  L_3 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Offset_25(L_3);
		RectTransform_t3349966182 * L_4 = __this->get_m_HandleContainerRect_24();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_008d;
		}
	}
	{
		RectTransform_t3349966182 * L_6 = __this->get_m_HandleRect_16();
		PointerEventData_t1599784723 * L_7 = ___eventData0;
		NullCheck(L_7);
		Vector2_t2243707579  L_8 = PointerEventData_get_position_m2131765015(L_7, /*hidden argument*/NULL);
		PointerEventData_t1599784723 * L_9 = ___eventData0;
		NullCheck(L_9);
		Camera_t189460977 * L_10 = PointerEventData_get_enterEventCamera_m1539996745(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		bool L_11 = RectTransformUtility_RectangleContainsScreenPoint_m1244853728(NULL /*static, unused*/, L_6, L_8, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_008d;
		}
	}
	{
		RectTransform_t3349966182 * L_12 = __this->get_m_HandleRect_16();
		PointerEventData_t1599784723 * L_13 = ___eventData0;
		NullCheck(L_13);
		Vector2_t2243707579  L_14 = PointerEventData_get_position_m2131765015(L_13, /*hidden argument*/NULL);
		PointerEventData_t1599784723 * L_15 = ___eventData0;
		NullCheck(L_15);
		Camera_t189460977 * L_16 = PointerEventData_get_pressEventCamera_m724559964(L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		bool L_17 = RectTransformUtility_ScreenPointToLocalPointInRectangle_m2398565080(NULL /*static, unused*/, L_12, L_14, L_16, (&V_0), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0071;
		}
	}
	{
		Vector2_t2243707579  L_18 = V_0;
		__this->set_m_Offset_25(L_18);
	}

IL_0071:
	{
		Vector2_t2243707579 * L_19 = __this->get_address_of_m_Offset_25();
		Vector2_t2243707579 * L_20 = __this->get_address_of_m_Offset_25();
		float L_21 = L_20->get_y_1();
		L_19->set_y_1(((-L_21)));
		goto IL_009a;
	}

IL_008d:
	{
		PointerEventData_t1599784723 * L_22 = ___eventData0;
		PointerEventData_t1599784723 * L_23 = ___eventData0;
		NullCheck(L_23);
		Camera_t189460977 * L_24 = PointerEventData_get_pressEventCamera_m724559964(L_23, /*hidden argument*/NULL);
		BoxSlider_UpdateDrag_m1281607389(__this, L_22, L_24, /*hidden argument*/NULL);
	}

IL_009a:
	{
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void BoxSlider_OnDrag_m2506766611 (BoxSlider_t1871650694 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method)
{
	{
		PointerEventData_t1599784723 * L_0 = ___eventData0;
		bool L_1 = BoxSlider_MayDrag_m1941009447(__this, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		PointerEventData_t1599784723 * L_2 = ___eventData0;
		PointerEventData_t1599784723 * L_3 = ___eventData0;
		NullCheck(L_3);
		Camera_t189460977 * L_4 = PointerEventData_get_pressEventCamera_m724559964(L_3, /*hidden argument*/NULL);
		BoxSlider_UpdateDrag_m1281607389(__this, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void BoxSlider_OnInitializePotentialDrag_m2830850663 (BoxSlider_t1871650694 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method)
{
	{
		PointerEventData_t1599784723 * L_0 = ___eventData0;
		NullCheck(L_0);
		PointerEventData_set_useDragThreshold_m2778439880(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.UI.BoxSlider::UnityEngine.UI.ICanvasElement.get_transform()
extern "C"  Transform_t3275118058 * BoxSlider_UnityEngine_UI_ICanvasElement_get_transform_m3600860139 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.UI.BoxSlider::UnityEngine.UI.ICanvasElement.IsDestroyed()
extern "C"  bool BoxSlider_UnityEngine_UI_ICanvasElement_IsDestroyed_m2151284327 (BoxSlider_t1871650694 * __this, const MethodInfo* method)
{
	{
		bool L_0 = UIBehaviour_IsDestroyed_m3809050211(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.UI.BoxSlider/BoxSliderEvent::.ctor()
extern const MethodInfo* UnityEvent_2__ctor_m731674732_MethodInfo_var;
extern const uint32_t BoxSliderEvent__ctor_m304261805_MetadataUsageId;
extern "C"  void BoxSliderEvent__ctor_m304261805 (BoxSliderEvent_t1774115848 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSliderEvent__ctor_m304261805_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_2__ctor_m731674732(__this, /*hidden argument*/UnityEvent_2__ctor_m731674732_MethodInfo_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
