﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _SeaControl : MonoBehaviour {

    public float sea_frequency = 0.3f;
    public float sea_height = 0.6f;
    public float sea_choppy = 4.0f;
    public float sea_speed = 0.8f;
    public float sun_pos_u = 1.0f; //0.25f;
    public float sun_pos_v = 0.0f; //1.0f;
    public float camera_height = 20.0f;
    public float camera_rot_angle = 45.0f;
    public float sea_movement_speed = 0.0f;
    //public Vector4 sea_water_color = new Vector4(0.8f, 0.9f, 0.6f, 1.0f);

    public Color sea_water_color = new Color(0.8f, 0.9f, 0.6f);//(204f, 229.5f, 153f);//(0.8f, 0.9f, 0.6f, 1.0f);
    public Color sea_base_color = new Color(0.1f, 0.19f, 0.22f);//(25.5f, 48.45f, 56.1f);//(0.1f, 0.19f, 0.22f, 1.0f);
    public Color sun_color = new Color(1.0f, 1.0f, 1.0f);

    public GameObject Gui_layer; // Assign in inspector
    private bool show_gui = false;

    Camera main_camera;

    //public ColorPicker color_picker;
    public UnityEngine.UI.Text sun_pos_u_text;
    public UnityEngine.UI.Text sun_pos_v_text;
    public UnityEngine.UI.Text sea_speed_text;
    public UnityEngine.UI.Text sea_choppy_text;
    public UnityEngine.UI.Text sea_height_text;
    public UnityEngine.UI.Text sea_frequency_text;
    public UnityEngine.UI.Text camera_height_text;
    public UnityEngine.UI.Text camera_rot_angle_text;
    public UnityEngine.UI.Text sea_movement_speed_text;

    //public ColorPicker picker_sky_color;
    public Color sky_color = new Color(0.35294f, 0.58039f, 0.79608f);
      
    void Start()
    {
        Screen.SetResolution(Mathf.RoundToInt(Screen.width * 0.8f), Mathf.RoundToInt(Screen.height * 0.8f), true);

        Gui_layer.SetActive(show_gui);

        main_camera = Camera.main;
        camera_height = -camera_height;
        main_camera.transform.position = new Vector3(0.0f, camera_height, 0.0f);

        sea_frequency_text.text = "Sea frequency: " + Mathf.Round(sea_frequency * 100f) / 100f;
        sea_height_text.text = "Sea height: " + Mathf.Round(sea_height * 100f) / 100f;
        sea_choppy_text.text = "Sea choppy: " + Mathf.Round(sea_choppy * 100f) / 100f;
        sea_speed_text.text = "Sea speed: " + Mathf.Round(sea_speed * 100f) / 100f;
        sun_pos_u_text.text = "Sun pos U: " + Mathf.Round(sun_pos_u * 100f) / 100f;
        sun_pos_v_text.text = "Sun pos V: " + Mathf.Round(sun_pos_v * 100f) / 100f;
        sea_movement_speed_text.text = "Movement speed: " + Mathf.Round(sea_movement_speed * 100f) / 100f;
        camera_height_text.text = "Camera height: " + Mathf.Round(camera_height * 100f) / 100f;
        camera_rot_angle_text.text = "Camera angle: " + Mathf.Round(camera_rot_angle * 100f) / 100f;

        //picker_sky_color.CurrentColor = sky_color;

        //sea_speed_text = GetComponent<UnityEngine.UI.Text>();

        //color_picker.AssignColor(sea_base_color, 0.0f);

        //color_picker.CurrentColor = sea_base_color;

        //GameObject tempObject = GameObject.Find("PickerSeaBaseColor");
        //if (tempObject != null)
        //{
            //color_picker = tempObject.GetComponent;
            //color_picker = tempObject.GetComponent<ColorPicker>();
            //color_picker.CurrentColor = sea_base_color;

        //}
    }
    
	// Update is called once per frame
	void Update () 
    {
        RenderSettings.skybox.SetFloat("_SEA_FREQ", sea_frequency);
        RenderSettings.skybox.SetFloat("_SEA_HEIGHT", sea_height);
        RenderSettings.skybox.SetFloat("_SEA_CHOPPY", sea_choppy);
        RenderSettings.skybox.SetFloat("_SEA_SPEED", sea_speed);
        RenderSettings.skybox.SetColor("_SEA_WATER_COLOR", sea_water_color);
        RenderSettings.skybox.SetColor("_SEA_BASE_COLOR", sea_base_color);
        RenderSettings.skybox.SetColor("_SUN_COLOR", sun_color);
        RenderSettings.skybox.SetFloat("_SEA_MOVEMENT_SPEED", sea_movement_speed);
        RenderSettings.skybox.SetFloat("_SUN_POS_U", sun_pos_u);
        RenderSettings.skybox.SetFloat("_SUN_POS_V", sun_pos_v);
        RenderSettings.skybox.SetColor("_SKY_COLOR", sky_color);

        main_camera.transform.position = new Vector3(0.0f, camera_height, 0.0f);
        main_camera.transform.eulerAngles = new Vector3(camera_rot_angle, 0.0f, 0.0f);

        if (Input.GetKeyUp("escape"))
            Application.Quit();
        if ((Input.GetKeyUp(KeyCode.F1)) || (Input.GetKeyUp(KeyCode.I)))
        {
            show_gui = !show_gui;
            Gui_layer.SetActive(show_gui);
            //Time.timeScale = show_gui ? 0 : 1;
        }
	}

    public void ChangeSeaFreq(float newSeaFreq)
    {
        sea_frequency = newSeaFreq;
        sea_frequency_text.text = "Sea frequency: " + Mathf.Round(sea_frequency * 100f) / 100f;
    }

    public void ChangeSeaHeight(float newSeaHeight)
    {
        sea_height = newSeaHeight;
        sea_height_text.text = "Sea height: " + Mathf.Round(sea_height * 100f) / 100f;
    }

    public void ChangeSeaChoppy(float newSeaChoppy)
    {
        sea_choppy = newSeaChoppy;
        sea_choppy_text.text = "Sea choppy: " + Mathf.Round(sea_choppy * 100f) / 100f;
    }

    public void ChangeSeaSpeed(float newSeaSpeed)
    {
        sea_speed = newSeaSpeed;
        sea_speed_text.text = "Sea speed: " + Mathf.Round(sea_speed * 100f) / 100f;
    }

    public void ChangeSeaWaterColor(Color newSeaWaterColor)
    {
        sea_water_color = newSeaWaterColor;
    }

    public void ChangeSeaBaseColor(Color newSeaBaseColor)
    {
        sea_base_color = newSeaBaseColor;
    }

    public void ChangeSunColor(Color newSunColor)
    {
        sun_color = newSunColor;
    }

    public void ChangeSunPosU(float newSunPosU)
    {
        sun_pos_u = newSunPosU;
        sun_pos_u_text.text = "Sun pos U: " + Mathf.Round(sun_pos_u * 100f) / 100f;
    }

    public void ChangeSunPosV(float newSunPosV)
    {
        sun_pos_v = newSunPosV;
        sun_pos_v_text.text = "Sun pos V: " + Mathf.Round(sun_pos_v * 100f) / 100f;
    }

    public void ChangeSeaMovementSpeed(float newSeaMovementSpeed)
    {
        sea_movement_speed = newSeaMovementSpeed;
        sea_movement_speed_text.text = "Movement speed: " + Mathf.Round(newSeaMovementSpeed * 100f) / 100f; 
    }

    public void ChangeCameraHeight(float newCameraHeight)
    {
        camera_height = newCameraHeight;
        camera_height_text.text = "Camera height: " + Mathf.Round(camera_height * 100f) / 100f;
    }

    public void ChangeCameraRotAngle(float newCameraRotAngle)
    {
        camera_rot_angle = newCameraRotAngle;
        camera_rot_angle_text.text = "Camera angle: " + Mathf.Round(camera_rot_angle * 100f) / 100f;
    }

    public void ChangeSkyColor(Color newSkyColor)
    {
        sky_color = newSkyColor;
    }
    
}
