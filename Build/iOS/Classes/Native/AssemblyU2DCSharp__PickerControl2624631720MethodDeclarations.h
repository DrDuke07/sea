﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// _PickerControl
struct _PickerControl_t2624631720;

#include "codegen/il2cpp-codegen.h"

// System.Void _PickerControl::.ctor()
extern "C"  void _PickerControl__ctor_m82678137 (_PickerControl_t2624631720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _PickerControl::Start()
extern "C"  void _PickerControl_Start_m2315169225 (_PickerControl_t2624631720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _PickerControl::Update()
extern "C"  void _PickerControl_Update_m2391504050 (_PickerControl_t2624631720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
