﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _PickerSkyColor : MonoBehaviour {

    //public new Renderer renderer;
    public ColorPicker picker_sky_color;
    private Color sky_color = new Color(0.35294f, 0.58039f, 0.79608f);

    // Use this for initialization
    void Start()
    {
        picker_sky_color.CurrentColor = sky_color;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
