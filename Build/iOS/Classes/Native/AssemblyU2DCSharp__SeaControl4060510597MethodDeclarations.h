﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// _SeaControl
struct _SeaControl_t4060510597;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void _SeaControl::.ctor()
extern "C"  void _SeaControl__ctor_m3015587372 (_SeaControl_t4060510597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _SeaControl::Start()
extern "C"  void _SeaControl_Start_m3738024332 (_SeaControl_t4060510597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _SeaControl::Update()
extern "C"  void _SeaControl_Update_m2073980969 (_SeaControl_t4060510597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _SeaControl::ChangeSeaFreq(System.Single)
extern "C"  void _SeaControl_ChangeSeaFreq_m2565305150 (_SeaControl_t4060510597 * __this, float ___newSeaFreq0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _SeaControl::ChangeSeaHeight(System.Single)
extern "C"  void _SeaControl_ChangeSeaHeight_m4220722243 (_SeaControl_t4060510597 * __this, float ___newSeaHeight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _SeaControl::ChangeSeaChoppy(System.Single)
extern "C"  void _SeaControl_ChangeSeaChoppy_m1268845189 (_SeaControl_t4060510597 * __this, float ___newSeaChoppy0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _SeaControl::ChangeSeaSpeed(System.Single)
extern "C"  void _SeaControl_ChangeSeaSpeed_m1480111079 (_SeaControl_t4060510597 * __this, float ___newSeaSpeed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _SeaControl::ChangeSeaWaterColor(UnityEngine.Color)
extern "C"  void _SeaControl_ChangeSeaWaterColor_m3629580051 (_SeaControl_t4060510597 * __this, Color_t2020392075  ___newSeaWaterColor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _SeaControl::ChangeSeaBaseColor(UnityEngine.Color)
extern "C"  void _SeaControl_ChangeSeaBaseColor_m1177439755 (_SeaControl_t4060510597 * __this, Color_t2020392075  ___newSeaBaseColor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _SeaControl::ChangeSunColor(UnityEngine.Color)
extern "C"  void _SeaControl_ChangeSunColor_m3190758425 (_SeaControl_t4060510597 * __this, Color_t2020392075  ___newSunColor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _SeaControl::ChangeSunPosU(System.Single)
extern "C"  void _SeaControl_ChangeSunPosU_m2897449384 (_SeaControl_t4060510597 * __this, float ___newSunPosU0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _SeaControl::ChangeSunPosV(System.Single)
extern "C"  void _SeaControl_ChangeSunPosV_m893575213 (_SeaControl_t4060510597 * __this, float ___newSunPosV0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _SeaControl::ChangeSeaMovementSpeed(System.Single)
extern "C"  void _SeaControl_ChangeSeaMovementSpeed_m3088995436 (_SeaControl_t4060510597 * __this, float ___newSeaMovementSpeed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _SeaControl::ChangeCameraHeight(System.Single)
extern "C"  void _SeaControl_ChangeCameraHeight_m1236121629 (_SeaControl_t4060510597 * __this, float ___newCameraHeight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _SeaControl::ChangeCameraRotAngle(System.Single)
extern "C"  void _SeaControl_ChangeCameraRotAngle_m4012972818 (_SeaControl_t4060510597 * __this, float ___newCameraRotAngle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void _SeaControl::ChangeSkyColor(UnityEngine.Color)
extern "C"  void _SeaControl_ChangeSkyColor_m1581686058 (_SeaControl_t4060510597 * __this, Color_t2020392075  ___newSkyColor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
