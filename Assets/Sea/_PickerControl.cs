﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _PickerControl : MonoBehaviour {
    
    //public new Renderer renderer;
    public ColorPicker picker;
    public Color sea_base_color = new Color(0.1f, 0.19f, 0.22f);

	// Use this for initialization
	void Start () {
        picker.CurrentColor = sea_base_color;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
