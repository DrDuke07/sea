﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.BoxSlider
struct BoxSlider_t1871650694;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.BoxSlider/BoxSliderEvent
struct BoxSliderEvent_t1774115848;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_BoxSlider_BoxSlid1774115848.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate1528800019.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"

// System.Void UnityEngine.UI.BoxSlider::.ctor()
extern "C"  void BoxSlider__ctor_m1006728114 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.BoxSlider::get_handleRect()
extern "C"  RectTransform_t3349966182 * BoxSlider_get_handleRect_m2621304777 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::set_handleRect(UnityEngine.RectTransform)
extern "C"  void BoxSlider_set_handleRect_m168119168 (BoxSlider_t1871650694 * __this, RectTransform_t3349966182 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.BoxSlider::get_minValue()
extern "C"  float BoxSlider_get_minValue_m3041624170 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::set_minValue(System.Single)
extern "C"  void BoxSlider_set_minValue_m1004770411 (BoxSlider_t1871650694 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.BoxSlider::get_maxValue()
extern "C"  float BoxSlider_get_maxValue_m1317285860 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::set_maxValue(System.Single)
extern "C"  void BoxSlider_set_maxValue_m1702472861 (BoxSlider_t1871650694 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.BoxSlider::get_wholeNumbers()
extern "C"  bool BoxSlider_get_wholeNumbers_m627638124 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::set_wholeNumbers(System.Boolean)
extern "C"  void BoxSlider_set_wholeNumbers_m2694006389 (BoxSlider_t1871650694 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.BoxSlider::get_value()
extern "C"  float BoxSlider_get_value_m2439160562 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::set_value(System.Single)
extern "C"  void BoxSlider_set_value_m3164710557 (BoxSlider_t1871650694 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.BoxSlider::get_normalizedValue()
extern "C"  float BoxSlider_get_normalizedValue_m911068691 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::set_normalizedValue(System.Single)
extern "C"  void BoxSlider_set_normalizedValue_m2010660092 (BoxSlider_t1871650694 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.BoxSlider::get_valueY()
extern "C"  float BoxSlider_get_valueY_m2581533163 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::set_valueY(System.Single)
extern "C"  void BoxSlider_set_valueY_m2217414554 (BoxSlider_t1871650694 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.BoxSlider::get_normalizedValueY()
extern "C"  float BoxSlider_get_normalizedValueY_m3976688554 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::set_normalizedValueY(System.Single)
extern "C"  void BoxSlider_set_normalizedValueY_m851020601 (BoxSlider_t1871650694 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.BoxSlider/BoxSliderEvent UnityEngine.UI.BoxSlider::get_onValueChanged()
extern "C"  BoxSliderEvent_t1774115848 * BoxSlider_get_onValueChanged_m1694276966 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::set_onValueChanged(UnityEngine.UI.BoxSlider/BoxSliderEvent)
extern "C"  void BoxSlider_set_onValueChanged_m1579271121 (BoxSlider_t1871650694 * __this, BoxSliderEvent_t1774115848 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.BoxSlider::get_stepSize()
extern "C"  float BoxSlider_get_stepSize_m1073340932 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::Rebuild(UnityEngine.UI.CanvasUpdate)
extern "C"  void BoxSlider_Rebuild_m3577117895 (BoxSlider_t1871650694 * __this, int32_t ___executing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::LayoutComplete()
extern "C"  void BoxSlider_LayoutComplete_m2656874525 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::GraphicUpdateComplete()
extern "C"  void BoxSlider_GraphicUpdateComplete_m2980925808 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::OnEnable()
extern "C"  void BoxSlider_OnEnable_m2230558070 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::OnDisable()
extern "C"  void BoxSlider_OnDisable_m3760522835 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::UpdateCachedReferences()
extern "C"  void BoxSlider_UpdateCachedReferences_m3248150675 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::Set(System.Single)
extern "C"  void BoxSlider_Set_m3989333985 (BoxSlider_t1871650694 * __this, float ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::Set(System.Single,System.Boolean)
extern "C"  void BoxSlider_Set_m1512577286 (BoxSlider_t1871650694 * __this, float ___input0, bool ___sendCallback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::SetY(System.Single)
extern "C"  void BoxSlider_SetY_m1241843396 (BoxSlider_t1871650694 * __this, float ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::SetY(System.Single,System.Boolean)
extern "C"  void BoxSlider_SetY_m956310887 (BoxSlider_t1871650694 * __this, float ___input0, bool ___sendCallback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::OnRectTransformDimensionsChange()
extern "C"  void BoxSlider_OnRectTransformDimensionsChange_m1273303486 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::UpdateVisuals()
extern "C"  void BoxSlider_UpdateVisuals_m495585056 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::UpdateDrag(UnityEngine.EventSystems.PointerEventData,UnityEngine.Camera)
extern "C"  void BoxSlider_UpdateDrag_m1281607389 (BoxSlider_t1871650694 * __this, PointerEventData_t1599784723 * ___eventData0, Camera_t189460977 * ___cam1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.BoxSlider::MayDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  bool BoxSlider_MayDrag_m1941009447 (BoxSlider_t1871650694 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void BoxSlider_OnPointerDown_m523869696 (BoxSlider_t1871650694 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void BoxSlider_OnDrag_m2506766611 (BoxSlider_t1871650694 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void BoxSlider_OnInitializePotentialDrag_m2830850663 (BoxSlider_t1871650694 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.UI.BoxSlider::UnityEngine.UI.ICanvasElement.get_transform()
extern "C"  Transform_t3275118058 * BoxSlider_UnityEngine_UI_ICanvasElement_get_transform_m3600860139 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.BoxSlider::UnityEngine.UI.ICanvasElement.IsDestroyed()
extern "C"  bool BoxSlider_UnityEngine_UI_ICanvasElement_IsDestroyed_m2151284327 (BoxSlider_t1871650694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
