﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _PickerSeaWaterColorControl : MonoBehaviour {

    //public new Renderer renderer;
    public ColorPicker sea_water_color_picker;
    public Color sea_water_color = new Color(0.8f, 0.9f, 0.6f);

	// Use this for initialization
	void Start () {
        sea_water_color_picker.CurrentColor = sea_water_color;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
