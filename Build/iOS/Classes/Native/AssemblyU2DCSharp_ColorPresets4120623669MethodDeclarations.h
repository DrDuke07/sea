﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColorPresets
struct ColorPresets_t4120623669;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void ColorPresets::.ctor()
extern "C"  void ColorPresets__ctor_m508027560 (ColorPresets_t4120623669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPresets::Awake()
extern "C"  void ColorPresets_Awake_m2912318885 (ColorPresets_t4120623669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPresets::CreatePresetButton()
extern "C"  void ColorPresets_CreatePresetButton_m19834235 (ColorPresets_t4120623669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPresets::PresetSelect(UnityEngine.UI.Image)
extern "C"  void ColorPresets_PresetSelect_m1727594879 (ColorPresets_t4120623669 * __this, Image_t2042527209 * ___sender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPresets::ColorChanged(UnityEngine.Color)
extern "C"  void ColorPresets_ColorChanged_m578093369 (ColorPresets_t4120623669 * __this, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
